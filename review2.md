**Design Review**
[I] 1 El texto y la animación está muy arriba, el texto debería ir mas o menos por donde está el cuadro rojo (la imagen viene en el correo)
[D] 2 Le faltan efectos parallax que sugerí en el prototipo, si no se puede exactamente así pues hay que implementar algunos más sencillos para que no aparezca estática
     http://www.mindworks.gr/
[D] 3 En pago en línea las cajas de los textos tienen una sombra muy fuerte, ponerles más sutil 
[D] 4 En pago en línea debería ir el texto pago seguro un poco más pequeño y en una línea
[D] 5 Cuando estás en pago en línea el menú no se puede abrir
[X] Se podrá esconder del menú el portal web? es que aún no está activo
[X] Acabo de sacar el aviso de privacidad del sitio anterior por que no me lo pasaron, se los mando en un PDF adjunto para que puedan ponerle la acción al aviso de privacidad que está en el footer y mostrar el PDF
[R]  Todos los textos en todos los slides de servicios tienen que ir pegados a la izquierda, aquí algunos están más a la derecha, algunos en medio y así...
[R] En cadena de suministro el texto está muy arriba, supongo que corrigiendo el de la versión de escritorio se corrige ese problema también
[R] Planeación y asesoría: La imagen está muy abajo y se empalma con el timeline
[R] Compras nacionales e internacionales: La imagen está muy abajo y se empalma con el timeline
[R] Almacenaje: La imagen está muy abajo y se empalma con el timeline
[R] En cadena de suministro el texto está muy arriba, supongo que corrigiendo el de la versión de escritorio se corrige ese problema también
[R] Planeación y asesoría: No se empalma con el timeline como las otras pero aún así la imagen debería ir más arriba por que se junta mucho con el timeline
[R] Compras nacionales e internacionales: La imagen está muy abajo y se empalma con el timeline
[R] Almacenaje: La imagen está muy abajo y se empalma con el timeline
[R] En cadena de suministro el texto está muy arriba, supongo que corrigiendo el de la versión de escritorio se corrige ese problema también
[R] Planeación y asesoría: No se empalma con el timeline como las otras pero aún así la imagen debería ir más arriba por que se junta mucho con el timeline
[R] Compras nacionales e internacionales: No se empalma con el timeline como las otras pero aún así la imagen debería ir más arriba por que se junta mucho con el timeline
[R] Almacenaje: La imagen está muy abajo y se empalma con el timeline
[R] El texto de residencial e interiorismo sigue muy pegado a las líneas del hexágono, no sé si le tenemos que poner un tamaño más pequeño de tipografía, o pasar el & a la línea de abajo
     
[X] CargoBaja es pegado y con CB mayusculas y actualizar el año a 2019  
[] Quitar la animación y poner solo la ilustración de la línea que va en el primer slide (Adjunto ilustración)
[X] Dejar la entrada del logo de Cargo Baja 2 segundos más después de que termine la animación