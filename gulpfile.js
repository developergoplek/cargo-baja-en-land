// ---------------------------------------------------------------------
// | Define dependencies to use.                                       |
// ---------------------------------------------------------------------
var gulp = require('gulp');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
//var sass = require('gulp-sass');
var sass = require('gulp-sass')(require('sass'));
var uglifycss = require('gulp-uglifycss');
var tinypng = require('gulp-tinypng-compress');
var uglify = require('gulp-uglify');
var babel = require('gulp-babel');
var typescript = require('gulp-typescript');
var htmlmin = require('gulp-htmlmin');
var favicons = require("favicons").stream, log = require("fancy-log");
var sequence = require('gulp-sequence');
var autoprefixer = require('gulp-autoprefixer');
var livereload = require('gulp-livereload');


const srcPath = {
    css: 'source/design/css/',
    fonts: 'source/design/fonts/',
    imgs: 'source/design/imgs/',
    js: 'source/design/js/',
    sass: 'source/design/sass/',
    ts: 'source/design/ts/',
    root: 'source/'
};

const destPath = {
    css: 'html/fragment/themes/cargobajanuevo/design/css/',
    fonts: 'html/fragment/themes/cargobajanuevo/design/fonts/',
    imgs: 'html/fragment/themes/cargobajanuevo/design/imgs/',
    js: 'html/fragment/themes/cargobajanuevo/design/js/',
    root: 'html/fragment/themes/cargobajanuevo/'
};


// ---------------------------------------------------------------------
// | Maintains updated src changes in the browser.                     |
// ---------------------------------------------------------------------

/**
 * Compile sass files.
 * Reference: https://www.npmjs.com/package/gulp-sass
 */
gulp.task('sass', async () => {
    // Source files.
    let srcFiles = `${srcPath.sass}**/*.scss`;

    let autoPrefixerOpts = {
        browsers: 'last 2 versions',
        cascade: true
    };

    // Output file.
    let outputFile = 'styles.css';

    return gulp.src(srcFiles)
        .pipe(plumber())
        .pipe(sass())
        .pipe(concat(outputFile))
        .pipe(autoprefixer(autoPrefixerOpts))
        .pipe(gulp.dest(srcPath.css));
});

/**
 * Concatenate and minify css files using gulp-minify-css.
 * Reference: https://github.com/murphydanger/gulp-minify-css
 */
gulp.task('css', async () => {
    // Source files.
    let srcFiles = [
        `${srcPath.css}/*.css`
    ];

    // Output file.
    let outputFile = 'styles.min.css';

    return gulp.src(srcFiles)
        .pipe(plumber())
        .pipe(concat(outputFile))
        .pipe(uglifycss())
        .pipe(gulp.dest(destPath.css));
});


/**
 * Copy and minify cs vendor files.
 */
gulp.task('css-vendor', () => {
    // Source vendor files
    let srcFiles = [`${srcPath.css}vendor/**/*.css`];

    return gulp.src(srcFiles)
        .pipe(uglifycss())
        .pipe(gulp.dest(`${destPath.css}vendor`));
});


/**
 * Generate generate favicons.
 * Reference: https://github.com/haydenbleasel/favicons
 */
gulp.task('favicon', function () {
    return gulp.src("source/favicon.png").pipe(favicons({
        appName: "My App",
        appDescription: "This is my application",
        developerName: "Goplek",
        developerURL: "http://goplek.com/",
        background: "transparent",
        path: "html/favicons",
        url: "http://goplek-boilerplate.com/",
        display: "standalone",
        orientation: "portrait",
        version: 1.0,
        logging: false,
        icons: {
            android: false,
            appleIcon: true,
            appleStartup: false,
            coast: false,
            favicons: true,
            firefox: false,
            opengraph: false,
            twitter: false,
            windows: false,
            yandex: false
        }
    }))
        .on("error", log)
        .pipe(gulp.dest(`${destPath.root}favicons`));
});


/**
 * Copy specific files from fonts folder.
 */
gulp.task('fonts', () => {
    // Source files.
    let srcFiles = `${srcPath.fonts}*.{eot,woff,woff2,ttf,svg,otf}`;

    return gulp.src(srcFiles)
        .pipe(gulp.dest(destPath.fonts));
});


/**
 * Minify and create txt file from html.
 * References: https://github.com/jonschlinkert/gulp-htmlmin
 *             https://github.com/kangax/html-minifier
 */
gulp.task('html', () => {
    // Source files.
    let srcFiles = `${srcPath.root}[!_]*.html`;

    // Opts
    let opts = {
        collapseWhitespace: true,
        removeComments: true
    };

    return gulp.src(srcFiles)
    // .pipe(htmlmin(opts))
        .pipe(rename({extname: '.txt'}))
        .pipe(gulp.dest(destPath.root));
});


/**
 * Clone html files adding an underscore in name file.
 */
gulp.task('html-dev', () => {
    // Source files.
    let srcFiles = `${srcPath.root}[!_]*.html`;

    return gulp.src(srcFiles)
        .pipe(rename({prefix: '_'}))
        .pipe(gulp.dest(srcPath.root));
});


/**
 * Optimize images using gulp-tinypng-compress.
 * Reference: https://github.com/stnvh/gulp-tinypng-compress
 */
gulp.task('imgs', () => {
    let tinyFiles = `${srcPath.imgs}**/*.{png,jpg,jpeg}`,
        otherFiles = `${srcPath.imgs}**/*.*`;

    // Copy non png, jpg and jpeg files.
    gulp.src([otherFiles, `!${tinyFiles}`])
        .pipe(gulp.dest(destPath.imgs));


    // tinypng options
    let opts = {
        key: 'pFFAVLRIqtR-exFUo5XuSLrNAuP53k4d',
        sigFile: `${srcPath.imgs}.tinypng-sigs`,
        log: true
    };

    // Optimize tinyFiles.
    return gulp.src(tinyFiles)
    // .pipe(tinypng(opts))
        .pipe(gulp.dest(destPath.imgs));
});

/**
 * Concatenate and minify js files.
 * References: https://github.com/terinjokes/gulp-uglify
 *             https://github.com/babel/gulp-babel
 */
gulp.task('js', () => {
    // Source files (avoid vendor)
    let srcFiles = [
        `!${srcPath.js}vendor/**/`,
        `${srcPath.js}**/*.js`
    ];

    // Output file.
    let outputFile = 'scripts.min.js';

    // Babel settings.
    let babelSettings = {presets: ['es2015']};

    return gulp.src(srcFiles)
        .pipe(plumber())
        .pipe(babel(babelSettings))
        .pipe(concat(outputFile))
        .pipe(uglify())
        .pipe(gulp.dest(destPath.js));
});


/**
 * Copy and minify js vendor files.
 */
gulp.task('js-vendor', () => {
    // Source vendor files
    let srcFiles = [`${srcPath.js}vendor/**/*.js`];

    return gulp.src(srcFiles)
        .pipe(uglify())
        .pipe(gulp.dest(`${destPath.js}vendor`));
});


/**
 * Concatenate and compile typescript files.
 * Reference: https://www.npmjs.com/package/gulp-typescript/
 */
gulp.task('ts', () => {
    let opts = {
        target: 'ES5',
        removeComments: false,
        noImplicitAny: false
    };

    // Source files.
    let srcFiles = [
        `${srcPath.ts}core/*.ts`,
        `${srcPath.ts}util/*.ts`,
        `${srcPath.ts}*.ts`
    ];

    // Output file.
    let outputFile = 'pl.ts';

    return gulp.src(srcFiles)
        .pipe(plumber())
        .pipe(concat(outputFile))
        .pipe(typescript(opts))
        .pipe(gulp.dest(srcPath.js));
});


/**
 * Reload on change.
 */
gulp.task('reload', gulp.series('js', 'sass', 'css'), () => {
    gulp.src(srcPath.root)
        .pipe(livereload());
});
/**
 * Monitors changes in projects files and apply changes instantly.
 * Use with livereload chrome extension.
 * Reference: https://github.com/vohof/gulp-livereload
 */
gulp.task('watch', () => {
    // Files to be watched.
    let files = [
        `${destPath.root}*.php`,

        `${srcPath.root}*.html`,
        `${srcPath.sass}**/*.scss`,
        `${srcPath.js}**/*.js`
    ];

    livereload.listen();

    gulp.watch(files, gulp.series('reload'));
});

gulp.task('all:prod', gulp.parallel(
    'css', 'js'
));

/**
 * Build project and lave ready to deploy.
 * @param done
 */
gulp.task('build', (done) => {
    gulp.series('css', 'fonts', 'imgs', 'js', 'js-vendor', 'html', 'favicon', done);
});


/**
 * Run default task.
 */
gulp.task('default', gulp.series('build'));