/**
 * @author cesarmejia
 */

'use strict';

let q = (selector, target) => (target || document).querySelector(selector);
let qa = (selector, target) => (target || document).querySelectorAll(selector);

var Page = {
    /**
     * @type {jQuery}
     */
    $html: $('html'),
    $body: $('body'),

    /**
     * @type {jQuery}
     */
    $window: $(window),

    animNames: [],

    counter: 0,
    scrollX: 0,
    totalSlides: 0,
    currentSlide: 0,

    animRequest: window.requestAnimationFrame
        || window.webkitRequestAnimationFrame
        || window.mozRequestAnimationFrame
        || window.msRequestAnimationFrame
        || function (callback) {
            window.setTimeout(callback, 25);
        },
    /**
     * Get transitionend event depending of the browser.
     * @return {string}
     */
    transitionSelect: function () {
        var el = document.createElement('div');
        var transEndEventNames = {
            WebkitTransition: 'webkitTransitionEnd',
            MozTransition: 'transitionend',
            OTransition: 'otransitionend',
            transition: 'transitionend'
        };
        for (var name_1 in transEndEventNames) {
            if (el.style[name_1] !== undefined)
                return transEndEventNames[name_1];
        }
    },

    /**
     * Initialize page scripts.
     */
    init: function () {

        let loc = location.pathname;
        if ((loc != '/contacto') && (loc != '/') && (loc != '/beneficios') && (loc != '/servicios')) {
            Page.Pagination.init();
            Page.scrollOrientation();
            Page.Swipe_Pagination();
            Page.HideSwipeanimation();
        }
        Page.Contact.init();
        Page.LoaderAnimation();

        // Initialize page parts.
        Page.Navigation.init();
        Page.scrollFire();
        Page.Checkout.init();
        // Events
        Page.$window.on('load', function () {
            Page._onLoad();
        });
        Page.$window.on('resize', function () {
            Page._onResize();
        });
        Page.$window.on('scroll', function () {
            Page._onScroll();
        });
    },

    /**
     * Fires when the page is loaded.
     * @private
     */
    _onLoad: function () {
        // Page.returnCorrectSection();
    },
    /**
     * Fires when the page is resized.
     * @private
     */
    _onResize: function () {
    },
    /**
     * Fires on scrolling.
     * @private
     */
    _onScroll: function () {
    },
    /**
     * Effect scroll in elements
     */
    scrollFire: function () {
        let elements = document.querySelectorAll('.partial');
        let blocks = document.querySelectorAll('.menu');
        let navBlocks = document.querySelectorAll('.menu');
        let blocksInview = new pl.ScrollFire(blocks, {
            method: 'markerOver',
            markerPercentage: 40
        });
        let scrollFire = new pl.ScrollFire(elements, {
            method: 'PartiallyVisible',
        });
        let navInview = new pl.ScrollFire(navBlocks, {
            method: 'markerOver',
            markerPercentage: 40
        });

        scrollFire.inview.add((element) => {
            if (!pl.Classie.hasClass(element, 'viewed'))
                pl.Classie.addClass(element, 'viewed');
            if (pl.Classie.hasClass(element, 'rewards') && !pl.Classie.hasClass(element, 'rewards-played')) {
                this.counter();
                pl.Classie.addClass(element, 'rewards-played')
            }

        });
    },
    /**
     * function to hide loader in main
     */
    LoaderAnimation: function () {
        let content_Load = document.querySelector('.loader');

        if (content_Load) {
            setTimeout(function () {
                pl.Classie.addClass(content_Load, 'lo-hide');
            }, 4000);
        }
    },
    /**
     * function to hide swipe svg  inner process
     */
    HideSwipeanimation: function () {
        let swipe_img_content = document.querySelector('.wrapper-img-swipe');

        if (swipe_img_content) {
            setTimeout(function () {
                pl.Classie.addClass(swipe_img_content, 'swipe-hide');
            }, 2000);
        }
    },
    /**
     * Scroll horizontal
     */
    scrollOrientation: function () {
        let wrapper = document.getElementById('wrapper');
        window.onscroll = function (e) { e.preventDefault() };
        if (wrapper) {
            let children = wrapper.children[0];
            let totalSlides = children.children.length - 1;
            let slide = children.children[0];

            if (!Page.isMobileSize()) {
                this.$body.css('overflow', 'hidden');
                var marker = true,
                    delta,
                    direction,
                    interval = 50,
                    counter1 = 0,
                    counter2,
                    flag = true,
                    timer,
                    seconds = 1.2;

                let exec = function () {
                    if (direction === "up" && Page.currentSlide > 0) {
                        /* left swipe */
                        Page.currentSlide = Page.currentSlide - 1;
                    } else if (direction === "down" && Page.currentSlide < 7) {
                        /* right swipe */
                        Page.currentSlide = Page.currentSlide + 1;
                    }
                    if (Page.currentSlide >= 0 && Page.currentSlide < Page.totalSlides) {
                        Page.updateScrollX();
                    }
                }

                let wheel = function (e) {
                    counter1 += 1;
                    e = e || window.event;
                    delta = e.deltaY || e.detail || e.wheelDelta;
                    if (delta > 0) { direction = 'down'; } else { direction = 'up'; }
                    if (marker) wheelStart();
                    return false;
                };

                let wheelStart = function () {
                    marker = false;
                    wheelAct();
                    // console.log('start');

                    clearInterval(timer);
                    timer = setInterval(() => {
                        exec();
                    }, seconds * 1000);

                    exec();



                    /* if (flag) {
                        exec();
                        flag = false;
                    } */
                };

                let wheelAct = function () {
                    counter2 = counter1;
                    setTimeout(function () {
                        if (counter2 == counter1) {
                            wheelEnd();
                        } else {
                            wheelAct();
                        }
                    }, interval);
                };

                let wheelEnd = function () {
                    marker = true,
                    counter1 = 0,
                    counter2 = 0;

                    clearInterval(timer);
                    // console.log('end');
                    // flag = true;
                };

                let enable = function () {
                    if (wrapper.addEventListener) {
                        if ('onwheel' in document) {
                            wrapper.addEventListener('wheel', wheel);
                        } else if ('onmousewheel' in document) {
                            wrapper.addEventListener('mousewheel', wheel);
                        } else {
                            wrapper.addEventListener('MozMousePixelScroll', wheel);
                        }
                    } else {
                        wrapper.attachEvent('onmousewheel', wheel);
                    }
                }
                enable();
            }
        }
    },

    /**
     *
     */
    updateWrapper: function () {
        let wrapper = document.getElementById('wrapper');
        let children = wrapper.children[0];

        children.style.webkitTransform = `translate(${Page.scrollX}px, 0)`;
        children.style.transform = `translate(${Page.scrollX}px, 0)`;
    },

    /**
     * Represent Checkout section
     */
    Checkout: {

        /**
         * @type jQuery
         */
        $elem: null,

        /**
         * @type CheckoutForm
         */
        checkout: null,

        /**
         * Initialize Checkout section
         */
        init: function () {
            this.$elem = $('.block.payment');

            if (this.$elem.length) {
                var form = document.querySelector('.checkout-form');

                this.checkout = new CheckoutForm(form);
            }
        }

    },

    /**
     * Send notification to Google Analytics.
     * @param {string} category
     * @param {string} action
     * @param {string} label
     * @return {[type]} [description]
     */
    ga: function (category, action, label) {
        if ("function" === typeof gtag
            && "string" === typeof category
            && "string" === typeof action) {

            gtag('event', action, {
                'event_category': category,
                'event_label': label || ''
            });

            // Print in console.
            if ("console" in window) {
                console.log(
                    'ga: [category: %s, action: %s, label: %s]',
                    category, action, label || ''
                );
            }
        }
    },
    /**
     * Determine if current viewport has mobile size.
     * @returns {boolean}
     */
    isMobileSize: function () {
        return Page.$window.width() < 996;
    },
    /**
     * Scroll to a section indicated by hash.
     * @param {string} hash
     * @param {number} scrollTime
     * @param {number} extraOffset
     */
    scrollTo: function (hash, scrollTime, extraOffset) {
        var section = $(hash);
        if (section.length) {
            var st = scrollTime || 1000, eo = extraOffset || 0;
            var offset = section.offset().top - eo;
            $('html, body').stop().animate({ scrollTop: offset }, st);
        }
    },
    /**
     * Navigation.
     */
    Navigation: {
        /**
         * @type jQuery
         */
        $closeListBtn: null,

        /**
         * @type jQuery
         */
        $elem: null,

        /**
         * @type jQuery
         */
        $list: null,

        /**
         * @type jQuery
         */
        $toggleBtn: null,

        /**
         * Initialize page part.
         */
        init: function () {
            this.$elem = $('.navigation');

            if (this.$elem.length) {
                this.$listWrapper = this.$elem.find('.list-wrapper');
                this.$closeListBtn = this.$elem.find('.close-list-btn');
                this.$toggleBtn = this.$elem.find('.toggle-btn');

                this.toggleCollapsed = this.toggleCollapsed.bind(this);

                this.$closeListBtn.on('click', this.toggleCollapsed);
                this.$toggleBtn.on('click', this.toggleCollapsed);
            }
        },

        /**
         * Show or hide mobile navigation.
         */
        toggleCollapsed: function (ev) {
            ev.stopPropagation();

            let closeMenu = () => {
                this.$listWrapper.removeClass('list-collapsed');
                Page.$body.removeClass('no-scroll');
                this.$elem.removeClass('navigation-collapsed');
            };

            if (this.$listWrapper.hasClass('list-collapsed')) {
                this.$listWrapper.removeClass('list-collapsed');
                Page.$html.removeClass('no-scroll');
                Page.$body.removeClass('no-scroll');
                this.$elem.removeClass('navigation-collapsed');
                Page.$body.off('click', closeMenu);

            } else {
                this.$listWrapper.addClass('list-collapsed');
                Page.$html.addClass('no-scroll');
                Page.$body.addClass('no-scroll');
                this.$elem.addClass('navigation-collapsed');

                Page.$body.on('click', closeMenu);
            }
        }
    },
    /**
     * Contact
     */
    Contact: {
        /**
         * @type jQuery
         */
        $elem: null,

        /**
         * Initialize page part.
         */
        init: function () {
            var contactForm = $(".block.contact .contact-form");

            if (contactForm.length != 0) {

                // Initialize contact form.
                this.form = new pl.ContactForm(contactForm.get(0), {
                    url: 'https://goplek.com/mailer/send-mail-v1.php',
                    //url: '/fragment/themes/cargobajanuevo/process-ajax.php',
                    inputSelectors: [
                        "input[type=text]",
                        "input[type=hidden]",
                        "textarea",
                    ],
                    useAjax: true,
                });

                this.onError = this.onError.bind(this);
                this.onSuccess = this.onSuccess.bind(this);
                this.onSending = this.onSending.bind(this);

                this.form.error.add(this.onError);
                this.form.success.add(this.onSuccess);
                this.form.sending.add(this.onSending);
                this.form.inputError.add(this.handleInputError.bind(this));
            }
        },
        /**
         * Fires if message fails.
         * @param {HTMLElement} form
         * @param {string} status
         * @param {string} statusText
         */
        onError: function (status, statusText, form) {
            let statusMessage = document.querySelector('.status-message');
            statusMessage.classList.remove('d-none');
            statusMessage.innerHTML = '<div class="error">Hubo un error al enviar tu mensaje, por favor intentalo más tarde</div>';
            this.removeFormStatus(form);
            //Page.ga('Contacto', 'Error', 'Error al enviar mensaje');
        },
        /**
         * Handle input error event.
         * @param {HTMLInputElement} input
         */
        handleInputError: function (input) {
            let clue = input.dataset['clue'];
            if (clue) {
                input.setAttribute('placeholder', clue);
            }
        },
        /**
         * Fires if message is sended successfully.
         * @param {HTMLElement} form
         * @param {any} response
         * @param {string} status
         * @param {string} statusText
         */
        onSuccess: function (response, status, statusText, form) {
            let statusMessage = document.querySelector('.status-message');
            statusMessage.classList.remove('d-none');
            console.log('Success ...');
            statusMessage.innerHTML = '<div class="success"> <div class="load-image-success"><img src="fragment/themes/cargoBaja/design/imgs/check.svg" alt="" class="img-fluid"></div>' + '<div class="message-success"> <b>Tu mensaje se ha enviado correctamente</b> <br> En breve nos pondremos en contacto contigo </div> </div>';
            //Page.ga('Contacto', 'Enviado', 'Mensaje enviado');
            this.removeFormStatus(form);
        },
        /**
         * Fires when message is sending.
         * @param {HTMLElement} form
         */
        onSending: function (form) {
            let statusMessage = document.querySelector('.status-message');
            let elementHide = form.querySelector('.wrapper-content-form');
            elementHide.classList.add('partial-hide');
            statusMessage.classList.remove('d-none');
            statusMessage.innerHTML = '<div class="sending"><div class="load-image"><img src="fragment/themes/cargoBaja/design/imgs/send.svg" alt="" class="img-fluid"></div>' + '<div class="message"> Enviando mensaje </div></div>';
            //Page.ga('Contacto', 'Enviando', 'Enviando mensaje');
        },
        /**
         * Remove Text
         */
        removeFormStatus: function (form) {
            let statusMessage = document.querySelector('.status-message');
            let elementHide = form.querySelector('.wrapper-content-form');
            let delay = 4000;
            setTimeout(() => {
                /**
                 * Get inputs and textarea to reset the placeholder
                 */
                this.form.texts.forEach(input => {
                    input.setAttribute('placeholder', input.getAttribute('data-placeholder'));
                });
                this.form.reset();
                elementHide.classList.remove('partial-hide');
                statusMessage.classList.add('d-none');
            }, delay);
        },
    },
    /**
     * get id of click of element
     */
    Pagination: {
        /**
         * Initialize function
         */
        init: function () {
            let process_elements = document.querySelectorAll('.mark-section');
            [].forEach.call(process_elements, (process, index) => {
                process.addEventListener('click', (ev) => {
                    ev.preventDefault();
                    Page.Pagination.resetStatus(process_elements);
                    pl.Classie.addClass(process, 'mark-visited');
                    Page.currentSlide = index;
                    Page.updateScrollX();
                });
            });

            Page.updateScrollX();
        },
        /**
         * Reset all status
         * Patch in order to close all elements on click
         */
        resetStatus: function (section) {
            [].forEach.call(section, ele => {
                pl.Classie.removeClass(ele, 'mark-visited');
            });
        },

    },

    updateScrollX: function () {
        let parentSlides = document.querySelector('.inner-outer-wrapper');
        let slides = document.querySelector('.slide-process');
        if (slides){
            let slide_width = slides.clientWidth;
            Page.scrollX = Page.currentSlide * (-slide_width);
            Page.updateDots();
            Page.toggleActiveDots();

            parentSlides.style.webkitTransition = `webkitTransform 650ms`;
            parentSlides.style.transition = `transform 650ms`;

            parentSlides.addEventListener(Page.transitionSelect(), function () {
                parentSlides.style.webkitTransition = `none`;
                parentSlides.style.transition = `none`;
            });

            // region Canvas animation
            let currentSlide = parentSlides.children[Page.currentSlide];
            let animNames = currentSlide.dataset['anim'].split(',');

            for (let i = 0; i < animNames.length; i++) {
                let animName = animNames[i];

                if (!Page.animNames.length || Page.animNames.indexOf(animName) === -1) {
                    console.log(animName);
                    Page.animNames.push(animName);
                    window.CanvAnim[animName].call();
                    currentSlide.classList.add('viewed');
                }
            }

            // endregion

            Page.updateWrapper();
        }
    },

    updateDots: function () {
        let dotContainer = q('.inner-wrapper-mark');
        let dot = dotContainer.children[0];
        let windowWidth = window.innerWidth;
        let dotWidth = dot.clientWidth;
        let dotsInview = Math.floor(windowWidth / dotWidth);
        let offset = 0;
        if (Page.currentSlide >= dotsInview) {
            offset = ((Page.currentSlide + 1) - dotsInview) * dotWidth;
        }
        dotContainer.style.transform = 'translateX(' + -offset + 'px)';
    },

    toggleActiveDots: function () {
        let dotContainer = q('.inner-wrapper-mark');
        let dots = qa('a', dotContainer);
        [].forEach.call(dots, (dot, index) => {
            if (Page.currentSlide === index) {
                pl.Classie.addClass(dot, 'mark-visited')
            }
            else {
                pl.Classie.removeClass(dot, 'mark-visited')
            }
        });
    },

    /**
     * detect swipe to responsive version
     */
    Swipe_Pagination: function () {
        let parentContent = document.getElementById('wrapper');
        if (parentContent) {
            Page.totalSlides = document.querySelectorAll('.slide-process').length;

            parentContent.addEventListener('touchstart', this.handleTouchStart.bind(this), false);
            parentContent.addEventListener('touchmove', this.handleTouchMove.bind(this), false);

        }
    },
    handleTouchStart: function (ev) {
        this.xDown = ev.touches[0].clientX;
        this.yDown = ev.touches[0].clientY;
    },
    handleTouchMove: function (ev) {
        if (!this.xDown || !this.yDown) {
            return;
        }

        let xUp = ev.touches[0].clientX;
        let yUp = ev.touches[0].clientY;

        let xDiff = this.xDown - xUp;
        let yDiff = this.yDown - yUp;

        if (Math.abs(xDiff) > Math.abs(yDiff)) {/*most significant*/
            if (xDiff > 0) {
                /* left swipe */
                Page.currentSlide = Page.currentSlide + 1;
            } else {
                /* right swipe */
                Page.currentSlide = Page.currentSlide - 1;
            }
            if (Page.currentSlide >= 0 && Page.currentSlide < Page.totalSlides) {
                Page.updateScrollX();
            }
        } else {
            if (yDiff > 0) { /* up swipe */
            }
            else { /* down swipe */
            }
        }

        /* reset values */
        this.xDown = null;
        this.yDown = null;
    },

};
$(Page.init);