/*
 * jquery.slider.js
 */

(function($){

    var Slider = (function(){

        /**
         * Slider
         *
         * TODO: 1. Create elements only if they will be used.
         *       2. Organize the code.
         *       3. Rename methods.
         *       4. Generalize some measures.
         *
         * @constructor
         *
         * @param {jQuery} element
         * @param {Object} settings
         * @param {function} callback
         */
        function Slider(element, settings, callback){
            var _this = this;

            this.$window   = $(window);
            this.element   = element;
            this.settings  = settings;
            this.arrSlides = this.element.children(this.settings.searchFor);

            this.currentSlide = null;
            this.index        = 0;
            this.totalslides  = this.arrSlides.length;
            this.itvl         = null;
            this.slides       = $('<div class="slides">');
            this.middlepoint  = 0;

            // Initialize when images are already loaded.
            if(this.settings.searchFor === 'img' && this.arrSlides.length){
                this.waitForImages(this.arrSlides, function(){ _this.initialize(callback); });

            } else {
                this.images = this.arrSlides.find('img');

                if(this.images.length) {
                    this.waitForImages(this.images, function(){ _this.initialize(callback); });

                } else {
                    this.initialize(callback);
                }

            }

        }

        /**
         * Fires when window resize.
         *
         * @private
         */
        Slider.prototype._onWindowResize = function(){

            // Calculate new height.
            this.calcAspectRatio();

            if(this.settings.searchFor === 'img'){
                // Resize current image.
                this.adaptImage(this.currentSlide);
            } else {
                this.adaptImage(this.currentSlide.children('.background').children('img'));
            }

            // Align arrows.
            if(this.settings.showArrows)
                this.alignArrows();

            // Align thumbnails
            if(this.settings.showThumbnails && this.settings.searchFor === 'img')
                this.alignThumbs();

            // Show or hide image labels.
            if(this.settings.showLabels && this.settings.searchFor === 'img')
                this.displayLabels();

            //
            this.middlepoint  = this.slides.width() / 2;

        };

        /**
         * Adapt image with respect to his parent.
         *
         * @param {jQuery} images
         */
        Slider.prototype.adaptImage = function(images){
            var _this = this;

            function adapt(image){

                if(image.is(':visible')){
                    _this.resizeImage(image);
                    _this.centerItem(image, true);
                }
            };

            if(images.length > 1)
                images.each(function(){ adapt($(this)); });

            else
                adapt(images);

        };

        /**
         * Align arrows to his container.
         */
        Slider.prototype.alignArrows = function(){

            this.centerItem(this.prevButton, true, 'VERTICAL');
            this.centerItem(this.nextButton, true, 'VERTICAL');

        };

        /**
         *
         */
        Slider.prototype.actualizeThumbs = function(){
            var tinner     = this.thumbs.children('.inner'),
                posarr     = this.thumbs.data('posarr'),
                offset     = posarr[this.index],
                marginleft = 0;

            if((tinner.width() - offset - this.element.width()) >= 0){
                marginleft = offset;
            } else {
                marginleft = offset + (tinner.width() - offset - this.element.width());
            }

            tinner.stop().animate({'margin-left': -marginleft});

        };

        /**
         * Align thumbnails wrapper.
         */
        Slider.prototype.alignThumbs = function() {
            if (this.thumbs.width() > this.thumbs.children('.inner').width()) {
                this.thumbs.children('.inner').css('margin', '0 auto');
                this.thumbs.data('animate', false);

            } else {
                this.thumbs.children('.inner').css('margin', '0');
                this.thumbs.data('animate', true);

                this.actualizeThumbs();
            }

        };

        /**
         * Determine the animation for the slider.
         */
        Slider.prototype.animation = function(){
            var _this = this; // Closure.

            // Actualize loading bar.
            if (this.settings.showLoadingBar)
                this.moveLoadingbar();

            // Actualize slide.
            // this.currentSlide = this.arrSlides.eq(this.index).hide();
            this.currentSlide = this.arrSlides.eq(this.index);

            /* this.slides.children(this.settings.searchFor)
                .stop(true, true)
                .fadeOut(this.settings.transitionTime, function(){ $(this).remove(); }); */

            this.slides.children(this.settings.searchFor).removeClass('current');
            setTimeout(() => {
                this.slides
                    .children(this.settings.searchFor)
                    .not(this.currentSlide)
                    .remove();
            }, this.settings.transitionTime);


            this.slides.append(this.currentSlide);

            // Before slide change.
            _this.element.trigger('before-slide-change', [_this.currentSlide, _this.arrSlides]);

            /* this.currentSlide.stop(true, true).fadeIn(this.settings.transitionTime, function(){
                // After slide change
                _this.element.trigger('after-slide-change', [_this.currentSlide, _this.arrSlides]);
            }); */
            window.getComputedStyle(this.currentSlide.get(0)).opacity;
            this.currentSlide.addClass('current');
            setTimeout(() => {
                _this.element.trigger('after-slide-change', [_this.currentSlide, _this.arrSlides]);
            }, this.settings.transitionTime);

            // Temporal
            if(this.settings.searchFor === 'img'){
                this.adaptImage(this.currentSlide);
            } else {
                var image = this.currentSlide.children('.background').children('img');

                this.adaptImage(image);

            }

            // Show image label (Optional).
            if(this.settings.showLabels && this.settings.searchFor === 'img'){
                if(!this.altElement){
                    this.altElement = $('<div class="alt">');
                    this.slides.append(this.altElement);
                }

                var altText         = $('<div class="strong">').text(this.currentSlide.attr('alt') || ''),
                    descriptionText = $('<div class="light">').text(this.currentSlide.data('description') || '');

                this.altElement
                    .empty()
                    .append(altText, descriptionText);

                this.slides.children('.alt').stop(true, true).fadeOut(this.settings.transitionTime);

                if(this.displayLabels()){
                    _this.altElement.stop(true, true).fadeIn(this.settings.transitionTime);
                }

            }

            // Actualize thumbnail (Optional).
            if(this.settings.showThumbnails && this.settings.searchFor === 'img'){
                var thumb = this.thumbs.find('.thumb').eq(this.index),
                    animate = this.thumbs.data('animate');

                thumb
                    .addClass('selected')
                    .siblings('.thumb')
                    .removeClass('selected');

                if(animate){
                    this.actualizeThumbs();
                }

            }

            // Actualize dots (Optional).
            if(this.settings.showDots){
                var activeDot = this.dots.children('.dot').eq(this.index);

                activeDot.addClass('active')
                    .siblings()
                    .removeClass('active');

            }

        };

        /**
         * Keep aspect ratio.
         * original height / original width x new width = new height
         *
         * @returns number
         */
        Slider.prototype.calcAspectRatio = function(){

            if(!this.settings.calcAspectRatio)
                return true;

            var aspectRatio = this.settings.aspectRatio,
                newHeight  = 0;

            newHeight = aspectRatio.h / aspectRatio.w * this.slides.width();

            this.slides.height(newHeight);

        };

        /**
         * Description for centerItem.
         *
         * @param {jQuery} item
         * @param {boolean} toParent
         * @param {string} orientation
         */
        Slider.prototype.centerItem = function(item, toParent, orientation){

            var container = (toParent) ? item.parent() : $(window);
            var position  = item.css('position');
            var o         = (typeof orientation === 'string') ? orientation : 'BOTH';

            // Obtains values.
            var l = (container.outerWidth() - item.outerWidth(true)) / 2,
                t = (container.outerHeight() - item.outerHeight(true)) / 2;

            // Check if the position is absolute to asign a left and top values.
            if(position === 'absolute' || position === 'fixed'){
                if(o === 'VERTICAL' || o === 'BOTH') item.css({'top': t});
                if(o === 'HORIZONTAL' || o === 'BOTH') item.css({'left': l});

            } else {
                if(o === 'VERTICAL' || o === 'BOTH') item.css({'margin-top': t});
                if(o === 'HORIZONTAL' || o === 'BOTH') item.css({'margin-left': l});

            }

        };

        /**
         * Add buttons to the slider.
         */
        Slider.prototype.createControls = function(){
            var _this = this;
            var controls = $('<div class="controls">');

            this.prevButton   = $('<div class="previous-slide">');
            this.nextButton   = $('<div class="next-slide">');

            this.slides.append(
                controls,
                this.prevButton,
                this.nextButton
            );

            this.alignArrows();

            // Calc position of the mouse over the slides container.
            this.middlepoint  = this.slides.width() / 2;
            var direction = 'right';
            this.slides.on('mousemove', function(e){

                if (e.offsetX < _this.middlepoint) {
//                    _this.prevButton.removeClass('hide');
//                    _this.nextButton.addClass('hide');

                    direction = 'left';

                } else if(e.offsetX > _this.middlepoint) {
//                    _this.prevButton.stop().addClass('hide');
//                    _this.nextButton.stop().removeClass('hide');

                    direction = 'right';

                }

                // console.log(e.offsetX + ', ' + e.offsetY);
            });

            this.slides.click(function(){
                _this.isNext(direction);
                _this.resetTimer();
            });
        };

        /**
         * Add dots to the slider.
         */
        Slider.prototype.createDots = function(){
            var _this = this;
            this.dots = $('<div class="dots">');

            this.element.append(this.dots);

            this.arrSlides.each(function(){
                var dot = $('<span class="dot">').appendTo(_this.dots);

                dot.click(function(){
                    var index = $(this).index();

                    if (index !== _this.index) {
                        _this.index = index;

                        _this.resetTimer();
                        _this.animation();
                    }
                });

            });

        };

        /**
         * Create slides, buttons and thumbnails.
         */
        Slider.prototype.createLayout = function(){
            var _this = this; // Closure

            this.element.append(this.slides);

            //
            this.calcAspectRatio();

            if(this.settings.searchFor === 'img'){

                // Show Thumbnails.
                if(this.settings.showThumbnails)
                    this.createThumbs();

            }

            // Show Arrows
            if(this.settings.showArrows)
                this.createControls();

            // Show Dots
            if(this.settings.showDots)
                this.createDots();

            // Show loading bar.
            if (this.settings.showLoadingBar)
                this.createLoadingBar();

            // Active key controls.
            if(this.settings.activeKeyControls){
                this.$window.on('keydown', function(e){

                    if(e.keyCode === 37){
                        _this.isNext('left');
                        _this.resetTimer();

                    } else if(e.keyCode === 39){
                        _this.isNext('right');
                        _this.resetTimer();

                    } else { /* Do nothing */ }

                });
            }

        };

        /**
         * Create thumbs.
         */
        Slider.prototype.createThumbs = function(){
            var _this = this; // Closure

            this.thumbs = $('<div class="thumbs">');

            var inner = $('<div class="inner">'),
                width = 0,
                posarr = [];

            // Add to container.
            this.element.append(this.thumbs);

            // Add inner to thumbs element.
            this.thumbs.append(inner);

            /* Create thumbnails */
            this.arrSlides.each(function(){
                var image = $(this).clone(),
                    thumb = $('<div class="thumb">').append(image);

                inner.append(thumb);

                posarr.push(width);
                width += thumb.width();

                this.resizeImage(image);
                this.centerItem(image, true);

                image.fadeIn(function(){
                    var $this = $(this);

                    $this.hover(
                        function(){ $this.stop().animate({'opacity': 1}); },
                        function(){ $this.stop().animate({'opacity': .65}); }
                    );

                    thumb.click(function(){
                        var index = $(this).index();

                        if (index !== _this.index) {
                            _this.index = index;

                            _this.resetTimer();
                            _this.animation();
                        }

                    });
                });

            });

            inner.width(width);
            this.thumbs.data('posarr', posarr);

            this.alignThumbs();

        };

        /**
         * Create loading bar.
         */
        Slider.prototype.createLoadingBar = function() {
            this.loadingBar = $('<div class="loading-bar">');

            this.element.append(this.loadingBar);
        };

        /**
         *
         * @returns {Boolean}
         */
        Slider.prototype.displayLabels = function(){
            this.altElement.show();

            return true;

            /*if (this.slides.height() < 275) {
             this.altElement.hide();

             return false;

             } else {
             this.altElement.show();

             return true;
             }*/

        };

        /**
         * Initialize slider.
         *
         * @param {function} callback
         */
        Slider.prototype.initialize = function(callback){
            var _this = this;

            this.element.removeClass('loading');

            // Remove spinner.
            this.element.css('background-image', 'none');

            // Clean container.
            this.arrSlides.detach();
            this.element.empty();

            // Add elements of slider.
            this.createLayout();

            // Start with the first element.
            this.animation();

            // Initialize interval.
            if(this.arrSlides.length > 1 && this.settings.autoPlay)
                this.itvl = setInterval(function(){ _this.isNext('right'); }, this.settings.displayTime);

            // Add resize handler.
            this.$window.on('resize', function(){ _this._onWindowResize(); });

            if(callback && typeof callback === 'function')
                callback(this);

        };

        /**
         * Determine next image to show.
         *
         * @param {string} direction
         */
        Slider.prototype.isNext = function(direction){

            if(direction === 'left'){
                (this.index <= 0)
                    ? this.index = this.totalslides - 1
                    : this.index --;

            } else{
                (this.index >= this.totalslides - 1)
                    ? this.index = 0
                    : this.index ++;

            }

            // Animate with new index.
            this.animation();

        };

        /**
         * Start to animate loading bar.
         */
        Slider.prototype.moveLoadingbar = function () {
            var self       = this;
            var start      = Date.now();
            var to         = 100;
            var loadingBar = this.loadingBar.get(0);
            var show       = loadingBar.dataset.show || 'show';

            // Reset animation if exist.
            if (typeof this.loadingBarId === 'number')
                window.clearTimeout(this.loadingBarId);


            // Initialize
            loadingBar.style.bottom = (show === 'show') ? 'auto' : '0';
            loadingBar.style.top    = (show === 'show') ? '0'    : 'auto';
            loadingBar.style.height = (show === 'show') ? '0%'   : '100%';

            loadingBar.dataset.show = (show === 'show') ? 'hide' : 'show';


            this.loadingBarId = setInterval(function() {
                var timeElapsed = Date.now() - start;
                var progress = timeElapsed / self.settings.displayTime;

                if (progress > 1) progress = 1;


                if (show === 'show')
                    loadingBar.style.height = to * progress + "%";
                else
                    loadingBar.style.height = to - (to * progress) + "%";


                if (progress == 1) window.clearTimeout(self.loadingBarId);

            }, 1000 / 60);
        };

        /**
         *
         */
        Slider.prototype.randomize = function(){
            function shuffle(o) { //v1.0
                for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
                return o;
            }
        };

        /**
         * Reset the timer.
         */
        Slider.prototype.resetTimer = function(){
            var _this = this; // Closure

            if(this.itvl && typeof this.itvl === 'number' && this.arrSlides.length > 1){
                clearInterval(this.itvl);
                this.itvl = setInterval(function(){ _this.isNext('right'); }, this.settings.displayTime);
            }

        };

        /**
         * Fill the container with the image.
         * Note: Make sure that the image is fully loaded.
         *
         * @param {jQuery} image
         */
        Slider.prototype.resizeImage = function(image){
            var container = image.parent();

            var width = container.width();
            var height = container.width() * image.height() / image.width();

            if( height < container.height() ){
                height = container.height();
                width = container.height() * image.width() / image.height();
            }

            image.height(height);
            image.width(width);
        };

        /**
         * Wait to the images are already loaded, then excecute a callback.
         *
         * @param {jQuery} images
         * @param {function} callback
         */
        Slider.prototype.waitForImages = function(images, callback){
            var totalimages = images.length,
                loadedimages = 0;

            function onImageLoaded(){

                if(++loadedimages >= totalimages)
                    if(callback && typeof callback === 'function')
                        callback();

            }

            images.each(function(){
                var image = $(this);

                if(image.get(0).complete)
                    onImageLoaded();

                else
                    image.load(function(){ onImageLoaded(); });
            });

        };

        return Slider;

    })();

    $.fn.slider = function(options, callback){

        var settings = $.extend({
            autoPlay: true,
            aspectRatio: { h: 520, w: 825},
            activeKeyControls: false,
            calcAspectRatio: false,
            displayTime: 10000,
            searchFor: 'img',
            showThumbnails: false,
            showArrows: true,
            showLabels: false,
            showDots: false,
            showLoadingBar: false,
            transitionTime: 400
        }, options);

        return this.each(function(){
            var $element = $(this);

            if($element.find(settings.searchFor).length > 0)
                new Slider($element, settings, callback);

        });

    };

})(jQuery);