/**
 * Created by cesarmejia on 27/09/17.
 */

var CheckoutForm = (function () {

    /**
     * Create a checkout form instance.
     * @constructor
     * @param {HTMLElement} form
     */
    function CheckoutForm(form) {
        this.form = form;
        this.response = document.querySelector('.response');
        this.inputs = CheckoutForm.getFormInputs(this.form);
        this.request = new XMLHttpRequest();

        this.initEvents();

        CheckoutForm.instance = this;
    }

    /**
     * @type CheckoutForm
     */
    CheckoutForm.instance = null;

    /**
     * Get form inputs.
     * @static
     * @param {HTMLElement} form
     * @returns {NodeList}
     */
    CheckoutForm.getFormInputs = function (form) {
        var validInputs = [
            "input[type=text]",
            "input[type=password]",
            "select"
        ];

        return form.querySelectorAll(validInputs.join(","));
    };

    /**
     * Handle Banamex cancel event.
     */
    CheckoutForm.handleBanamexCancel = function () {
        "console" in window && console.log('Cancel by user.');

        CheckoutForm.instance.showLoader(false);
    };

    /**
     * Handle Banamex error event.
     * @param {Object} error Details of the error.
     * @param {string} cause Cause of the error. One of INVALID_REQUEST, SERVER_BUSY, SERVER_FAILED, CONSTRAINT_VIOLATION, REQUEST_REJECTED
     * @param {string} explanation Human readable description of the error.
     * @param {string} field Indicates the name of the field that failed validation or has violated the payment constraints.
     * @param {string} result Fixed value = ERROR
     * @param {string} validationType Indicates the type of field validation error.
     */
    CheckoutForm.handleBanamexError = function (error, cause, explanation, field, result, validationType) {
        "console" in window && console.log(
            'There was an error. Please check following list:\nError: %o\nCause: %s\nExplanation: %s\nField: %s\nResult: %s\nValidationType: %s',
            error, cause, explanation, field, result, validationType
        );
    };

    /**
     * Handle Banamex complete event.
     * @param {string} resultIndicator The result indicator can be used to securely determine if the payment succeeded in a Return To Merchant integration.
     * @param {string} sessionVersion Version of the Payment Session after payment completed.
     */
    CheckoutForm.handleBanamexComplete = function (resultIndicator, sessionVersion) {

        console.log(resultIndicator);
        console.log(sessionVersion);

        "console" in window && console.log("Successfull payment!");

        // 1. Send mail by ajax
        // 2. Show success message
        var form = CheckoutForm.instance;

        form.ajaxRequest({ method: 'sendConfirm' }, function (res) {
            var message = "";
            try {
                var response = JSON.parse(res);
                var data = JSON.parse(response.data);
                var customer = data.customer;
                var transaction = data.transaction[0];
                var order = transaction.order;
                var gettingDate = new Date();
                var actualDate = gettingDate.getDate() + "/" + (gettingDate.getMonth() + 1) + "/" + gettingDate.getFullYear();
    
                // 2. Build successful message.
                message += "<div class='inner'>" +
                    "<div class='heading'>" +
                    "<div><b>Tu pago se realizó exitosamente</b></div>" +
                    "<div>En breve recibirás en tu correo la confirmación de pago.</div>" +
                    "</div>" +
                    "<div class='body'>" +
                    "<h3>Detalle de Pago</h3>" +
                    // Purchase ID's
                    "<div class='data'>" +
                    "<div><b>ID Pedido</b>: " + data['id'] + "</div>" +
                    "<div><b>Código de Autorización</b>: " + transaction['transaction']['authorizationCode'] + "</div>" +
                    "</div>" +
                    // Customer
                    "<div class='data'>" +
                    "<div><b>Fecha</b>: " + actualDate + "</div>" +
                    "<div><b>Nombre</b>: " + customer['firstName'] + " " + customer['lastName'] + "</div>" +
                    "<div><b>Email</b>: " + customer['email'] + "</div>" +
                    "<div><b>Teléfono</b>: " + customer['phone'] + "</div>" +
                    "</div>" +
                    // Detail
                    "<div class='data'>" +
                    "<div><b>Detalle</b>: " + order['description'] + "</div>" +
                    "<div><b>Subtotal</b>: " + order['amount'] + " " + order['currency'] + "</div>" +
                    "<div><b>Total</b>: " + order['amount'] + " " + order['currency'] + "</div>" +
                    "</div>" +
                    "</div>" +
                    "<div>";
    
                // 3. Display the success message.
                form.showResponse(true, message);
    
            } catch (error) {
                console.error("Error processing payment response:", error);
            }

        });

    };


    // Define CheckoutForm prototype.
    CheckoutForm.prototype = {


        /**
         * Make an ajax request.
         * @param {function} callback
         */
        ajaxRequest: function (data, callback) {
            var self   = this;
            var async  = true;
            var method = "POST";
            var url    = "/fragment/themes/cargobajanuevo/crear-sesion.php";

            this.showLoader(true);

            this.request.onreadystatechange = function () {
                var DONE = 4;   // readyState 4 means the request is done.
                var OK   = 200; // status 200 is a successful return.

                if (self.request.readyState === DONE) {
                    if (self.request.status === OK) {
                        callback.call(
                            self,
                            self.request.responseText,
                            self.request.status,
                            self.request.statusText
                        );
                    } else {
                        callback.call(
                            self,
                            self.request.status,
                            self.request.statusText
                        );
                    }
                }
            };

            this.request.open(method, url, async);
            this.request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            this.request.send("data=" + JSON.stringify(data));

        },

        /**
         * Request to check a cardholder's enrollment in the 3DSecure scheme.
         * @param {string} orderId
         * @param {string} sessionId
         * @param {function} callback
         */
        check3DSEnrollment: function (orderId, sessionId, callback) {
            this.ajaxRequest({method: "check3DSEnrollment", orderId: orderId, sessionId: sessionId}, callback);
        },

        /**
         * Disable or enable inputs depending on the "disable" flag
         * @param {boolean} disable
         */
        disableInputs: function (disable) {
            for (var i = 0; i < this.form.elements.length; i++) {
                (function (input) {
                    input.disabled = disable;
                })(this.form.elements[i]);
            }
        },

        /**
         * Get session id.
         * @param {function} callback
         */
        getSessionId: function (callback) {
            var name = this.form.querySelector('input[data-key="name"]').value;
            var company = this.form.querySelector('input[data-key="company"]').value;
            var phone = this.form.querySelector('input[data-key="phone"]').value;
            var email = this.form.querySelector('input[data-key="email"]').value;
            var quote = this.form.querySelector('input[data-key=quote]').value || "Orden-" + Date.now();
            var invoice = this.form.querySelector('input[data-key="invoice"]').value;
            var amount = this.form.querySelector('input[data-key=amount]').value;
            var currency = this.form.querySelector('select[data-key=currency]').value;

            // Crear el objeto con los datos del formulario
            var formData = {
                nombre: this.form.querySelector('input[data-key="name"]').value,
                compania: this.form.querySelector('input[data-key="company"]').value,
                telefono: this.form.querySelector('input[data-key="phone"]').value,
                correo: this.form.querySelector('input[data-key="email"]').value,
                cotizacion: this.form.querySelector('input[data-key=quote]').value || "Orden-" + Date.now(),
                factura: this.form.querySelector('input[data-key="invoice"]').value,
                monto: this.form.querySelector('input[data-key=amount]').value,
                currency: this.form.querySelector('select[data-key=currency]').value
            };

            // Guardar el objeto en localStorage como un string JSON
            localStorage.setItem('paymentData', JSON.stringify(formData));

            this.ajaxRequest({
                method: "getSessionId",
                nombre: name,
                compania: company,
                telefono: phone,
                correo: email,
                cotizacion: quote,
                factura: invoice,
                monto: amount,
                currency: currency
            }, callback);
        },

        /**
         * Go to banamex checkout.
         * @param {string} orderid
         * @param {string} sessionid
         */
        goCheckout: function (orderid, sessionid) {
            var MERCHANT_ID = "1023085HPP";
            var CURRENCY = this.form.querySelector('select[data-key=currency]').value;
            var LOCALE = CURRENCY === "MXN" ? "es_MX" : "en_US";
        
            var config = {
                "merchant": MERCHANT_ID,
                "order": {
                    "amount": this.form.querySelector('input[data-key=amount]').value,
                    "currency": CURRENCY,
                    "description": "Pago de servicio"
                },
                "interaction": {
                    "merchant": {
                        "email": "bancos@cargobaja.com",
                        "logo": "https://goplek-net.s3.amazonaws.com/59dcf458ddafb-cargo-baja-logo.png",
                        "name": "CargoBaja"
                    },
                    "locale": LOCALE
                },
                "customer": {
                    "email": this.form.querySelector('[data-key=email]').value,
                    "firstName": this.form.querySelector('[data-key=name]').value,
                    "phone": this.form.querySelector('[data-key=phone]').value
                },
                "session": {
                    "id": sessionid
                }
            };
        
            // Configura el checkout de Banamex
            Checkout.configure(config);
            Checkout.showLightbox();
        },

        /**
         * Initialize events.
         */
        initEvents: function () {
            var self = this;

            this.onInputChange = this.onInputChange.bind(this);
            this.onSubmit = this.onSubmit.bind(this);

            // Attach change handler to each input.
            [].forEach.call(this.inputs, function (input) {
                input.addEventListener('change', self.onInputChange, false);
            });


            // Attach submit handler to form.
            this.form.addEventListener('submit', this.onSubmit, false);

        },

        /**
         * Determine if all form fields are valid.
         * @returns {boolean}
         */
        isFormValid: function () {
            var self  = this,
                valid = true;

            [].forEach.call(this.inputs, function (input) {
                if (!self.isInputValid(input)) {
                    self.toggleInputError(input);
                    valid = false;
                }
            });

            return valid;
        },

        /**
         * Determine if an input is valid.
         * @param {HTMLElement} input
         * @returns {boolean}
         */
        isInputValid: function (input) {
            var value      = input.value,
                validation = input.dataset.validation;

            if ("string" !== typeof value)
                return false;

            // Note: This logic should not be in here!
            if (validation === 'restrict') {
                var elements = this.form.querySelectorAll('input[data-validation=restrict]');
                var valid = false, i, el;

                for (i = 0; el = elements[i], i < elements.length; i++) {
                    if (el.value !== "") {
                        valid = true;
                    }
                }

                // Remove class invalid if one of restrict elements are true.
                if (valid) {
                    for (i = 0; el = elements[i], i < elements.length; i++) {
                        Util.removeClass(el, 'invalid');
                    }
                }


                return valid;
            }


            switch (validation) {
                case 'notEmpty':
                    return value.length > 0;
                case 'phone':
                    return value.replace(/[^\d]/g, '').length === 10;
                case 'email':
                    return /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/.test(value);
                case 'number':
                    return /\d+/.test(value);
                case 'amount':
                    return /^\d+(\.\d{0,2})?$/.test(value) && value.length <= 14;
                case 'cardNumber':
                    return /(\d{4}\-?){3}\d{4}/.test(value);
                case 'securityCode':
                    return /\d{3,4}/.test(value);

                default:
                    "console" in window && console.log("Validation not recognized.");
                    return false;

            }
        },

        /**
         * Handles input change event.
         * @param {Event} ev
         */
        onInputChange: function (ev) {
            var input = ev.target;

            this.toggleInputError(input);
        },

        /**
         * Handles submit event.
         * @param {Event} ev
         */
        onSubmit: function (ev) {
            ev.preventDefault();

            if (!this.isFormValid()) {
                // Show error message.
                console.log('Datos inválidos');

            } else {
                this.getSessionId(function (res) {
                    var resp = JSON.parse(res);
                    if(resp.orderId) {
                        localStorage.setItem('orderId', resp.orderId)
                    }
                    try {
                        var response = JSON.parse(res);
                        console.log("Respuesta del servidor completa:", response);

                        if (response.sessionId) {
                            Checkout.configure({
                                session: { id: response.sessionId }
                            });
                            Checkout.showEmbeddedPage('#embed-target');

                            this.showLoader(false);
                        } else {
                            console.error("No se pudo obtener la sesión de pago");
                        }
                    } catch (error) {
                        console.error("Error al analizar la respuesta:", error);
                    }
                });
            }

        },

        /**
         * Reset form inputs.
         */
        resetForm: function () {
            this.form.reset();
        },

        /**
         * Show loader
         * @param {boolean} show
         */
        showLoader: function (show) {
            if (show) {
                this.form.className += " loading";
                this.response.className += " loading";
                this.disableInputs(true);
            } else {
                this.form.className = this.form.className.replace(/\b\s?loading\b/g, "");
                this.response.className = this.form.className.replace(/\b\s?loading\b/g, "");
                this.disableInputs(false);
            }
        },

        /**
         * Show loader
         * @param {boolean} show
         * @param {string} message
         */
        showResponse: function (show, message) {
            this.form.className += " response-visible";

            this.showLoader(false);
            this.resetForm();

            this.response.innerHTML = message;
        },

        /**
         * Add or remove invalid class to an input.
         * @param input
         */
        toggleInputError: function (input) {
            if (this.isInputValid(input)) {
                Util.removeClass(input, 'invalid');
            } else {
                Util.addClass(input, 'invalid');
            }
        }

    };


    return CheckoutForm;

})();