/**
 * Created by sexar on 29/05/15.
 */
/// <reference path="jquery.d.ts" />
var util;
(function (util) {
    var Carrousel = (function () {
        /**
         * Create Carrousel
         *
         * @param {jQuery} elem
         * @param {Object} settings
         * @param {function} callback
         */
        function Carrousel(elem, settings, callback) {
            var _this = this;
            if (settings === void 0) { settings = {}; }
            if (callback === void 0) { callback = null; }
            // Terminate if no elem or no thumbs.
            if (!(elem && elem instanceof jQuery && elem.length))
                return;
            this.$window = $(window);
            this.elem = elem;
            this.thumbs = this.elem.find('.thumb').detach();
            this.increasedThumbs = this.thumbs;
            this.thumbsImages = this.increasedThumbs.find('.image img');
            this.settings = $.extend(this.getDefaultSettings(), settings);
            this.currentThumb = 0;
            this.interval = null;
            this.createLayout();
            this.calculateWidths();
            // Show images.
            this.displayImages(this.thumbsImages);
            // Initialize auto play.
            this.initAutoPlay();
            // Attach hander on resize.
            this.$window.on('resize', function () { _this.onResize(); });
            // Execute callback when all instructions in constructo are executed.
            if (typeof callback === 'function') {
                callback(this);
            }
        }
        /**
         * Fires on resizing.
         */
        Carrousel.prototype.onResize = function () {
            this.calculateWidths();
            this.adaptImage(this.thumbsImages);
            this.animate(false);
        };
        /**
         * Return default settings of carrousel.
         * @returns Object
         */
        Carrousel.prototype.getDefaultSettings = function () {
            return {
                autoPlay: false,
                delay: 2000,
                infiniteScroll: true,
                thumbsToDisplay: 3,
                scaleImages: false,
                scrollSpeed: 500
            };
        };
        /**
         * Adapt image to his container.
         * @param images
         */
        Carrousel.prototype.adaptImage = function (images) {
            var _this = this;
            var adapt = function (image) {
                if (_this.settings.scaleImages)
                    _this.scaleImage(image, true);
                else
                    _this.resizeImage(image, true);
                _this.centerItem(image, true, 'BOTH');
            };
            images.each(function (index, element) { adapt($(element)); });
        };
        /**
         * Change margin left of thumbs container with animation.
         * @param duration
         * @param direction
         */
        Carrousel.prototype.animate = function (duration, direction = "next") {
            var _this = this;
            if (duration === void 0) { duration = true; }
            var offset = -(this.thumbs.outerWidth(true) * this.currentThumb);
            var time = (duration) ? this.settings.scrollSpeed : 0;
            // Add current class to the thumb.
            this.increasedThumbs.removeClass('current-thumb');
            this.increasedThumbs.eq(this.currentThumb).addClass('current-thumb');
            // Trigger before event
            this.elem.trigger('before-animate', [ _this.currentThumb, direction, _this.increasedThumbs ]);
            this.thumbsContainer
                .stop()
                .animate({ 'margin-left': offset }, time, 'swing', function () {
                    _this.elem.trigger('after-animate');
                    _this.hideArrow(_this.currentThumb);
                });
        };

        /**
         * Calculate width for thumbs and thumbs container.
         */
        Carrousel.prototype.calculateWidths = function () {
            var thumbMargin = parseInt(this.thumbs.css('margin-left')) + parseInt(this.thumbs.css('margin-right')), thumbWidth = (this.carrouselWrapper.width() / this.settings.thumbsToDisplay), totWidth = thumbWidth * this.increasedThumbs.length;
            this.thumbsContainer.width(totWidth);
            this.increasedThumbs.width(thumbWidth - thumbMargin);
        };
        /**
         * Center an item with respect to his parent.
         *
         * @param item
         * @param toParent
         * @param orientation
         */
        Carrousel.prototype.centerItem = function (item, toParent, orientation) {
            if (orientation === void 0) { orientation = 'BOTH'; }
            var container = (toParent) ? item.parent() : $(window), position = item.css('position');
            // Obtains values.
            var left = (container.outerWidth() - item.outerWidth()) / 2, top = (container.outerHeight() - item.outerHeight()) / 2;
            // Check if the position is absolute to asign a left and top values.
            if (position === 'absolute' || position === 'fixed') {
                if (orientation === 'HORIZONTAL' || orientation === 'BOTH')
                    item.css({ 'left': left });
                if (orientation === 'VERTICAL' || orientation === 'BOTH')
                    item.css({ 'top': top });
            }
            else {
                if (orientation === 'HORIZONTAL' || orientation === 'BOTH')
                    item.css({ 'margin-left': left });
                if (orientation === 'VERTICAL' || orientation === 'BOTH')
                    item.css({ 'margin-top': top });
            }
        };
        /**
         * Clone a certain number of thumbs to simulate an infinite carrousel.
         */
        Carrousel.prototype.cloneThumbs = function () {
            if (this.settings.infiniteScroll) {
                this.increasedThumbs = this.thumbs;
                for (var i = 0; i < this.settings.thumbsToDisplay; i++) {
                    let clon = this.thumbs.eq(i).clone();
                    clon.addClass('cloned');
                    this.increasedThumbs = this.increasedThumbs.add(clon);
                }
                this.thumbsImages = this.increasedThumbs.find('.image img');
            }
        };
        /**
         * Builds the layout for the carrousel.
         */
        Carrousel.prototype.createLayout = function () {
            var _this = this;
            // Create elements.
            this.carrouselWrapper = $('<div class="carrousel-wrapper">');
            this.thumbsContainer = $('<div class="thumbs-container clearfix">');
            this.prevButton = $('<div class="prev-button">');
            this.nextButton = $('<div class="next-button">');
            this.elem.append(this.prevButton, this.carrouselWrapper, this.nextButton);
            this.cloneThumbs();
            // Add thumbs to thumbs container.
            this.thumbsContainer.append(this.increasedThumbs);
            // Add thumbs container, prev button and next button to elem.
            this.carrouselWrapper.append(this.thumbsContainer);
            // Attach handlers to buttons.
            this.prevButton.on('click', function (event, autoClick) {
                _this.next('prev', autoClick);
                event.stopPropagation();
            });
            this.nextButton.on('click', function (event, autoClick) { _this.next('next', autoClick); event.stopPropagation();});
            // this.prevButton.on('swipe', function (event, autoClick) { _this.next('prev', autoClick); event.stopPropagation();});
            // this.nextButton.on('swipe', function (event, autoClick) { _this.next('next', autoClick); event.stopPropagation();});
            // this.prevButton.on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {e.stopPropagation();});
            // this.nextButton.on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {e.stopPropagation();});


        };
        /**
         * Display images when are already load.
         * @param images
         */
        Carrousel.prototype.displayImages = function (images) {
            var _this = this;
            images.each(function (index, element) {
                var image = $(element);
                _this.waitForImages(image, function () {
                    _this.adaptImage(image);
                    image.fadeIn('slow');
                });
            });
        };
        /**
         * Go to an specific thumb
         * @param {number} thumb
         */
        Carrousel.prototype.goTo = function (index) {
            this.currentThumb = index || 0;
            this.animate(0);
        };
        /**
         * Initialize autoplay.
         */
        Carrousel.prototype.initAutoPlay = function () {
            var _this = this;
            if (this.settings.autoPlay) {
                this.interval = window.setInterval(function () {
                    _this.nextButton.trigger('click', [true]);
                }, this.settings.delay);
            }
        };
        /**
         * Determine prev or next thumb.
         * @param direction
         * @param autoClick
         */
        Carrousel.prototype.next = function (direction, autoClick) {
            if (autoClick === void 0) { autoClick = false; }
            if (autoClick)
                this.resetAutoPlay();
            if (direction === 'prev') {
                if (this.currentThumb > 0) {
                    this.currentThumb--;
                    this.resetAutoPlay();
                }
                else {
                    if (this.settings.infiniteScroll) {
                        this.thumbsContainer.css('margin-left', -((this.thumbs.length) * this.thumbs.outerWidth(true)));
                        this.currentThumb = this.thumbs.length - 1;
                    }
                }
            }
            else {
                if (this.currentThumb < ((this.thumbs.length) - this.settings.thumbsToDisplay)) {
                    this.currentThumb++;
                    this.resetAutoPlay();
                }
                else {
                    if (this.settings.infiniteScroll) {
                        this.currentThumb = 1;
                        this.thumbsContainer.css('margin-left', 0);
                    }
                }
            }
            this.animate(true, direction);
        };
        /**
         * Initialize stopCarrousel.
         */
        Carrousel.prototype.stop = function () {
            if (this.interval) {
                window.clearInterval(this.interval);
            }
        };
        /**
         * Reset auto play.
         */
        Carrousel.prototype.resetAutoPlay = function () {
            if (this.interval) {
                window.clearInterval(this.interval);
                this.initAutoPlay();
            }
        };
        /**
         * Resize image with respect to his parent.
         * Note: Make sure that image is fully loaded.
         * @param {jQuery} image
         * @param {boolean} outerSizes
         * @returns {void}
         */
        Carrousel.prototype.resizeImage = function (image, outerSizes) {
            if (outerSizes === void 0) { outerSizes = false; }
            var container = image.parent(), cWidth = (outerSizes) ? container.outerWidth() : container.width(), cHeight = (outerSizes) ? container.outerHeight() : container.height(), iWidth = 0, iHeight = 0, width = 0, height = 0;
            // Retrieve original sizes.
            if (!image.data('pl-real-width'))
                image.data('pl-real-width', image.outerWidth());
            if (!image.data('pl-real-height'))
                image.data('pl-real-height', image.outerHeight());
            iWidth = image.data('pl-real-width');
            iHeight = image.data('pl-real-height');
            // Calculate new sizes.
            width = cWidth;
            height = cWidth * iHeight / iWidth;
            if (height < cHeight) {
                height = cHeight;
                width = cHeight * iWidth / iHeight;
            }
            // Asign new sizes to image.
            image.height(height);
            image.width(width);
        };
        /**
         * Scale image without lose aspect ratio.
         * Note: Make sure that the image is fully loaded.
         * @param {jQuery} image
         * @param {boolean} outerSizes
         * @return {void}
         */
        Carrousel.prototype.scaleImage = function (image, outerSizes) {
            if (outerSizes === void 0) { outerSizes = false; }
            var container = image.parent(), cHeight = (outerSizes) ? container.outerHeight() : container.height(), cWidth = (outerSizes) ? container.outerWidth() : container.width(), iHeight = 0, iWidth = 0, ratio, min;
            // Retrieve original sizes.
            if (!image.data('pl-real-width'))
                image.data('pl-real-width', image.outerWidth());
            if (!image.data('pl-real-height'))
                image.data('pl-real-height', image.outerHeight());
            iWidth = image.data('pl-real-width');
            iHeight = image.data('pl-real-height');
            // Calculate new sizes.
            ratio = [cWidth / iWidth, cHeight / iHeight];
            min = Math.min(ratio[0], ratio[1]);
            // Assign new sizes to image.
            image.width(iWidth * min);
            image.height(iHeight * min);
        };
        /**
         * Set thumbs to display.
         * @param thumbsToDisplay
         */
        Carrousel.prototype.setThumbsToDisplay = function (thumbsToDisplay) {
            this.settings.thumbsToDisplay = thumbsToDisplay;
            this.cloneThumbs();
            this.thumbsContainer
                .empty()
                .append(this.increasedThumbs);
            this.calculateWidths();
            this.adaptImage(this.thumbsImages);
            this.animate(false);
        };
        /**
         * function to hide Arrows when  the carousel isn´t infinite or isn´t more elements to show
         */
        Carrousel.prototype.hideArrow = function () {

            let clonedThumbs = this.increasedThumbs;
            let thumbsToDisplay = this.settings.thumbsToDisplay;
            let currentThumb = this.currentThumb;
            let next = this.nextButton;
            let prev = this.prevButton;

            if (!this.settings.infiniteScroll) {

                if (thumbsToDisplay >= clonedThumbs.length) {
                    prev.addClass('d-none');
                    next.addClass('d-none');
                }
                else if (thumbsToDisplay < clonedThumbs.length) {
                    if ((currentThumb + thumbsToDisplay) === clonedThumbs.length) {
                        next.addClass('d-none');
                        prev.removeClass('d-none');
                    }
                    else {
                        if ((currentThumb + thumbsToDisplay) <= clonedThumbs.length && currentThumb != 0) {
                            prev.removeClass('d-none');
                            next.removeClass('d-none');
                        }
                        else {
                            prev.addClass('d-none');
                            next.removeClass('d-none');
                        }
                    }
                }
            }

        };
        /**
         * Wait to images are already loaded, then excecute a callback.
         *
         * @param images
         * @param callback
         */
        Carrousel.prototype.waitForImages = function (images, callback) {
            var totalImages = images.length, loadedImages = 0;
            function onImageLoaded() {
                if (++loadedImages >= totalImages)
                    if (callback && typeof callback === 'function')
                        callback();
            }
            images.each(function () {
                var image = $(this);
                if (image.get(0).complete)
                    onImageLoaded();
                else
                    image.on('load', function () { onImageLoaded(); });
            });
        };
        return Carrousel;
    }());
    util.Carrousel = Carrousel;
})(util || (util = {}));
