/**
 * Created by cesarmejia on 27/09/17.
 */

var Util = (function () {

    /**
     *
     * @constructor
     */
    function Util() {  }

    /**
     * Determine whether any of the matched elements are assigned the given class.
     * @param {HTMLElement} elem
     * @param {string} className
     * @returns {boolean}
     */
    Util.hasClass = function(elem, className) {
        return elem.classList
            ? elem.classList.contains(className)
            : new RegExp("\\b" + className + "\\b").test(elem.className);
    };

    /**
     * Adds the specified class to an element.
     * @param {HTMLElement} elem
     * @param {string} className
     */
    Util.addClass = function(elem, className) {
        if (elem.classList) elem.classList.add(className);
        else if (!Util.hasClass(elem, className)) elem.className += " " + className;
    };

    /**
     * Remove class from element.
     * @param {HTMLElement} elem
     * @param {string} className
     */
    Util.removeClass = function(elem, className) {
        if (elem.classList) elem.classList.remove(className);
        else elem.className = elem.className.replace(new RegExp("\\b" + className + "\\b", "g"), '');
    };

    return Util;

})();
