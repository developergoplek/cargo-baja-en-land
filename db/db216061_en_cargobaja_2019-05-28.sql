# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: internal-db.s216061.gridserver.com (MySQL 5.6.33-79.0)
# Base de datos: db216061_en_cargobaja
# Tiempo de Generación: 2019-05-28 15:14:10 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla file
# ------------------------------------------------------------

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file` (
  `idfile` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(50) DEFAULT NULL,
  `iduser` int(11) DEFAULT '0',
  `idowner` int(11) DEFAULT '0',
  `idparent` int(11) DEFAULT '0',
  `owner` varchar(50) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `size` int(11) DEFAULT '0',
  `bucket` varchar(30) DEFAULT NULL,
  `path` varchar(128) DEFAULT NULL,
  `uploaded` datetime DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `width` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0',
  `key` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idfile`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;

INSERT INTO `file` (`idfile`, `guid`, `iduser`, `idowner`, `idparent`, `owner`, `name`, `size`, `bucket`, `path`, `uploaded`, `description`, `width`, `height`, `key`)
VALUES
	(1,'m-1zgSryqp',1,13,0,'Page','f6026c1e-2b14-427a-b205-7c33aa22da92.jpg',5635,'','fragment/files/uploads/2019/04/03/5ca4f6ebe07e4-f6026c1e-2b14-427a-b205-7c33aa22da92.jpg','2019-04-03 12:09:47','',0,0,''),
	(2,'5j-l-IZNb8',1,1,1,'File','f6026c1e-2b14-427a-b205-7c33aa22da92.jpg',5731,'','fragment/files/uploads/2019/04/03/5ca4f6ee6d708-f6026c1e-2b14-427a-b205-7c33aa22da92.jpg','2019-04-03 12:09:50','',84,53,'presentable'),
	(3,'xV3ktGowVw',1,14,0,'Page','cc21f000-5cf6-4081-af14-0f87de7d94c2.jpg',4809,'','fragment/files/uploads/2019/04/03/5ca4f71996091-cc21f000-5cf6-4081-af14-0f87de7d94c2.jpg','2019-04-03 12:10:33','',0,0,''),
	(4,'HHkDcVOtZD',1,3,3,'File','cc21f000-5cf6-4081-af14-0f87de7d94c2.jpg',4876,'','fragment/files/uploads/2019/04/03/5ca4f71a3da69-cc21f000-5cf6-4081-af14-0f87de7d94c2.jpg','2019-04-03 12:10:34','',84,53,'presentable'),
	(5,'nBKh5hFV1c',1,15,0,'Page','26467016-c730-4de5-b1e4-7feecadf684e.jpg',4588,'','fragment/files/uploads/2019/04/03/5ca4f7a833ea8-26467016-c730-4de5-b1e4-7feecadf684e.jpg','2019-04-03 12:12:56','',0,0,''),
	(6,'svRxpMi1qe',1,5,5,'File','26467016-c730-4de5-b1e4-7feecadf684e.jpg',4692,'','fragment/files/uploads/2019/04/03/5ca4f7a9073a2-26467016-c730-4de5-b1e4-7feecadf684e.jpg','2019-04-03 12:12:57','',84,53,'presentable'),
	(7,'PaTleRH56b',1,16,0,'Page','e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg',4980,'','fragment/files/uploads/2019/04/03/5ca4f7de4d172-e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg','2019-04-03 12:13:50','',0,0,''),
	(8,'uYN8fbv3yG',1,7,7,'File','e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg',5065,'','fragment/files/uploads/2019/04/03/5ca4f7df0c29b-e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg','2019-04-03 12:13:51','',84,53,'presentable'),
	(9,'Vs2EZXv80I',1,17,0,'Page','e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg',4980,'','fragment/files/uploads/2019/04/03/5ca4f7f21442d-e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg','2019-04-03 12:14:10','',0,0,''),
	(10,'IRP6S-YdFH',1,9,9,'File','e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg',5065,'','fragment/files/uploads/2019/04/03/5ca4f7f2c3d5e-e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg','2019-04-03 12:14:10','',84,53,'presentable'),
	(11,'PU3kgDQP6a',1,17,0,'Page','cd53c9cb-cd4d-460e-971a-292ae18ce98a.jpg',4561,'','fragment/files/uploads/2019/04/03/5ca4f81b17248-cd53c9cb-cd4d-460e-971a-292ae18ce98a.jpg','2019-04-03 12:14:51','',0,0,''),
	(12,'DDUIj3u5mK',1,11,11,'File','cd53c9cb-cd4d-460e-971a-292ae18ce98a.jpg',4651,'','fragment/files/uploads/2019/04/03/5ca4f81b6ed32-cd53c9cb-cd4d-460e-971a-292ae18ce98a.jpg','2019-04-03 12:14:51','',84,53,'presentable'),
	(13,'Xtlhl1kFpa',1,13,0,'Page','f6026c1e-2b14-427a-b205-7c33aa22da92.jpg',5635,'','fragment/files/uploads/2019/04/05/5ca7f6aec0919-f6026c1e-2b14-427a-b205-7c33aa22da92.jpg','2019-04-05 18:45:34','',0,0,''),
	(14,'a3ByJ32C06',1,13,13,'File','f6026c1e-2b14-427a-b205-7c33aa22da92.jpg',5731,'','fragment/files/uploads/2019/04/05/5ca7f6af8483b-f6026c1e-2b14-427a-b205-7c33aa22da92.jpg','2019-04-05 18:45:35','',84,53,'presentable'),
	(15,'ZY107gZ2ST',1,14,0,'Page','cc21f000-5cf6-4081-af14-0f87de7d94c2.jpg',4809,'','fragment/files/uploads/2019/04/05/5ca7f6dcc067d-cc21f000-5cf6-4081-af14-0f87de7d94c2.jpg','2019-04-05 18:46:20','',0,0,''),
	(16,'0601MWLRRs',1,15,15,'File','cc21f000-5cf6-4081-af14-0f87de7d94c2.jpg',4876,'','fragment/files/uploads/2019/04/05/5ca7f6ddd6847-cc21f000-5cf6-4081-af14-0f87de7d94c2.jpg','2019-04-05 18:46:21','',84,53,'presentable'),
	(17,'VtS3z4rPvT',1,15,0,'Page','26467016-c730-4de5-b1e4-7feecadf684e.jpg',4588,'','fragment/files/uploads/2019/04/05/5ca7f6e86cba7-26467016-c730-4de5-b1e4-7feecadf684e.jpg','2019-04-05 18:46:32','',0,0,''),
	(18,'6QVIACQjRD',1,17,17,'File','26467016-c730-4de5-b1e4-7feecadf684e.jpg',4692,'','fragment/files/uploads/2019/04/05/5ca7f6e95485b-26467016-c730-4de5-b1e4-7feecadf684e.jpg','2019-04-05 18:46:33','',84,53,'presentable'),
	(19,'SdRIBUnhGr',1,16,0,'Page','e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg',4980,'','fragment/files/uploads/2019/04/05/5ca7f6f05a336-e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg','2019-04-05 18:46:40','',0,0,''),
	(20,'PoV-rDR2vg',1,19,19,'File','e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg',5065,'','fragment/files/uploads/2019/04/05/5ca7f6f0ef53b-e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg','2019-04-05 18:46:40','',84,53,'presentable'),
	(21,'71Ai2M1hmH',1,17,0,'Page','cd53c9cb-cd4d-460e-971a-292ae18ce98a.jpg',4561,'','fragment/files/uploads/2019/04/05/5ca7f6f971f11-cd53c9cb-cd4d-460e-971a-292ae18ce98a.jpg','2019-04-05 18:46:49','',0,0,''),
	(22,'IHq7BnMcBm',1,21,21,'File','cd53c9cb-cd4d-460e-971a-292ae18ce98a.jpg',4651,'','fragment/files/uploads/2019/04/05/5ca7f6fa4b9bd-cd53c9cb-cd4d-460e-971a-292ae18ce98a.jpg','2019-04-05 18:46:50','',84,53,'presentable');

/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla fragment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fragment`;

CREATE TABLE `fragment` (
  `idfragment` int(11) NOT NULL AUTO_INCREMENT,
  `idpage` int(11) DEFAULT '0',
  `value` longtext,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idfragment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `fragment` WRITE;
/*!40000 ALTER TABLE `fragment` DISABLE KEYS */;

INSERT INTO `fragment` (`idfragment`, `idpage`, `value`, `name`)
VALUES
	(157,1,'Lorem Ipsum','body'),
	(158,4,'We have a comprehensive solution for the supply chains of the following industries','text-cover'),
	(159,12,'<b>Commercial</b>','title-service'),
	(160,11,'<b>Residential</b>','title-service'),
	(161,10,'<div><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</b></div><div><b>Hospitality</b></div>','title-service'),
	(162,6,'Contacto','contact-title'),
	(163,6,'Leave your information in the following form and we will contact you shortly.','contact-text'),
	(164,6,'(33) 3852 3911 Ext 1000','office_number1'),
	(165,6,'(33) 3852 3911','office_number2'),
	(166,6,'(33) 1410 0485','personal_number1'),
	(167,6,'(33) 1410 0485','personal_number2'),
	(168,8,'http://www.goplek.com','body'),
	(169,5,'<span id=\"docs-internal-guid-5531698b-7fff-92d6-1d99-a44c44985a00\"><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:0pt;text-align: center;\"><span id=\"docs-internal-guid-5531698b-7fff-92d6-1d99-a44c44985a00\"></span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:0pt;text-align: center;\"><span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-b682e1f3-7fff-3394-e880-2962ad803da7\">We are a professional and passionate team, with more than ten years satisfying the specific needs of customers and exceeding the highest standards in customer service.</span></p></span>','text-benefits'),
	(170,13,'<span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-f13a4779-7fff-0a42-bb53-87f6ce059890\">We understand the importance of delivering your goods on time.</span><br><div><br></div>','desc-benefit'),
	(171,13,'<img data-guid=\"a3ByJ32C06\" data-original-guid=\"Xtlhl1kFpa\" alt=\"\" src=\"/fragment/files/uploads/2019/04/05/5ca7f6af8483b-f6026c1e-2b14-427a-b205-7c33aa22da92.jpg\">','icon-benefit'),
	(172,14,'<span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-ce3e506c-7fff-e9bb-7067-498ea7fffd27\">Your goods will be safe in the hands of our highly qualified staff.</span>','desc-benefit'),
	(173,14,'<img data-guid=\"0601MWLRRs\" data-original-guid=\"ZY107gZ2ST\" alt=\"\" src=\"/fragment/files/uploads/2019/04/05/5ca7f6ddd6847-cc21f000-5cf6-4081-af14-0f87de7d94c2.jpg\">','icon-benefit'),
	(174,15,'<img data-guid=\"6QVIACQjRD\" data-original-guid=\"VtS3z4rPvT\" alt=\"\" src=\"/fragment/files/uploads/2019/04/05/5ca7f6e95485b-26467016-c730-4de5-b1e4-7feecadf684e.jpg\">','icon-benefit'),
	(175,15,'<span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-42e88e2e-7fff-56f8-ceed-52cc0f213181\">Manage and visualize the status of your merchandise in real-time through our web portal.</span>','desc-benefit'),
	(176,16,'<span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-4be03c21-7fff-df3b-5f6b-0506da25110f\">Managing your supply chain through one company significantly reduces costs, risks, workload, and complexity.</span>','desc-benefit'),
	(177,16,'<img data-guid=\"PoV-rDR2vg\" data-original-guid=\"SdRIBUnhGr\" alt=\"\" src=\"/fragment/files/uploads/2019/04/05/5ca7f6f0ef53b-e72b06e7-acdf-4a80-b926-40b8e0f3ecd3.jpg\">','icon-benefit'),
	(178,17,'<span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-1ad6e64c-7fff-29ef-048c-904205b0780b\">Personalized attention by our logistics coordinators.</span>','desc-benefit'),
	(179,17,'<img data-guid=\"IHq7BnMcBm\" data-original-guid=\"71Ai2M1hmH\" alt=\"\" src=\"/fragment/files/uploads/2019/04/05/5ca7f6fa4b9bd-cd53c9cb-cd4d-460e-971a-292ae18ce98a.jpg\">','icon-benefit'),
	(180,18,'<div><ul><li>Fiscal</li><li>Compras</li><li>Comercio Internacional</li><li>Logistica</li><li>Almacenaje e instalaciÃ³n</li><li>AdministraciÃ³n de proyecto de cadena de suministro</li></ul></div>','body'),
	(181,19,'<div>AnÃ¡lisis y bÃºsqueda de artÃ­culos.</div><div>SelecciÃ³n y negociaciÃ³n con proveedores.</div><div>EjecuciÃ³n y administraciÃ³n de las compras.</div><div>SupervisiÃ³n de producciÃ³n para controlar calidad y puntualidad.</div><div>Muebles fijos y artÃ­culos decorativos.</div>','body'),
	(182,20,'<ul><li>Nacional.</li><li>Internacional.</li></ul>','body'),
	(183,26,'<ul><li>AdministraciÃ³n de compras.</li><li>Comercio Internacional.</li><li>LogÃ­stica, almacenaje y distribuciÃ³n.</li></ul>','body'),
	(184,33,'<div><ul><li>DefiniciÃ³n de Ã¡reas</li><li>Listado maestro de artÃ­culos</li><li>AdministraciÃ³n de compras</li><li>Comercio Internacional</li><li>LogÃ­stica, almacenaje e instalaciÃ³n</li></ul></div>','body'),
	(185,27,'<ul><li>AnÃ¡lisis y bÃºsqueda de artÃ­culos</li><li>SelecciÃ³n y negociaciÃ³n con proveedores.</li><li>EjecuciÃ³n y administraciÃ³n de las compras.</li></ul>','body'),
	(186,34,'<div><ul><li>AnÃ¡lisis y bÃºsqueda de artÃ­culos</li><li>SelecciÃ³n y negociaciÃ³n con proveedores</li><li>EjecuciÃ³n y administraciÃ³n de las compras</li><li>SupervisiÃ³n de producciÃ³n para controlar calidad y puntualidad.</li><li>Muebles fijos y artÃ­culos decorativos</li></ul></div>','body'),
	(187,21,'<div><ul><li>ImportaciÃ³n.</li><li>ExportaciÃ³n.</li><li>Gestion de tramites y permisos.</li></ul></div>','body'),
	(188,28,'<ul><li>Nacional.</li><li>Internacional.</li></ul>','body'),
	(189,22,'<div><ul><li>Almacenaje fijo y variable.</li><li>AdministraciÃ³n y reportes de inventarios.</li><li>InspecciÃ³n y empaque de bienes.</li></ul></div>','body'),
	(190,23,'<div><ul><li>Reparto de mercancÃ­a.</li><li>Maniobras especiales.</li></ul></div>','body'),
	(191,29,'<ul><li><br></li></ul>','body'),
	(192,24,'<div><ul><li>Armado y colocaciÃ³n.</li><li>Entregas de guante blanco.</li></ul></div>','body'),
	(193,30,'<ul><li>Almacenaje fijo y variable.</li><li>AministraciÃ³n y reportes de inventarios.</li><li>InspecciÃ³n y empaque de bienes.</li></ul>','body'),
	(194,31,'<ul><li>Reparto de mercancÃ­a.</li><li>Maniobras especiales.</li></ul>','body'),
	(195,32,'<ul><li>Armado y colocaciÃ³n.</li><li>Entregas de guante blanco.</li></ul>','body'),
	(196,35,'<ul><li>Nacional.</li><li>Internacional.</li></ul>','body'),
	(197,36,'<ul><li>ImportaciÃ³n.</li><li>ExportaciÃ³n.</li><li>Gestion de tramites y permisos.</li></ul>','body'),
	(198,38,'<ul><li>Almacenaje fijo y variable.</li><li>AdministraciÃ³n y reportes de inventarios.</li><li>InspecciÃ³n y empaque de bienes.</li></ul>','body'),
	(199,39,'<ul><li>Reparto de mercancÃ­a.</li><li>Maniobras especiales.</li></ul>','body'),
	(200,40,'<ul><li>Armado y colocaciÃ³n.</li><li>Entregas de guante blanco.</li></ul>','body'),
	(201,10,'<b>Consulting &amp; Planning</b>','title-process1'),
	(202,10,'<div><span id=\"docs-internal-guid-32b651c2-7fff-c029-5aea-8b49b934658a\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">Proper planning of your supply chain is critical for your projectâ€™s success. We start by analyzing each step of your supply chain to optimize it based on your staff and projectâ€™s needs</span>.<br><div><ul><li>Customs and international trade compliance.</li><li>Logistics, procurement &amp; cost optimization.</li><li>Fiscal advising for operations in Mexico.</li></ul></div></div>','list-process1'),
	(203,10,'Procurement','title-process2'),
	(204,10,'<div><span style=\"font-family: &quot;Exo 2&quot;, sans-serif; font-size: 14.6667px; white-space: pre-wrap;\">We can help you find the best options according to your needs and budget. Our in-house purchasing system will enable you to manage your purchase orders from any web browser.</span></div><div><span style=\"white-space: pre-wrap;\"><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px;\"><br></span></font></span><div><ul><li><span style=\"white-space: pre-wrap;\">Fixed furniture, operating supplies, equipment, and construction materials.</span></li><li><span style=\"white-space: pre-wrap;\">Exclusive shopping trips to Guadalajara, San Miguel de Allende, and other cities.</span></li><li><span style=\"white-space: pre-wrap;\">Access to the best manufacturers in Mexico.</span></li><li><span style=\"white-space: pre-wrap;\">Production inspections and reports.</span></li></ul></div></div>','list-process2'),
	(205,10,'Logistics','title-process3'),
	(206,10,'<span id=\"docs-internal-guid-b03083e7-7fff-3782-6ab7-b1626e9b743b\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">We move your shipments on time, safely and at the best rates.</span><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li>Complete coverage throughout Mexico and international 3PL coordination.&nbsp;</li><li>By sea, land, and air.</li><li>Full or partial truckloads.</li></ul></div></div>','list-process3'),
	(207,10,'<b>Customs Brokerage</b>','title-process4'),
	(208,10,'<span id=\"docs-internal-guid-3eebb358-7fff-97ae-0bd3-084f11b84487\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">Our qualified team will make sure your operations are prompt and in compliance with government authorities.</span><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li>We can process shipments from any point in Mexico.</li><li>Registered importers in Mexico and USA with most of the necessary permits for importing and exporting goods for the hospitality, residential and development industries.&nbsp;</li><li>Permits and documentation compliance.</li></ul></div></div>','list-process4'),
	(209,10,'Storage','title-process5'),
	(210,10,'<span id=\"docs-internal-guid-4e1be1ca-7fff-ba9e-4135-10db0c7a59e7\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">From temporary projects to permanent solutions. Your merchandise will be safe and available.</span><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li>Warehouse in San Diego, Ca. and at strategic points throughout Mexico to consolidate and process your shipments.</li><li><span id=\"docs-internal-guid-a06ddf68-7fff-05be-2e1f-fca63966d03d\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Warehouse management system to provide detailed inventory controls.</span>&nbsp;</li><li>Merchandise inspection and packing.</li></ul></div></div>','list-process5'),
	(211,10,'Distribution','title-process6'),
	(212,10,'<span id=\"docs-internal-guid-f45a9231-7fff-d1b9-0549-2c71a6e490e7\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">Our experienced team and delivery trucks can execute projects of any size.</span><br><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li><span id=\"docs-internal-guid-b283684f-7fff-2d87-cad3-5e5d9431ca64\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Merchandise sorting, inspection, and delivery</span>.</li><li><span id=\"docs-internal-guid-a06ddf68-7fff-05be-2e1f-fca63966d03d\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Proper handling of large, heavy, and delicate items.</span>&nbsp;</li><li><span id=\"docs-internal-guid-ab1622d4-7fff-8063-66cc-44a44f33b31e\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Inventory controls</span>.</li><li>Relocation services for executives.</li></ul></div></div>','list-process6'),
	(213,10,'<span id=\"docs-internal-guid-d8db975a-7fff-f9af-1f52-de28cea61fe5\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">Our experienced team can execute projects of any size and level of difficulty.</span><br><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li>White glove deliveries &amp; furniture assembly</li><li>Art and mirror hanging.</li><li><span id=\"docs-internal-guid-a06ddf68-7fff-05be-2e1f-fca63966d03d\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Protection of facilities.</span>&nbsp;</li><li>Maneuvering of large, heavy, and delicate items.</li><li><span id=\"docs-internal-guid-9b3ec1c2-7fff-f826-8c2e-d18aad1e4c9b\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Debris removal</span>.</li></ul></div></div>','list-process7'),
	(214,10,'<span style=\"color: rgb(89, 89, 89); font-family: Arial; font-size: 16px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">Installation</span>','title-process7'),
	(215,10,'<b>Supply Chain</b>','inner-title-service'),
	(216,10,'Hospitality','inner-subtitle-service'),
	(217,10,'<span id=\"docs-internal-guid-2117c8c0-7fff-ee25-e81f-af51f1b49482\"><span style=\"font-size: 11pt; font-family: Arial; color: rgb(33, 33, 33); background-color: rgb(255, 255, 255); font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">In a global market, the key to providing competitive products and services relies mostly on in the supply chain. We help our customers optimize their supply chain through:</span></span>','inner-phrase-service'),
	(218,11,'<b>Supply Chain</b>','inner-title-service'),
	(219,11,'Residential&nbsp;','inner-subtitle-service'),
	(220,11,'<span style=\"color: rgb(33, 33, 33); font-family: Arial; font-size: 14.6667px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">In a global market, the key to providing competitive products and services relies mostly on in the supply chain. We help our customers optimize their supply chain through:</span>','inner-phrase-service'),
	(221,11,'<b>Consulting &amp; Planning</b>','title-process1'),
	(222,11,'<span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-32b651c2-7fff-c029-5aea-8b49b934658a\">Proper planning of your supply chain is critical for your projectâ€™s success. We start by analyzing each step of your supply chain to optimize it based on your staff and projectâ€™s needs</span>.<div><br><div><div><div><ul><li>Customs and international trade compliance.</li><li>Logistics, procurement &amp; cost optimization.</li><li>Fiscal advising for operations in Mexico.</li></ul></div></div></div></div>','list-process1'),
	(223,11,'Procurement','title-process2'),
	(224,11,'<span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-8b622e3e-7fff-f5ce-4903-ddf93e8b1c24\">We can help you find the best options according to your needs and budget. Our in-house purchasing system will enable you to manage your purchase orders from any web browser.</span><br><div><span style=\"white-space: pre-wrap;\"><br></span><div><ul><li><span style=\"white-space: pre-wrap;\">Fixed furniture, operating supplies, equipment, and construction materials.</span></li><li><span style=\"white-space: pre-wrap;\">Exclusive shopping trips to Guadalajara, San Miguel de Allende, and other cities.</span></li><li><span style=\"white-space: pre-wrap;\">Access to the best manufacturers in Mexico.</span></li><li><span style=\"white-space: pre-wrap;\">Production inspections and reports.</span></li></ul></div></div>','list-process2'),
	(225,11,'Logistics','title-process3'),
	(226,11,'<span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-b03083e7-7fff-3782-6ab7-b1626e9b743b\">We move your shipments on time, safely and at the best rates.</span><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li>Complete coverage throughout Mexico and international 3PL coordination.&nbsp;</li><li>By sea, land, and air.</li><li>Full or partial truckloads.</li></ul></div></div>','list-process3'),
	(227,11,'<b>Customs Brokerage</b>','title-process4'),
	(228,11,'<span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-3eebb358-7fff-97ae-0bd3-084f11b84487\">Our qualified team will make sure your operations are prompt and in compliance with government authorities.</span><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li>We can process shipments from any point in Mexico.</li><li>Registered importers in Mexico and USA with most of the necessary permits for importing and exporting goods for the hospitality, residential and development industries.&nbsp;</li><li>Permits and documentation compliance.</li></ul></div></div>','list-process4'),
	(229,11,'Storage','title-process5'),
	(230,11,'<span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-4e1be1ca-7fff-ba9e-4135-10db0c7a59e7\">From temporary projects to permanent solutions. Your merchandise will be safe and available.</span><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li>Warehouse in San Diego, Ca. and at strategic points throughout Mexico to consolidate and process your shipments.</li><li><span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-a06ddf68-7fff-05be-2e1f-fca63966d03d\">Warehouse management system to provide detailed inventory controls.</span>&nbsp;</li><li>Merchandise inspection and packing.</li></ul></div></div>','list-process5'),
	(231,11,'Distribution','title-process6'),
	(232,11,'<span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-f45a9231-7fff-d1b9-0549-2c71a6e490e7\">Our experienced team and delivery trucks can execute projects of any size.</span><br><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li><span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-b283684f-7fff-2d87-cad3-5e5d9431ca64\">Merchandise sorting, inspection, and delivery</span>.</li><li><span id=\"docs-internal-guid-a06ddf68-7fff-05be-2e1f-fca63966d03d\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Proper handling of large, heavy, and delicate items.</span>&nbsp;</li><li><span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-ab1622d4-7fff-8063-66cc-44a44f33b31e\">Inventory controls</span>.</li><li>Relocation services for executives.</li></ul></div></div>','list-process6'),
	(233,11,'<span style=\"color: rgb(89, 89, 89); font-family: Arial; font-size: 16px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">Installation</span>','title-process7'),
	(234,11,'<span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-d8db975a-7fff-f9af-1f52-de28cea61fe5\">Our experienced team can execute projects of any size and level of difficulty.</span><br><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li>White glove deliveries &amp; furniture assembly</li><li>Art and mirror hanging.</li><li><span id=\"docs-internal-guid-a06ddf68-7fff-05be-2e1f-fca63966d03d\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Protection of facilities.</span>&nbsp;</li><li>Maneuvering of large, heavy, and delicate items.</li><li><span style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\" id=\"docs-internal-guid-9b3ec1c2-7fff-f826-8c2e-d18aad1e4c9b\">Debris removal</span>.</li></ul></div></div>','list-process7'),
	(235,12,'<b>Supply Chain</b>','inner-title-service'),
	(236,12,'Commercial','inner-subtitle-service'),
	(237,12,'<span style=\"color: rgb(33, 33, 33); font-family: Arial; font-size: 14.6667px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">In a global market, the key to providing competitive products and services relies mostly on in the supply chain. We help our customers optimize their supply chain through:</span>','inner-phrase-service'),
	(238,12,'<b>Consulting &amp; Planning</b>','title-process1'),
	(239,12,'<span id=\"docs-internal-guid-32b651c2-7fff-c029-5aea-8b49b934658a\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">Proper planning of your supply chain is critical for your projectâ€™s success. We start by analyzing each step of your supply chain to optimize it based on your staff and projectâ€™s needs</span>.<div><br><div><ul><li>Customs and international trade compliance.</li><li>Logistics, procurement &amp; cost optimization.</li><li>Fiscal advising for operations in Mexico.</li></ul></div></div>','list-process1'),
	(240,12,'Procurement','title-process2'),
	(241,12,'<span id=\"docs-internal-guid-8b622e3e-7fff-f5ce-4903-ddf93e8b1c24\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">We can help you find the best options according to your needs and budget. Our in-house purchasing system will enable you to manage your purchase orders from any web browser.</span><br><div><span style=\"white-space: pre-wrap;\"><br></span><div><ul><li><span style=\"white-space: pre-wrap;\">Fixed furniture, operating supplies, equipment, and construction materials.</span></li><li><span style=\"white-space: pre-wrap;\">Exclusive shopping trips to Guadalajara, San Miguel de Allende, and other cities.</span></li><li><span style=\"white-space: pre-wrap;\">Access to the best manufacturers in Mexico.</span></li><li><span style=\"white-space: pre-wrap;\">Production inspections and reports.</span></li></ul></div></div>','list-process2'),
	(242,12,'Logistics','title-process3'),
	(243,12,'<span id=\"docs-internal-guid-b03083e7-7fff-3782-6ab7-b1626e9b743b\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">We move your shipments on time, safely and at the best rates.</span><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li>Complete coverage throughout Mexico and international 3PL coordination.&nbsp;</li><li>By sea, land, and air.</li><li>Full or partial truckloads.</li></ul></div></div>','list-process3'),
	(244,12,'<b>Customs Brokerage</b>','title-process4'),
	(245,12,'<div><span id=\"docs-internal-guid-3eebb358-7fff-97ae-0bd3-084f11b84487\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">Our qualified team will make sure your operations are prompt and in compliance with government authorities.</span><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li>We can process shipments from any point in Mexico.</li><li>Registered importers in Mexico and USA with most of the necessary permits for importing and exporting goods for the hospitality, residential and development industries.&nbsp;</li><li>Permits and documentation compliance.</li></ul></div></div></div>','list-process4'),
	(246,12,'Storage','title-process5'),
	(247,12,'<span id=\"docs-internal-guid-4e1be1ca-7fff-ba9e-4135-10db0c7a59e7\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">From temporary projects to permanent solutions. Your merchandise will be safe and available.</span><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li>Warehouse in San Diego, Ca. and at strategic points throughout Mexico to consolidate and process your shipments.</li><li><span id=\"docs-internal-guid-a06ddf68-7fff-05be-2e1f-fca63966d03d\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Warehouse management system to provide detailed inventory controls.</span>&nbsp;</li><li>Merchandise inspection and packing.</li></ul></div></div>','list-process5'),
	(248,12,'Distribution','title-process6'),
	(249,12,'<span id=\"docs-internal-guid-f45a9231-7fff-d1b9-0549-2c71a6e490e7\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">Our experienced team and delivery trucks can execute projects of any size.</span><br><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li><span id=\"docs-internal-guid-b283684f-7fff-2d87-cad3-5e5d9431ca64\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Merchandise sorting, inspection, and delivery</span>.</li><li><span id=\"docs-internal-guid-a06ddf68-7fff-05be-2e1f-fca63966d03d\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Proper handling of large, heavy, and delicate items.</span>&nbsp;</li><li><span id=\"docs-internal-guid-ab1622d4-7fff-8063-66cc-44a44f33b31e\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Inventory controls</span>.</li><li>Relocation services for executives.</li></ul></div></div>','list-process6'),
	(250,12,'<span style=\"color: rgb(89, 89, 89); font-family: Arial; font-size: 16px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">Installation</span>','title-process7'),
	(251,12,'<span id=\"docs-internal-guid-d8db975a-7fff-f9af-1f52-de28cea61fe5\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; vertical-align: baseline; white-space: pre-wrap;\">Our experienced team can execute projects of any size and level of difficulty.</span><br><div><font face=\"Exo 2, sans-serif\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\"><br></span></font><div><ul><li>White glove deliveries &amp; furniture assembly</li><li>Art and mirror hanging.</li><li><span id=\"docs-internal-guid-a06ddf68-7fff-05be-2e1f-fca63966d03d\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Protection of facilities.</span>&nbsp;</li><li>Maneuvering of large, heavy, and delicate items.</li><li><span id=\"docs-internal-guid-9b3ec1c2-7fff-f826-8c2e-d18aad1e4c9b\" style=\"font-size: 11pt; font-family: &quot;Exo 2&quot;, sans-serif; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Debris removal</span>.</li></ul></div></div>','list-process7');

/*!40000 ALTER TABLE `fragment` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group`;

CREATE TABLE `group` (
  `idgroup` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`idgroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;

INSERT INTO `group` (`idgroup`, `name`)
VALUES
	(1,'System');

/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla group_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_user`;

CREATE TABLE `group_user` (
  `idgroupuser` int(11) NOT NULL AUTO_INCREMENT,
  `idgroup` int(11) DEFAULT '0',
  `iduser` int(11) DEFAULT '0',
  PRIMARY KEY (`idgroupuser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `page`;

CREATE TABLE `page` (
  `idpage` int(11) NOT NULL AUTO_INCREMENT,
  `idparent` int(11) DEFAULT '0',
  `idgroup` int(11) DEFAULT '0',
  `iduser` int(11) DEFAULT '0',
  `guid` varchar(50) DEFAULT NULL,
  `key` varchar(200) DEFAULT NULL,
  `trash` int(1) DEFAULT '0',
  `online` int(1) DEFAULT '0',
  `template` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `sort` varchar(20) DEFAULT NULL,
  `powner` int(11) DEFAULT '0',
  `pgroup` int(11) DEFAULT '0',
  `pother` int(11) DEFAULT '0',
  `pworld` int(11) DEFAULT '0',
  `flags` int(11) DEFAULT '0',
  PRIMARY KEY (`idpage`),
  KEY `typeIndex` (`trash`,`online`,`idparent`) USING BTREE,
  KEY `i_guid` (`guid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;

INSERT INTO `page` (`idpage`, `idparent`, `idgroup`, `iduser`, `guid`, `key`, `trash`, `online`, `template`, `created`, `modified`, `title`, `description`, `order`, `sort`, `powner`, `pgroup`, `pother`, `pworld`, `flags`)
VALUES
	(1,0,1,1,'home','home',0,1,'index','2018-11-09 09:27:05','2018-11-09 09:27:05','Home',NULL,0,NULL,63,19,17,17,0),
	(2,1,1,1,'Fra_1nMoIc','extras',0,0,'index','2019-03-26 12:14:58','2019-03-26 12:14:58','Extras','',0,'',59,17,0,17,1024),
	(3,0,1,1,'pvl6jSVdSg','extras',0,1,'','2019-03-26 12:15:32','2019-03-26 12:15:35','Extras','',0,'',63,19,17,17,0),
	(4,1,1,1,'x6x7TseTrT','servicios',0,1,'index','2019-03-26 12:16:09','2019-04-05 18:08:23','Services','',0,'',59,17,0,17,0),
	(5,1,1,1,'un2tJwdjzI','beneficios',0,1,'benefits','2019-03-26 13:06:11','2019-04-05 18:41:50','Benefits','',0,'',59,17,0,17,0),
	(6,1,1,1,'C8-IFb_vsi','contacto',0,1,'contacto','2019-03-26 13:06:20','2019-04-05 18:44:39','Contact','',0,'',59,17,0,17,0),
	(7,1,1,1,'JkkADpuxQ7','clientes',0,1,'index','2019-03-26 13:06:54','2019-04-05 18:46:59','Customers','',0,'',59,17,0,17,0),
	(8,1,1,1,'tIBmpX_uuk','portal-web',0,1,'index','2019-03-26 13:07:12','2019-03-26 13:07:40','Portal web','',0,'',59,17,0,17,0),
	(9,1,1,1,'n4bBsNFvvs','pago-en-lnea',0,1,'payment','2019-03-26 13:07:27','2019-04-05 20:00:04','Pay online','',0,'',59,17,0,17,0),
	(10,4,1,1,'1yQzxb25V2','hoteleria-and-construccion',0,1,'content-process','2019-03-27 18:19:06','2019-04-07 13:05:24','Hospitality','',0,'',59,17,0,17,0),
	(11,4,1,1,'6FAk0_a7Zn','residencial-and-interiorismo',0,1,'content-process','2019-03-27 18:20:04','2019-04-05 19:15:25','Residential','',0,'',59,17,0,17,0),
	(12,4,1,1,'uFBNcS0sSE','comercial-and-industrial',0,1,'content-process','2019-03-27 18:20:24','2019-04-07 12:09:13','Commercial','',0,'',59,17,0,17,0),
	(13,5,1,1,'EyDBrrk0k_','puntualidad',0,1,'benefits','2019-04-03 12:05:40','2019-04-05 19:08:04','Always on time','',0,'',59,17,0,17,0),
	(14,5,1,1,'BjllecJx1y','seguridad',0,1,'benefits','2019-04-03 12:05:46','2019-05-13 17:03:06','Security','',0,'',59,17,0,17,0),
	(15,5,1,1,'OmBSOKyKbU','acceso-a-la-informacin',0,1,'benefits','2019-04-03 12:05:58','2019-04-05 18:43:07','Access to information','',0,'',59,17,0,17,0),
	(16,5,1,1,'Ya4jFOFZ7j','reduccin-de-costo-y-riesgo',0,1,'benefits','2019-04-03 12:06:18','2019-04-05 18:43:32','Cost and risk reduction','',0,'',59,17,0,17,0),
	(17,5,1,1,'WtaOBomhYA','atencin-personalizada',0,1,'benefits','2019-04-03 12:06:33','2019-04-05 19:09:35','Personalized Attention','',0,'',59,17,0,17,0),
	(18,10,1,1,'Y5V9kKLb29','planeacion-y-asesoria',0,1,'content-process','2019-04-03 12:56:54','2019-04-03 12:57:18','PlaneaciÃ³n y AsesorÃ­a','',0,'',59,17,0,17,1024),
	(19,10,1,1,'Hs34PlxCqc','compras-nacionales-e-internacionales',0,1,'content-process','2019-04-03 12:58:41','2019-04-03 12:58:45','COMPRAS NACIONALES E INTERNACIONALES ','',0,'',59,17,0,17,1024),
	(20,10,1,1,'twb6At7WUp','logistica',0,1,'content-process','2019-04-03 13:01:22','2019-04-03 13:01:49','LOGÃSTICA','',0,'',59,17,0,17,1024),
	(21,10,1,1,'jzI0HbM2_s','comercio-internacional',0,1,'content-process','2019-04-03 13:03:57','2019-04-03 13:04:01','Comercio Internacional','',0,'',59,17,0,17,1024),
	(22,10,1,1,'GBe67WwFDU','almacenaje',0,1,'content-process','2019-04-03 13:04:11','2019-04-03 13:04:31','Almacenaje','',0,'',59,17,0,17,1024),
	(23,10,1,1,'xrghJi_Srz','entregas',0,1,'content-process','2019-04-03 13:04:41','2019-04-03 13:04:45','Entregas','',0,'',59,17,0,17,1024),
	(24,10,1,1,'I1fw0tIp9o','instalacion',0,1,'content-process','2019-04-03 13:04:56','2019-04-03 13:05:04','InstalaciÃ³n','',0,'',59,17,0,17,1024),
	(25,11,1,1,'hxgIIH-DWL','planeacin-y-asesora',0,0,'content-process','2019-04-03 13:07:06','2019-04-03 13:07:06','PlaneaciÃ³n y asesorÃ­a','',0,'',59,17,0,17,1024),
	(26,12,1,1,'boiiA6FcbB','planeacin-y-asesora',0,1,'content-process','2019-04-03 13:08:22','2019-04-03 13:08:39','PlaneaciÃ³n y asesorÃ­a','',0,'',59,17,0,17,1024),
	(27,12,1,1,'WCQrzVtWPj','compras-nacionales-e-internacionales',0,1,'content-process','2019-04-03 13:09:08','2019-04-03 13:11:40','Compras nacionales e internacionales','',0,'',59,17,0,17,1024),
	(28,12,1,1,'4tYEf7qDmQ','logstica',0,1,'content-process','2019-04-03 13:09:37','2019-04-03 13:11:44','LogÃ­stica','',0,'',59,17,0,17,1024),
	(29,12,1,1,'UFxIPcLw6I','comercio-internacional',0,1,'content-process','2019-04-03 13:10:57','2019-04-03 13:11:47','Comercio internacional','',0,'',59,17,0,17,1024),
	(30,12,1,1,'O0ubn5nQsF','almacenaje',0,1,'content-process','2019-04-03 13:11:13','2019-04-03 13:11:51','Almacenaje','',0,'',59,17,0,17,1024),
	(31,12,1,1,'HEplRY6Gvl','entregas',0,1,'content-process','2019-04-03 13:11:25','2019-04-03 13:11:55','Entregas','',0,'',59,17,0,17,1024),
	(32,12,1,1,'XcT5azQn70','instalacin',0,1,'content-process','2019-04-03 13:11:35','2019-04-03 13:11:58','InstalaciÃ³n','',0,'',59,17,0,17,1024),
	(33,11,1,1,'CyR1kKuSd7','planeacin-y-asesora',0,1,'content-process','2019-04-03 13:19:09','2019-04-03 13:20:39','PlaneaciÃ³n y AsesorÃ­a','',0,'',59,17,0,17,1024),
	(34,11,1,1,'eID5mFp_hT','compras-nacionales-e-internacionales',0,1,'content-process','2019-04-03 13:20:57','2019-04-03 13:25:33','Compras Nacionales e Internacionales','',0,'',59,17,0,17,1024),
	(35,11,1,1,'DD78BBspe5','logstica',0,1,'content-process','2019-04-03 13:28:37','2019-04-03 13:28:56','LogÃ­stica ','',0,'',59,17,0,17,1024),
	(36,11,1,1,'ac66lmFTZn','comercio-internacional',0,1,'content-process','2019-04-03 13:30:02','2019-04-03 13:30:59','Comercio Internacional','',0,'',59,17,0,17,1024),
	(37,11,1,1,'IypQ1UCe_G','comercio-internacional',0,0,'content-process','2019-04-03 13:30:34','2019-04-03 13:30:34','Comercio Internacional','',0,'',59,17,0,17,1024),
	(38,11,1,1,'gttJGS3apr','almacenaje',0,1,'content-process','2019-04-03 13:31:09','2019-04-03 13:31:30','Almacenaje','',0,'',59,17,0,17,1024),
	(39,11,1,1,'tKOv6RQTj4','entregas',0,1,'content-process','2019-04-03 13:31:41','2019-04-03 13:32:01','Entregas','',0,'',59,17,0,17,1024),
	(40,11,1,1,'7eFA0YIgE0','instalacin',0,1,'content-process','2019-04-03 13:32:29','2019-04-03 13:35:50','InstalaciÃ³n','',0,'',59,17,0,17,1024);

/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla setting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `setting`;

CREATE TABLE `setting` (
  `idsetting` int(11) NOT NULL AUTO_INCREMENT,
  `idowner` int(11) DEFAULT '0',
  `owner` varchar(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` longtext,
  PRIMARY KEY (`idsetting`),
  KEY `i_name` (`name`) USING BTREE,
  KEY `i_owner` (`idowner`,`owner`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;

INSERT INTO `setting` (`idsetting`, `idowner`, `owner`, `name`, `value`)
VALUES
	(1832,1,'Page','page-configuration',''),
	(1833,0,'global','home','home'),
	(1835,0,'global','theme','cargobajanuevo'),
	(1836,2,'Page','page-configuration',''),
	(1837,3,'Page','page-configuration',''),
	(1838,4,'Page','page-configuration','{\n  \"fragments\": [\n    {\n      \"key\": \"text-cover\",\n      \"name\": \"Texto\",\n      \"type\": \"html\"\n    }\n  ],\n  \"children\": {\n    \"fragments\": [\n      {\n        \"key\": \"title-service\",\n        \"name\": \"TÃ­tulo\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"inner-title-service\",\n        \"name\": \"TÃ­tulo de secciÃ³n\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"inner-subtitle-service\",\n        \"name\": \"SubtÃ­tulo de secciÃ³n\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"inner-phrase-service\",\n        \"name\": \"Frase de secciÃ³n\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"title-process1\",\n        \"name\": \"TÃ­tulo proceso 1\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"list-process1\",\n        \"name\": \"Lista proceso 1\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"title-process2\",\n        \"name\": \"TÃ­tulo proceso 2\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"list-process2\",\n        \"name\": \"Lista proceso 2\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"title-process3\",\n        \"name\": \"TÃ­tulo proceso 3\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"list-process3\",\n        \"name\": \"Lista proceso 3\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"title-process4\",\n        \"name\": \"TÃ­tulo proceso 4\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"list-process4\",\n        \"name\": \"Lista proceso 4\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"title-process5\",\n        \"name\": \"TÃ­tulo proceso 5\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"list-process5\",\n        \"name\": \"Lista proceso 5\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"title-process6\",\n        \"name\": \"TÃ­tulo proceso 6\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"list-process6\",\n        \"name\": \"Lista proceso 6\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"title-process7\",\n        \"name\": \"TÃ­tulo proceso 7\",\n        \"type\": \"html\"\n      },\n      {\n        \"key\": \"list-process7\",\n        \"name\": \"Lista proceso 7\",\n        \"type\": \"html\"\n      }\n    ]\n  }\n}'),
	(1839,5,'Page','page-configuration','{\n  \"fragments\": [\n    {\n      \"key\": \"text-benefits\",\n      \"name\": \"Texto\",\n      \"type\": \"html\"\n    }\n  ],\n  \"children\": {\n     \"fragments\": [\n       {\n          \"key\": \"icon-benefit\",\n          \"name\": \"Imagen (84pxâ€Šxâ€Š53px)\",\n          \"type\": \"image\",\n          \"image-quality\": 1,\n          \"image-width\": 84,\n          \"image-height\": 53\n       },\n       {\n         \"key\": \"desc-benefit\",\n         \"name\": \"Texto\",\n         \"type\": \"html\"\n       }\n     ]\n  }\n}'),
	(1840,6,'Page','page-configuration','{\n  \"fragments\": [\n    {\n      \"key\": \"contact-text\",\n      \"name\": \"Texto\",\n      \"type\": \"html\"\n    },\n    {\n      \"key\": \"office_number1\",\n      \"name\": \"NÃºmero de oficina para marcar\",\n      \"type\": \"html\"\n    },\n    {\n      \"key\": \"office_number2\",\n      \"name\": \"NÃºmero de oficina para mostrar\",\n      \"type\": \"html\"\n    },\n    {\n      \"key\": \"personal_number1\",\n      \"name\": \"NÃºmero celular para marcar\",\n      \"type\": \"html\"\n    },\n    {\n      \"key\": \"personal_number2\",\n      \"name\": \"NÃºmero celular para mostrar\",\n      \"type\": \"html\"\n    }\n  ]\n}'),
	(1841,7,'Page','page-configuration',''),
	(1842,8,'Page','page-configuration',''),
	(1843,9,'Page','page-configuration',''),
	(1844,10,'Page','page-configuration',''),
	(1845,11,'Page','page-configuration',''),
	(1846,12,'Page','page-configuration',''),
	(1847,13,'Page','page-configuration',''),
	(1848,14,'Page','page-configuration',''),
	(1849,15,'Page','page-configuration',''),
	(1850,16,'Page','page-configuration',''),
	(1851,17,'Page','page-configuration',''),
	(1852,0,'global','image-quality',''),
	(1853,18,'Page','page-configuration',''),
	(1854,19,'Page','page-configuration',''),
	(1855,20,'Page','page-configuration',''),
	(1856,21,'Page','page-configuration',''),
	(1857,22,'Page','page-configuration',''),
	(1858,23,'Page','page-configuration',''),
	(1859,24,'Page','page-configuration',''),
	(1860,25,'Page','page-configuration',''),
	(1861,26,'Page','page-configuration',''),
	(1862,27,'Page','page-configuration',''),
	(1863,28,'Page','page-configuration',''),
	(1864,29,'Page','page-configuration',''),
	(1865,30,'Page','page-configuration',''),
	(1866,31,'Page','page-configuration',''),
	(1867,32,'Page','page-configuration',''),
	(1868,33,'Page','page-configuration',''),
	(1869,34,'Page','page-configuration',''),
	(1870,35,'Page','page-configuration',''),
	(1871,36,'Page','page-configuration',''),
	(1872,37,'Page','page-configuration',''),
	(1873,38,'Page','page-configuration',''),
	(1874,39,'Page','page-configuration',''),
	(1875,40,'Page','page-configuration','');

/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `flags` int(11) DEFAULT '0',
  PRIMARY KEY (`iduser`),
  KEY `i_uname` (`uname`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`iduser`, `uname`, `password`, `flags`)
VALUES
	(1,'root','747a075853ee1089f474d8a049ffb8a6',3),
	(2,'teamcreativo','ebb9dbe1b6b91529979a264fde96fa95',3);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
