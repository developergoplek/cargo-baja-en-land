**Internal Review**
[X] Pago en linea abre en otra página
[X] La animación del home no queda pegada al footer y se ve una línea blanca entre ella  *Revisado en desktop y mobil
[C] Las animaciones se reproducen al mismo tiempo pero sin que pases el slide al que corresponden así que cuando estás en los últimos slides la animación ya terminó de reproducirse, se tiene que empezar a reproducir pero cuando ya esté en la pantalla
[-] Las animaciones se reproducen muy lento, ya chequé en otras compus y pasa lo mismo
[-] Faltan efectos parallax para los slides, no sé si tenías pensado ponerlas después
[A] Yo tenía entendido que también al dar scroll se podría cambiar de diapositiva
[X] El botón de inicio que está abajo del logo tendría que ir un poquito más abajo  *Explicar porque se hicieron los nuevos ajustes
[D] Qué el menú pueda cerrarse haciendo click afuera 
[?] Hay Problemas en general con la redimensión
[X] En pago en línea las cajas de los textos tienen una sombra muy fuerte, ponerles más sutil 
[?] En pago en línea debería ir el texto pago seguro con 
[X] En contacto no sé si se puedas hacer las líneas del texto en diagonal como en el prototipo
        **Responsive**
[X] El texto de tenemos una solución no queda alineado con el último hexágono
[X] En el celular el texto de dentro de los hexágonos se ve muy al borde de la línea  *Revisar porque están de acuerdo a prototipo
    (Este es del prototipo)
[X] En el footer el logo de Goplek debería ir un poco más pequeño y el de cargo más grande
[X] En contacto los espacios para escribir deberían ir un poquito más abiertos
[X] En contacto falta el botón de llamar
[X] El contacto y el texto de abajo debería estar más a la izquierda con respecto al cuadro verde de abajo
[X] Dentro de servicios falta el swipe en la animación  *Isaí
[X] En la primera de servicios faltó la animación del swipe *Isaí
[-] Al pasar a otra diapositiva el swipe tarda en reaccionar
[-] El menú hamburguesa se traba, está como lento 
[-] El menú no se alcanza a ver completo en mi pantalla pero el problema es que cuando quiero hacer scroll se hace scroll pero en la página y no se puede bajar en el menú hasta después de varios intentos
[X] En la entrada de cadena de suministro la línea con los puntos está muy pegada al texto
[X] En algunos slides no aparece la animación
[-] Si le das scroll el timeline tarda en aparecer
[X] Hay varias cosas que se empalman en  general con el texto el todos los slides
[X] Otra cosa súper importante que se está pasando es la animación de entrada
[X] Mejor les mando el prototipo con las animaciones como deberían ir, o díganme como se los mando, que haaaagoooo?? *Isaí