<?php
include_once "../util/fragment-helpers.php";
$location = $_SERVER['REQUEST_URI']; //location
$link_web = find_page_by_guid($webSite_guid, $root_pages);
$inner_nav = '';

?>
<nav class="navigation">
    <div class="holder">
        <div class="container-fluid">
            <!-- Logo -->
            <a class="logo" href="/">
                <span class="wrapper-logo-nav">
                    <img alt="Cargo Baja - logo" class="img-fluid" src="<?= IMGS_PATH ?>logo-espanol.svg">
                </span>
            </a>
            <!--Navigation-->
            <div class="list-wrapper">
                <ul class="list">
                    <?php
                    $add_After = "";
                    $change_font = "";
                    $extra_attrib = '';
                    $href_key = "";
                    $target = "";
                    $i = 0;
                    $partialClass = '';
                    $active_class = '';

                    foreach ($root_pages as $i => $option) {

                        if ($option->key === $page->key || ($page->key === 'home' && $option->key === 'services')){
                            $active_class = ' item-active';
                        } else{
                            $active_class = '';
                        }

                        if (($i + 1) % 3 == 0) {
                            $add_After = "add-after";
                        } else {
                            $add_After = "";
                        }

                        if (($i + 1) > 3) {
                            $change_font = "change-font";
                            if (($i + 1) == 4) {
                                $change_font = "change-font";
                            }
                        }

                        if ($option->title === 'Customers') {
                            $href_key = "https://www.cargobajaportal.com/";
                            $target = "_blank";
                        } else {
                            $extra_attrib = "";
                            $href_key = $option->key;
                            $partialClass = '';
                            $target = "";
                        }

                        ?>

                        <?php if ($option->title !== 'Benefits' && $option->title !== 'Pago exitoso'){ ?>
                        <li class="<?= $add_After ?> <?= $change_font ?> <?= $partialClass ?>">
                            <a class="<?= $active_class ?>" <?= $extra_attrib ?> href="<?= $href_key ?>"><span><?= $option->title ?></span></a>
                        </li>
                        <?php } ?>
                        <?php
                        $i++;
                    }
                    ?>
                </ul>
            </div>

            <div class="flag bandera"><a href="https://cargobaja.com/"><img src="<?= IMGS_PATH ?>flag-esp.svg" alt="" class="img-fluid"></a></div>


            <!-- Toggle Button -->
            <button class="toggle-btn">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </button>
        </div>
    </div>
</nav>