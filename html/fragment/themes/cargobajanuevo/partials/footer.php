<?php
include_once "../globals.php";
include_once '../util/fragment-helpers.php';

$current_section = $_SERVER['REQUEST_URI'];
$intern_section = ($current_section != "/") ? 'changePosition' : '';

?>
<footer class="footer <?= $intern_section ?>">
    <div class="holder">
        <div class="container-fluid">
            <div class="content">
                <div class="footer-left">
                    <a href="/" class="logo-cargo-baja">
                        <img class="img-fluid" src="<?= IMGS_PATH ?>logo-cargo-footer.svg" alt="">
                    </a>
                    <div class="copyright">
                        <span>Cargo baja 2019</span>
                        <span>All rights reserved</span>
                    </div>
                </div>
                <div class="footer-right">
                    <a class="logo-en" href="https://cargobaja.com/" target="_blank">
                        <img src="<?= IMGS_PATH ?>flag-esp.svg" alt="logo-en">
                    </a>
                    <a class="privacity" href="<?= PDF_PATH ?>aviso-de-privacidad.pdf"  target="_blank">
                        <span>PRIVACY POLICY</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>