<?php
$location = $_SERVER['REQUEST_URI']; //location
?>
<!-- Include jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
<script src="<?= JS_PATH ?>vendor/canvas-animations.js"></script>
<script src="<?= JS_PATH ?>vendor/canvas-triggers.js"></script>

<script src="https://evopaymentsmexico.gateway.mastercard.com/static/checkout/checkout.min.js" data-error="errorCallback" data-cancel="cancelCallback" data-complete="completeCallback"></script>

<script src="<?= JS_PATH ?>scripts.min.js"></script>