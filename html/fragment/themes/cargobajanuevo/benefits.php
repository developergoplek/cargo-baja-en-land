<?php
    include_once 'globals.php';
    include_once 'util/fragment-helpers.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" itemscope itemtype="http://schema.org/Thing" lang="es-MX">
<head>
    <?php include_once 'partials/head.php'; ?>
</head>
<body>

<!-- Navigation -->
<?php include_once 'partials/navigation.php'; ?>
<!-- /.Navigation -->

<!-- Outer wrapper -->
<div class="outer-wrapper">
    <!-- Block Cover -->


    <!-- Benefits -->
    <?php
        $benefits = find_page_by_guid($benefits_guid, $root_pages);
        $benefits_title = $benefits->title;
        $benefits_intro = $benefits->fragments['text-benefits']->value;

        $result = Page::search(array(
            'idparent' => $benefits->idpage,
            'fragments' => array('icon-benefit', 'desc-benefit'),
            'sortBy' => 'created ASC'
        ));
        $children = $result['records'];
    ?>
    <section class="block benefits" id="beneficios">
        <div class="holder">
            <div class="container-fluid">
                <div class="header">
                    <div class="title partial"><?= $benefits_title ?></div>
                    <div class="intro partial"><?= $benefits_intro ?></div>
                </div>
                <div class="content">
                    <? foreach ($children as $child) { ?>
                        <div class="item">
                            <div class="icon partial"><?= $child->fragments['icon-benefit']->value ?></div>
                            <div class="title partial"><span><?= $child->title ?></span></div>
                            <div class="desc partial"><?= $child->fragments['desc-benefit']->value ?></div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- /.Benefits -->

    <!-- /.Cover -->
    <!-- Footer -->
    <?php include_once 'partials/footer.php'; ?>
    <!-- /.Footer -->
</div>
<?php include_once 'partials/scripts.php'; ?>
</body>
</html>
