<?php
/**
 * Created by PhpStorm.
 * User: goplek18
 * Date: 3/28/19
 * Time: 10:20 AM
 */

include_once 'globals.php';
include_once 'util/fragment-helpers.php';
/**
 * Contact Section
 */
$contact_page = find_page_by_guid($contact_guid, $root_pages);
$contact_text = $contact_page->fragments['contact-text']->value;
$office_number1 = $contact_page->fragments['office_number1']->value;
$office_number2 = $contact_page->fragments['office_number2']->value;
$personal_number1 = $contact_page->fragments['personal_number1']->value;
$personal_number2 = $contact_page->fragments['personal_number2']->value;

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" itemscope itemtype="http://schema.org/Thing"
      lang="es-MX">
<head>
    <?php include_once 'partials/head.php'; ?>
</head>
<body>

    <!-- Navigation -->
    <?php include_once 'partials/navigation.php'; ?>
    <!-- /.Navigation -->

    <!-- Outer wrapper -->
    <div class="outer-wrapper">

        <!-- Block contacto -->
        <section class="block contact" id="contacto">
            <div class="element-title-back">
                <?= $contact_page->title ?>
            </div>
            <div class="status-message d-none"></div>
            <div class="holder">
                <div class="container-fluid">
                    <div class="header"></div>
                    <div class="content">
                        <!-- Contact Info -->
                        <div class="contact-info">
                            <div class="inner-contact-info">
                                <div class="header">
                                    <h2 class="title"><?= $contact_page->title ?></h2>
                                </div>
                                <div class="info-message"><?= $contact_text ?></div>
                                <div class="wrapper-content-numbers">
                                    <a href="tel:<?= $office_number2 ?>" class="office-phone">
                                            <span class="icon"><img src="<?= IMGS_PATH ?>telephone.svg" alt=""
                                                                    class="img-fluid"></span>
                                        <span class="text"><?= $office_number1 ?></span>
                                    </a>
                                    <a href="tel:<?= $personal_number2 ?>" class="mobil-phone">
                                            <span class="icon"><img src="<?= IMGS_PATH ?>smartphone.svg" alt=""
                                                                    class="img-fluid"></span>
                                        <span class="text"><?= $personal_number1 ?></span>
                                    </a>
                                    <div class="wrapper-mobile-btn-call d-block d-sm-none">
                                        <div class="inner-mobile-btn-call">
                                            <a href="tel:<?= $personal_number2 ?>"><span>Llamar</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Contact Form -->
                        <div class="wrapper-contact-form">
                            <div class="inner-wrapper-form">
                                <div class="inner-wrapper-unbend">
                                    <form class="contact-form">
                                        <div class="wrapper-content-form">
                                            <input autocomplete="off" data-clue="Ingresa un Nombre"
                                                   data-validate="notEmpty"
                                                   name="Nombre" placeholder="Nombre" data-placeholder="Nombre"
                                                   type="text">
                                            <input autocomplete="off"
                                                   data-clue="Ingrese número a diez (10) dígitos:"
                                                   data-validate="phone" name="Telefono" placeholder="Teléfono"
                                                   data-placeholder="Teléfono" type="text">
                                            <input autocomplete="off" data-validate="email"
                                                   data-clue="Ingresa un correo válido"
                                                   name="Email" data-placeholder="E-mail:" placeholder="E-mail"
                                                   type="text">
                                            <textarea data-validate="notEmpty" data-clue="Ingresa un Mensaje"
                                                      name="Mensaje"
                                                      placeholder="Mensaje" data-placeholder="Mensaje"></textarea>
                                            <button class="submit" type="submit">
                                                <span class="inner">Contacto</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- /.contacto -->

        <!-- Footer -->
        <?php include_once 'partials/footer.php'; ?>
        <!-- /.Footer -->

        </div>
        <?php include_once 'partials/scripts.php'; ?>
    </body>
</html>