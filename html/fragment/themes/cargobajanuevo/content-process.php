<?php
include_once 'globals.php';
include_once 'util/fragment-helpers.php';
//values to show
$current_page = $page->idpage;

$image1 = '';
$image2 = '';
$image3 = '';
$extraClass1 = '';
/**
 * 10 hoteleria
 * 11 residencial
 * 12 Comercial
 */
switch ($current_page){
    case 10:
        $image1 = "d-none";
        $image3 = "d-none";
        break;
    case 11:
        $image1 = "d-none";
        $image2 = "d-none";
        break;
    case 12:
        $image2 = "d-none";
        $image3 = "d-none";
        $extraClass1 = "d-none";
        break;
}
// Retrieve root pages.
$parent_values = find_page_by_guid($service_guid, $root_pages);
$result = Page::search(array(
    'idparent' => $parent_values->idpage,
    'sortBy' => 'created ASC',
    'fragments' => array(
        'inner-title-service',
        'inner-subtitle-service',
        'inner-phrase-service',
        'title-process1',
        'list-process1',
        'title-process2',
        'list-process2',
        'title-process3',
        'list-process3',
        'title-process4',
        'list-process4',
        'title-process5',
        'list-process5',
        'title-process6',
        'list-process6',
        'title-process7',
        'list-process7',
    )
));

$son_pages = $result['records'];

foreach ($son_pages as $value_page) {
    $option_current = $value_page->idpage;

    if ($option_current === $current_page) {

        $inner_title = $value_page->fragments['inner-title-service']->value;
        $inner_subtitle = $value_page->fragments['inner-subtitle-service']->value;
        $inner_phrase = $value_page->fragments['inner-phrase-service']->value;
        $title1 = $value_page->fragments['title-process1']->value;
        $list1 = $value_page->fragments['list-process1']->value;
        $title2 = $value_page->fragments['title-process2']->value;
        $list2 = $value_page->fragments['list-process2']->value;
        $title3 = $value_page->fragments['title-process3']->value;
        $list3 = $value_page->fragments['list-process3']->value;
        $title4 = $value_page->fragments['title-process4']->value;
        $list4 = $value_page->fragments['list-process4']->value;
        $title5 = $value_page->fragments['title-process5']->value;
        $list5 = $value_page->fragments['list-process5']->value;
        $title6 = $value_page->fragments['title-process6']->value;
        $list6 = $value_page->fragments['list-process6']->value;
        $title7 = $value_page->fragments['title-process7']->value;
        $list7 = $value_page->fragments['list-process7']->value;
    }
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" itemscope itemtype="http://schema.org/Thing" lang="es-MX">
    <head>
        <?php include_once 'partials/head.php'; ?>
    </head>
    <body>
        <!-- Navigation -->
        <?php include_once 'partials/navigation.php'; ?>
        <!-- /.Navigation -->

        <!-- Outer wrapper -->
        <div class="outer-wrapper inner-process">

            <div class="wrapper-main-return">
                <a class="return-main" href="/">
                    <span>Inicio</span>
                </a>
            </div>

            <div class="wrapper-img-swipe d-block d-sm-none">
                <img src="<?= IMGS_PATH ?>swipe.svg" alt="swipe - animación" class="img-fluid">
            </div>

            <div class="inner-parent-wrapper" id="wrapper">
                <div class="inner-outer-wrapper">

                    <!--<section class="cover-page slide-process" data-anim="canvas_cover_page" id="cover-process">
                        <div class="holder">
                            <div class="wrapper-information-cover">

                                <div class="content-principal-title">
                                    <div class="inner-content-title">
                                        <div class="title-principal">
                                            <?= $inner_title ?>
                                        </div>
                                        <div class="subtitle-principal">
                                            <?= $inner_subtitle ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="content-principal-image">
                                    <div class="inner-content-image">
                                        <div class="content-image-text">
                                            <?= $inner_phrase ?>
                                        </div>
                                        <div class="content-image-canvas">
                                            <div class="inner-image-canvas">
                                                <div class="animation-container" id="animation_container"
                                                     style="background-color:rgba(255, 255, 255, 1.00); width:594px; height:367px">
                                                    <canvas class="canvas-container" id="canvas" width="594"
                                                            height="367"
                                                            style=" display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                                                    <div class="container-overlay" id="dom_overlay_container"
                                                         style="pointer-events:none; overflow:hidden; width:594px; height:367px; position: absolute; left: 0px; top: 0px; display: block;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>-->

                    <section class="steps-process slide-process" data-anim="canvas_planning,canvas_pur1,canvas_pur2" id="step-1">
                        <div class="holder">
                            <div class="wrapper-step">
                                <div class="wrapper-information-step">
                                    <div class="inner-information-step">
                                        <div class="title-step">
                                            <div class="inner-title-step">
                                                <?= $title1 ?>
                                            </div>
                                            <div class="number-step">
                                                <div class="inner-step">
                                                    1
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-of-step">
                                           <?= $list1 ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrapper-animation-step">
                                    <?php if ($image1 !== 'd-none'): ?>
                                    <div class="inner-animation-step <?= $image1 ?>">
                                        <div class="content-animation" id="animation_container_planning"
                                             style="background-color:rgba(255, 255, 255, 1.00); width:594px; height:367px">
                                            <canvas class="content-canvas" id="canvas_planning" width="594" height="367"
                                                    style="display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                                            <div class="content-overlay" id="dom_overlay_container_planning"
                                                 style="pointer-events:none; overflow:hidden; width:594px; height:367px; position: absolute; left: 0px; top: 0px; display: block;">
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php if ($image2 !== 'd-none'): ?>
                                    <div class="inner-animation-step <?= $image2 ?>">
                                        <div class="content-animation" id="animation_container_pur1"
                                             style="background-color:rgba(255, 255, 255, 1.00); width:594px; height:367px">
                                            <canvas class="content-canvas" id="canvas_pur1" width="594" height="367"
                                                    style="display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                                            <div class="content-overlay" id="dom_overlay_container_pur1"
                                                 style="pointer-events:none; overflow:hidden; width:594px; height:367px; position: absolute; left: 0px; top: 0px; display: block;">
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php if ($image3 !== 'd-none'): ?>
                                    <div class="inner-animation-step <?= $image3 ?>">
                                        <div class="content-animation" id="animation_container_pur2"
                                             style="background-color:rgba(255, 255, 255, 1.00); width:594px; height:367px">
                                            <canvas class="content-canvas" id="canvas_pur2" width="594" height="367"
                                                    style="display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                                            <div class="content-overlay" id="dom_overlay_container_pur2"
                                                 style="pointer-events:none; overflow:hidden; width:594px; height:367px; position: absolute; left: 0px; top: 0px; display: block;">
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="steps-process slide-process" data-anim="canvas_log1,canvas_log2" id="step-2">
                        <div class="holder">
                            <div class="wrapper-step">
                                <div class="wrapper-information-step">
                                    <div class="inner-information-step">
                                        <div class="title-step other">
                                            <div class="inner-title-step">
                                               <?= $title2 ?>
                                            </div>
                                            <div class="number-step">
                                                <div class="inner-step">
                                                    2
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-of-step">
                                        </div>
                                        <div class="list-of-step">
                                            <?= $list2 ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrapper-animation-step">

                                    <?php if ($image1 !== 'd-none'): ?>
                                    <div class="inner-animation-step <?= $image1 ?>">
                                        <div class="content-animation" id="animation_container_log1"
                                             style="background-color:rgba(255, 255, 255, 1.00); width:594px; height:367px">
                                            <canvas class="content-canvas" id="canvas_log1" width="594" height="367"
                                                    style="display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                                            <div class="content-overlay" id="dom_overlay_container_log1"
                                                 style="pointer-events:none; overflow:hidden; width:594px; height:367px; position: absolute; left: 0px; top: 0px; display: block;">
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php if ($extraClass1 !== 'd-none'): ?>
                                    <div class="inner-animation-step <?= $extraClass1?>">
                                        <div class="content-animation" id="animation_container_log2"
                                             style="background-color:rgba(255, 255, 255, 1.00); width:594px; height:367px">
                                            <canvas class="content-canvas" id="canvas_log2" width="594" height="367"
                                                    style="display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                                            <div class="content-overlay" id="dom_overlay_container_log2"
                                                 style="pointer-events:none; overflow:hidden; width:594px; height:367px; position: absolute; left: 0px; top: 0px; display: block;">
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="steps-process slide-process" data-anim="canvas_comercio_internacional" id="step-3">
                        <div class="holder">
                            <div class="wrapper-step">
                                <div class="wrapper-information-step">
                                    <div class="inner-information-step">
                                        <div class="title-step">
                                            <div class="inner-title-step">
                                                <?= $title3 ?>
                                            </div>
                                            <div class="number-step">
                                                <div class="inner-step">
                                                    3
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-of-step">
                                       </div>
                                        <div class="list-of-step">
                                            <?= $list3 ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrapper-animation-step">
                                    <div class="inner-animation-step">
                                        <div class="content-animation" id="animation_container_comercio_internacional"
                                             style="background-color:rgba(255, 255, 255, 1.00); width:590px; height:367px">
                                            <canvas class="content-canvas" id="canvas_comercio_internacional" width="590" height="367"
                                                    style="display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                                            <div class="content-overlay" id="dom_overlay_container_comercio_internacional"
                                                 style="pointer-events:none; overflow:hidden; width:590px; height:367px; position: absolute; left: 0px; top: 0px; display: block;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="steps-process slide-process" data-anim="canvas_alma" id="step4">
                        <div class="holder">
                            <div class="wrapper-step">
                                <div class="wrapper-information-step">
                                    <div class="inner-information-step">
                                        <div class="title-step">
                                            <div class="inner-title-step">
                                                <?= $title4 ?>
                                            </div>
                                            <div class="number-step">
                                                <div class="inner-step">
                                                    4
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-of-step">
                                       </div>
                                        <div class="list-of-step">
                                            <?= $list4?>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrapper-animation-step">
                                    <div class="inner-animation-step">
                                        <div class="content-animation" id="animation_container_alma"
                                             style="background-color:rgba(255, 255, 255, 1.00); width:590px; height:367px">
                                            <canvas class="content-canvas" id="canvas_alma" width="590" height="367"
                                                    style="display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                                            <div class="content-overlay" id="dom_overlay_container_alma"
                                                 style="pointer-events:none; overflow:hidden; width:590px; height:367px; position: absolute; left: 0px; top: 0px; display: block;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="steps-process slide-process" data-anim="canvas_deliver1" id="step5">
                        <div class="holder">
                            <div class="wrapper-step">
                                <div class="wrapper-information-step">
                                    <div class="inner-information-step">
                                        <div class="title-step">
                                            <div class="inner-title-step">
                                               <?= $title5 ?>
                                            </div>
                                            <div class="number-step">
                                                <div class="inner-step">
                                                   5
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-of-step">
                                        </div>
                                        <div class="list-of-step">
                                            <?= $list5 ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrapper-animation-step">
                                    <div class="inner-animation-step">
                                        <div class="inner-animation-step">
                                            <div class="content-animation" id="animation_container_deliver1"
                                                 style="background-color:rgba(255, 255, 255, 1.00); width:590px; height:367px">
                                                <canvas class="content-canvas" id="canvas_deliver1" width="590"
                                                        height="367"
                                                        style=" display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                                                <div class="content-overlay" id="dom_overlay_container_deliver1"
                                                     style="pointer-events:none; overflow:hidden; width:590px; height:367px; position: absolute; left: 0px; top: 0px; display: block;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="steps-process slide-process" data-anim="canvas_deliver2" id="step6">
                        <div class="holder">
                            <div class="wrapper-step">
                                <div class="wrapper-information-step">
                                    <div class="inner-information-step">
                                        <div class="title-step">
                                            <div class="inner-title-step">
                                                <?= $title6 ?>
                                            </div>
                                            <div class="number-step">
                                                <div class="inner-step">
                                                    6
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-of-step">
                                            <?= $list6 ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrapper-animation-step">
                                    <div class="inner-animation-step">
                                        <div class="content-animation" id="animation_container_deliver2"
                                             style="background-color:rgba(255, 255, 255, 1.00); width:590px; height:367px">
                                            <canvas class="content-canvas" id="canvas_deliver2" width="590" height="367"
                                                    style=" display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                                            <div class="content-overlay" id="dom_overlay_container_deliver2"
                                                 style="pointer-events:none; overflow:hidden; width:590px; height:367px; position: absolute; left: 0px; top: 0px; display: block;">
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </section>


                    <?php if ($page->key === 'hospitality-and-property-development'){ ?>
                    <section class="steps-process slide-process" data-anim="canvas_install" id="step7">
                        <div class="holder">
                            <div class="wrapper-step">
                                <div class="wrapper-information-step">
                                    <div class="inner-information-step">
                                        <div class="title-step">
                                            <div class="inner-title-step">
                                                <?= $title7 ?>
                                            </div>
                                            <div class="number-step">
                                                <div class="inner-step">
                                                    7
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-of-step">
                                        </div>
                                        <div class="list-of-step">
                                            <?= $list7 ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrapper-animation-step">
                                    <div class="inner-animation-step">
                                        <div class="content-animation" id="animation_container_install"
                                             style="background-color:rgba(255, 255, 255, 1.00); width:590px; height:367px">
                                            <canvas class="content-canvas" id="canvas_install" width="590" height="367"
                                                    style=" display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                                            <div class="content-overlay" id="dom_overlay_container_install"
                                                 style="pointer-events:none; overflow:hidden; width:590px; height:367px; position: absolute; left: 0px; top: 0px; display: block;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php } ?>

                    <!-- Benefits -->
                    <?php
                    $benefits = find_page_by_guid($benefits_guid, $root_pages);
                    $benefits_title = $benefits->title;
                    $benefits_intro = $benefits->fragments['text-benefits']->value;

                    $result = Page::search(array(
                        'idparent' => $benefits->idpage,
                        'fragments' => array('icon-benefit', 'desc-benefit'),
                        'sortBy' => 'created ASC'
                    ));
                    $children = $result['records'];
                    ?>
                    <section class="steps-process slide-process block benefits" data-anim="canvas_benefit" id="step8">
                        <div class="holder">
                            <div class="container-fluid">
                                <div class="header">
                                    <div class="title partial"><?= $benefits_title ?></div>
                                    <div class="intro partial"><?= $benefits_intro ?></div>
                                </div>
                                <div class="content">
                                    <? foreach ($children as $child) { ?>
                                        <div class="item">
                                            <div class="icon partial"><?= $child->fragments['icon-benefit']->value ?></div>
                                            <div class="title partial"><span><?= $child->title ?></span></div>
                                            <div class="desc partial"><?= $child->fragments['desc-benefit']->value ?></div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- /.Benefits -->

                </div>
                <div class="animation">
                    <img src="<?= IMGS_PATH ?>scroll-eng.svg" alt="">
                </div>
                <div class="wrapper-marked">
                    <div class="inner-wrapper-mark">
                        <!--<a class="mark-section mark-visited" href="#cover-process">
                            <div class="wrapper-arrow-yellow">
                                <img src="<?= IMGS_PATH ?>yelo-arrow.svg" alt="" class="img-fluid">
                            </div>
                            <div class="mark-figure">
                                <div class="inner-mark-figure">

                                </div>
                            </div>
                            <div class="mark-name">
                                <?= $inner_title ?>
                            </div>
                        </a>-->
                        <a class="mark-section" href="#step-1">
                            <div class="wrapper-arrow-yellow">
                                <img src="<?= IMGS_PATH ?>yelo-arrow.svg" alt="" class="img-fluid">
                            </div>
                            <div class="mark-figure">
                                <div class="inner-mark-figure">

                                </div>
                            </div>
                            <div class="mark-name">
                                <?= $title1 ?>
                            </div>
                        </a>
                        <a class="mark-section" href="#step-2">
                            <div class="wrapper-arrow-yellow">
                                <img src="<?= IMGS_PATH ?>yelo-arrow.svg" alt="" class="img-fluid">
                            </div>
                            <div class="mark-figure">
                                <div class="inner-mark-figure">

                                </div>
                            </div>
                            <div class="mark-name">
                                <?= $title2 ?>
                            </div>
                        </a>
                        <a class="mark-section" href="#step-3">
                            <div class="wrapper-arrow-yellow">
                                <img src="<?= IMGS_PATH ?>yelo-arrow.svg" alt="" class="img-fluid">
                            </div>
                            <div class="mark-figure">
                                <div class="inner-mark-figure">

                                </div>
                            </div>
                            <div class="mark-name">
                                <?= $title3 ?>
                            </div>
                        </a>
                        <a class="mark-section" href="#step-4">
                            <div class="wrapper-arrow-yellow">
                                <img src="<?= IMGS_PATH ?>yelo-arrow.svg" alt="" class="img-fluid">
                            </div>
                            <div class="mark-figure">
                                <div class="inner-mark-figure">

                                </div>
                            </div>
                            <div class="mark-name">
                                <?= $title4 ?>
                            </div>
                        </a>
                        <a class="mark-section" href="#step-5">
                            <div class="wrapper-arrow-yellow">
                                <img src="<?= IMGS_PATH ?>yelo-arrow.svg" alt="" class="img-fluid">
                            </div>
                            <div class="mark-figure">
                                <div class="inner-mark-figure">

                                </div>
                            </div>
                            <div class="mark-name">
                                <?= $title5 ?>
                            </div>
                        </a>
                        <a class="mark-section" href="#step-6">
                            <div class="wrapper-arrow-yellow">
                                <img src="<?= IMGS_PATH ?>yelo-arrow.svg" alt="" class="img-fluid">
                            </div>
                            <div class="mark-figure">
                                <div class="inner-mark-figure">

                                </div>
                            </div>
                            <div class="mark-name">
                                <?= $title6 ?>
                            </div>
                        </a>


                        <?php if ($page->key === 'hospitality-and-property-development'){ ?>
                        <a class="mark-section" href="#step-7">
                            <div class="wrapper-arrow-yellow">
                                <img src="<?= IMGS_PATH ?>yelo-arrow.svg" alt="" class="img-fluid">
                            </div>
                            <div class="mark-figure">
                                <div class="inner-mark-figure">

                                </div>
                            </div>
                            <div class="mark-name">
                                <?= $title7 ?>
                            </div>
                        </a>
                        <?php } ?>

                        <a class="mark-section" href="#step-8">
                            <div class="wrapper-arrow-yellow">
                                <img src="<?= IMGS_PATH ?>yelo-arrow.svg" alt="" class="img-fluid">
                            </div>
                            <div class="mark-figure">
                                <div class="inner-mark-figure">

                                </div>
                            </div>
                            <div class="mark-name">
                                <?= $benefits->title ?>
                            </div>
                        </a>

                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php include_once 'partials/footer.php'; ?>
            <!-- /.Footer -->
        </div>
        <?php include_once 'partials/scripts.php'; ?>
    </body>
</html>