<?php

error_reporting(E_ALL); // Cambiar a E_ALL para mostrar todos los errores durante la depuración
session_start();

// Configuración de CORS para permitir solicitudes desde diferentes orígenes
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type");

include_once 'php/curl/Curl.php';
include_once 'php/phpmailer/Autoloader.php';
include_once 'php/send-mail.php';

// Verifica si el método es GET
if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    // Obtener la información de la orden usando los parámetros de consulta
    if (!isset($_GET['orderId'])) {
        http_response_code(400);
        echo json_encode(['error' => 'No se proporcionó orderId']);
        exit();
    }

    $orderId = $_GET['orderId'];

    // Configuración de cURL
    $ch = curl_init("https://evopaymentsmexico.gateway.mastercard.com/api/rest/version/84/merchant/1023085HPP/order/$orderId");
    $headers = [
        'Authorization: Basic ' . base64_encode('merchant.1023085HPP:fce75e216f451b7b1e4770d36e055eca'),
        'Content-Type: application/json'
    ];

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Ejecutar cURL
    $response = curl_exec($ch);
    //var_dump($response); // Muestra la respuesta cruda de cURL

    // Verificar el código de respuesta HTTP
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    // Intentar decodificar la respuesta JSON
    $data = json_decode($response, true);

    // Verificar el código de respuesta y devolver el resultado
    if ($httpCode === 200) {
        // Aquí es donde llamas a la función sendConfirmationEmail
        //sendConfirmationEmail($response); // Pasas los datos decodificados
        echo $response;
    } else {
        http_response_code($httpCode);
        echo json_encode([
            'error' => 'No se pudo obtener la información de la orden',
            'response' => $data
        ]);
    }
} else {
    http_response_code(405); // Método no permitido
    echo json_encode(['error' => 'Método no permitido']);
}