<?php
    include_once 'globals.php';
    include_once 'util/fragment-helpers.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" itemscope itemtype="http://schema.org/Thing" lang="es-MX">
<head>
    <?php include_once 'partials/head.php'; ?>
</head>
<body>

<!-- Navigation -->
<?php include_once 'partials/navigation.php'; ?>
<!-- /.Navigation -->

<!-- Outer wrapper -->
<div class="outer-wrapper">
    <!-- Block Cover -->

    <section class="block payment" id="pago-en-lnea">
        <div class="holder">
            <div class="container-fluid">
                <form class="checkout-form">
                    <div class="heading">
                        <div>To make the payment for your CargoBaja service, please fill out the following form.</div>
                        <div><small>*Required fields</small></div>
                    </div>
                    <div class="content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-container">
                                    <input data-validation="notEmpty" data-key="name" name="Name" type="text" placeholder="Name*" autocomplete="off" />
                                </div>

                                <div class="input-container">
                                    <input data-validation="notEmpty" data-key="company" name="Company" type="text" placeholder="Company*" autocomplete="off" />
                                </div>

                                <div class="input-container">
                                    <input data-validation="phone" data-key="phone" name="Phone number" type="text" placeholder="Phone number*" autocomplete="off" />
                                </div>

                                <div class="input-container">
                                    <input data-validation="email" data-key="email" name="Email" type="text" placeholder="Email*" autocomplete="off" />
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-container">
                                    <input data-validation="restrict" data-key="quote" name="Quote Number" type="text" placeholder="Quote Number*" autocomplete="off" />
                                </div>

                                <div class="input-container">
                                    <input data-validation="restrict" data-key="invoice" name="Invoice number" type="text" placeholder="Invoice Number*" autocomplete="off" />
                                </div>

                                <div class="input-container">
                                    <select name="Moneda" data-validation="notEmpty" data-key="currency">
                                        <option selected value="MXN">MXN</option>
                                        <option value="USD">USD</option>
                                    </select>
                                </div>

                                <div class="input-container">
                                    <input data-validation="amount" data-key="amount" name="Amount" type="text" placeholder="Amount to pay*" autocomplete="off" />
                                    <!-- <span class="hint">MXN</span> -->
                                </div>

                                <div class="input-container">
                                    <button class="submit" type="submit">
                                        <span class="inner">Pay</span>
                                    </button>
                                </div>

                                <div class="brand-wrapper">
                                    <span>Secure payment with </span>
                                    <img src="<?= IMGS_PATH ?>banamex-logo.svg" alt="Citi Banamex"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="embed-target"></div>

                    <!-- Loader element -->
                    <div class="loader-element"></div>
                    <!-- /.Loader element -->
                </form>

                <!-- Response view -->
                <div class="response">
                    <div class="inner"></div>
                </div>
                <!-- /.Response view -->

            </div>
        </div>
    </section>
    <!-- /.Cover -->
    <!-- Footer -->
    <?php include_once 'partials/footer.php'; ?>
    <!-- /.Footer -->
</div>
<?php include_once 'partials/scripts.php'; ?>
</body>
</html>
