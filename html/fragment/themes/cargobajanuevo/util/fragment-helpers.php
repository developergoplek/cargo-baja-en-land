<?php
/**
 * Created by PhpStorm.
 * User: cesarmejia
 * Date: 31/07/17
 * Time: 18:35
 */

// ---------------------------------------------------------------------
// | Global Variables                                                   |
// ---------------------------------------------------------------------

/*
 * Root Id
 */
$root_id = 1;

/*
 * Home GUID
 */
$home_guid = 'home';

/*
 * Benefits GUID
 */
$benefits_guid = 'un2tJwdjzI';

/**
 * Web Site
 */
$webSite_guid = 'tIBmpX_uuk';
/*
 * Services GUID
 */
$service_guid = 'x6x7TseTrT';
/*
 * Hoteleria service
 */
$hotel_guid = '1yQzxb25V2';
$hotel_id = 10;
/*
 * Residencial service
 */
$residencial_guid = '6FAk0_a7Zn';
$residencial_id = 11;
/*
 * Comercial guid
 */
$comercial_guid = 'uFBNcS0sSE';
$comercial_id = 12;
/*
 * Contact GUID
 */
$contact_guid = 'C8-IFb_vsi';
// ---------------------------------------------------------------------
// | Fragment Helpers                                                   |
// ---------------------------------------------------------------------

/**
 * Find page by guid.
 * @param {string} $guid
 * @param {Array<Page>} $pages
 * @return Page
 */
function find_page_by_guid($guid, $pages)
{
    foreach ($pages as $page) {
        if ($guid === $page->guid)
            return $page;
    }

    return null;
}


/**
 * Find fragments by name.
 * @param $name
 * @param $fragments
 * @return Fragment
 */
function find_fragment_by_name($name, $fragments)
{
    foreach ($fragments as $fragment) {
        if ($name === $fragment->name)
            return $fragment;
    }

    return null;
}


/**
 * Find fragments by name.
 * @param $key
 * @param $fragments
 * @return Fragment
 */
function find_fragment_by_key($key, $fragments)
{
    foreach ($fragments as $fragment) {
        if ($key === $fragment->key)
            return $fragment;
    }

    return null;
}


/**
 * Find setting by name.
 * @param $name
 * @param $settings
 * @return Setting
 */
function find_setting_by_name($name, $settings)
{
    foreach ($settings as $setting) {
        if ($name === $setting->name)
            return $setting;
    }

    return null;
}



/**
 * Get an image tag in string state.
 * @param $image
 * @return string
 */
function get_original_image($image) {
    if ($image) {
        $attrs = Fragment::elementAttributes($image);
        return $attrs['src'];
    }

    return "";
}


/**
 * Determine if current page is home.
 * @throws Exception
 * @return boolean
 */
function is_home() {
    global $page, $home_guid;

    if (!($page instanceof Page)) {
        throw new Exception('$page variable is not in the scope. Maybe you\'re in a file that is not a template.');
    }

    return $page->guid === $home_guid;
}


// ---------------------------------------------------------------------
// | Common pages used                                                  |
// ---------------------------------------------------------------------


// Retrieve root pages.
$result = Page::search(array(
    'idparent' => $root_id,
    'sortBy' => 'created ASC',
    'fragments' => array(
        'text-benefits',
        'icon-benefit',
        'desc-benefit',
        'body',
        'text-cover',
        'contact-text',
        'office_number1',
        'office_number2',
        'personal_number1',
        'personal_number2',
    )
));

$root_pages = $result['records'];
