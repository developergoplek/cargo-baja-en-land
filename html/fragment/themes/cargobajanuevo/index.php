<?php
include_once 'globals.php';
include_once 'util/fragment-helpers.php';

/**
 * Section Cover
 */
$cover_page = find_page_by_guid($service_guid,$root_pages);
$cover_text =   $cover_page->fragments['text-cover']->value;

$result = Page::search(array(
    'idparent' => $cover_page->idpage,
    'sortBy' => 'created ASC',
    'fragments' => array(
        'title-service'
    )
));
$cover_results = $result['records'];

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" itemscope itemtype="http://schema.org/Thing" lang="es-MX">
    <head>
        <?php include_once 'partials/head.php'; ?>
    </head>
    <body>

        <!-- Navigation -->
        <?php include_once 'partials/navigation.php'; ?>
        <!-- /.Navigation -->
        <div class="loader">
            <div class="wrapper-loader">
                <div class="content-loader" id="animation_container_loader"
                     style="background-color:rgba(255, 255, 255, 1.00); width:1300px; height:700px">
                    <canvas class="canvas-loader" id="canvas_loader" width="1300" height="700"
                            style=" display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                    <div class="overlay-loader" id="dom_overlay_container_loader"
                         style="pointer-events:none; overflow:hidden; width:1300px; height:700px; position: absolute; left: 0px; top: 0px; display: block;">
                    </div>
                </div>
            </div>
        </div>
        <!-- Outer wrapper -->
        <div class="outer-wrapper">
            <!-- Block Cover -->
            <section class="block cover" id="home">
                <div class="holder">
                    <div class="wrapper-information-cover">
                        <div class="wrapper-text-cover">
                            <?= $cover_text ?>
                        </div>
                        <div class="wrapper-services-cover">
                            <?php
                            foreach ($cover_results as $option) {
                                $title_cover = $option->fragments['title-service']->value;
                                ?>
                                <a class="inner-services-cover" href="<?= $option->key ?>">
                                    <div class="service-content"><?= $title_cover ?></div>
                                </a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="wrapper-animation-cover">
                        <div class="inner-animation-cover">
                            <div class="content-container-cover" id="animation_container_cover" style="background-color:rgba(255, 255, 255, 1.00); width:593px; height:366px">
                                <canvas class="content-canvas-cover"  id="canvas_cover" width="593" height="366" style="display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                                <div class="content-overlay-cover" id="dom_overlay_container_cover" style="pointer-events:none; overflow:hidden; width:593px; height:366px; position: absolute; left: 0px; top: 0px; display: block;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.Cover -->

            <!-- Footer -->
            <?php include_once 'partials/footer.php'; ?>
            <!-- /.Footer -->

        </div>
        <?php include_once 'partials/scripts.php'; ?>
    </body>
</html>
