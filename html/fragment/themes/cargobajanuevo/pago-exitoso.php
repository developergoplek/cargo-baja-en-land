<?php
    include_once 'globals.php';
    include_once 'util/fragment-helpers.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" itemscope itemtype="http://schema.org/Thing" lang="es-MX">
<head>
    <?php include_once 'partials/head.php'; ?>
</head>
<body>

<!-- Navigation -->
<?php include_once 'partials/navigation.php'; ?>
<!-- /.Navigation -->

<!-- Outer wrapper -->
<div class="outer-wrapper">

    <!-- Block Payment-success -->
    <section class="block payment-success">
        <div class="holder">
            <div class="container-fluid">
                <div class="content">
                </div>
            </div>
        </div>
    </section>
    <!-- /.Payment-success -->

    <!-- Footer -->
    <?php include_once 'partials/footer.php'; ?>
    <!-- /.Footer -->
</div>
<script src="https://evopaymentsmexico.gateway.mastercard.com/static/checkout/checkout.min.js" data-error="errorCallback" data-cancel="cancelCallback"></script>
<?php include_once 'partials/scripts.php'; ?>
        <script type="text/javascript">
                // Obtener la URL actual
                const currentUrl = new URL(window.location.href);

                // Obtener los parámetros de la URL
                const resultIndicator = currentUrl.searchParams.get('resultIndicator');
                const sessionVersion = currentUrl.searchParams.get('sessionVersion');

                // Verificar si ambos parámetros están presentes
                if (!resultIndicator || !sessionVersion) {
                    // Redirigir a la página principal si faltan los parámetros
                    window.location.href = '/';
                } else {
                    let content = document.querySelector('.content'),
                        infoPaymentData = localStorage.getItem('paymentData'),
                        info = JSON.parse(infoPaymentData),
                        orderId = localStorage.getItem('orderId');

                    const fechaActual = new Date(),
                            dia = fechaActual.getDate(),
                            mes = fechaActual.getMonth() + 1,
                            anio = fechaActual.getFullYear();

                    function ajaxRequest(url, method = 'GET', data = null) {
                        const options = {
                            method: method,
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        };

                        return fetch(url, options)
                            .then(response => {
                                if (!response.ok) {
                                    throw new Error('Request failed with status ' + response.status);
                                }
                                return response.json();
                            });
                    }

                    // Uso
                    ajaxRequest('https://en.cargobaja.com/fragment/themes/cargobajanuevo/pedir-orden.php?orderId=' + orderId)
                        .then(response => {
                            console.log('Datos de la orden:', response);
                            content.innerHTML = `
                                <div class='inner'>
                                    <div class='heading'>
                                        <div><b>Your payment was made successfully</b></div>
                                    </div>
                                    <div class='body'>
                                        <h3>Payment Detail</h3>
                                        <!-- Purchase ID's -->
                                        <div class='data'>
                                            <div><b>Order ID</b>: ${orderId}</div>
                                            <div><b>Transaction ID</b>: ${response.transaction[0].transaction.id}</div>
                                        </div>
                                        <!-- Customer -->
                                        <div class='data'>
                                            <div><b>Date</b>: ${dia}/${mes}/${anio}</div>
                                            <div><b>Name</b>: ${info.nombre}</div>
                                            <div><b>Email</b>: ${info.correo}</div>
                                            <div><b>Phone number</b>: ${info.telefono}</div>
                                            <div><b>Quote number</b>: ${info.cotizacion}</div>
                                            <div><b>Invoice number</b>: ${info.factura}</div>
                                        </div>
                                        <!-- Detail -->
                                        <div class='data'>
                                            <div><b>Subtotal</b>: ${info.monto} ${info.currency}</div>
                                            <div><b>Total</b>: ${info.monto} ${info.currency}</div>
                                        </div>
                                    </div>
                                </div>
                            `;
                        })
                        .catch(error => {
                            console.error('Error:', error);
                        });

                }
        </script>
</body>
</html>
