<?php

error_reporting(0);
session_start();

// Get out fo here if no post.
if (empty($_POST)) { die ('Invalid URL'); }

include_once 'php/curl/Curl.php';
include_once 'php/phpmailer/Autoloader.php';
include_once 'php/send-mail.php';

// --------------------------------------------------
//   Define constants                                |
// --------------------------------------------------
define ("MERCHANTID"   , "1023085HPP");
define ("ENDPOINT"     , "https://evopaymentsmexico.gateway.mastercard.com/api/rest/version/62/merchant/1023085HPP/session");
define ("USER"         , "merchant.1023085HPP");
define ("PASS"         , "fce75e216f451b7b1e4770d36e055eca");


//define ("MERCHANTID"   , "1023085HPP");
//define ("ENDPOINT"     , "https://banamex.dialectpayments.com/api/rest/version/45/");
//define ("USER"         , "merchant.1023085HPP");
//define ("PASS"         , "fce75e216f451b7b1e4770d36e055eca");

define ("CURRENCY"     , "MXN");

// --------------------------------------------------
//   Retrieve data from POST                         |
// --------------------------------------------------
$data = json_decode($_POST['data'], true);
$method = isset($data['method']) ? $data['method'] : 'no-method';
$response = array();



// --------------------------------------------------
//   Execute task indicated by method.               |
// --------------------------------------------------
if ($method === 'getSessionId') {

    $api_operation = "CREATE_CHECKOUT_SESSION";
    $order_id = uniqid("ban");
    $url = ENDPOINT;
    $currency = isset($data['currency']) ? $data['currency'] : CURRENCY;

    $data = array(
        "apiOperation" => $api_operation,
        "order" => array(
            "id"       => $order_id,
            "currency" => $currency
        ),
        "interaction" => array(
            "merchant" => array(
                "name" => MERCHANTID
            ),
            "operation" => "NONE",
            "cancelUrl" => "https://en.cargobaja.com/pago-en-lnea",
            "returnUrl" => "https://en.cargobaja.com/pago-en-lnea"
        )
    );

    $json_data = json_encode($data);

    $curl = new Curl($url);

    $curl->useAuth(true);
    $curl->setName(USER);
    $curl->setPass(PASS);
    $curl->setPost($json_data);
    $curl->createCurl();

    $curlRes = $curl->getWebPage();
    $resData = array();

    if ($curlRes) {
        $resData = json_decode($curlRes, true);
    }

    $resData['order-id'] = $order_id;

    $response['success'] = "true";
    $response['message'] = "";
    $response['data']    = $resData;

    // Store checkout session.
    $_SESSION['checkout-session'] = $resData;

} else if ($method === 'check3dsEnrollment') {

    die("Does not supported in Hosted Checkout");
    $api_operation = "CHECK_3DS_ENROLLMENT";
    $_3DSecureId = uniqid("3DSecure-id-");
    $checkoutSession = $_SESSION['checkout-session'];
    $order_id = $checkoutSession['order-id'];
    $url = sprintf("%smerchant/%s/3DSecureId/%s", ENDPOINT, MERCHANTID, $_3DSecureId);

    $data = array(
        "apiOperation" => $api_operation,
        "order" => array(
            "id"       => $order_id,
            "currency" => $currency
        )
    );

} else if ($method === 'sendConfirm') {

    // Retrieve checkout-session from session.
    $checkoutSession = $_SESSION['checkout-session'];
    $order_id = $checkoutSession['order-id'];
    $url = sprintf("%s/merchant/%s/order/%s", ENDPOINT, MERCHANTID, $order_id);


    $curl = new Curl($url);

    $curl->useAuth(true);
    $curl->setName(USER);
    $curl->setPass(PASS);
    $curl->createCurl();

    $curlRes = utf8_encode($curl->getWebPage());
    $resData = array();

    if ($curlRes) {
        $resData = json_decode($curlRes, true);
    }


    $res = sendConfirmationEmail($resData);

    $response['success'] = "true";
    $response['message'] = "Message sended successfully";
    $response['data']    = json_encode($resData);

} else {
    $response['success'] = "false";
    $response['message'] = "No method found";
    $response['data']    = $_POST;
}

echo json_encode($response);
