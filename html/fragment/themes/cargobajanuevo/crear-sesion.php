<?php

error_reporting(0);
session_start();

// Get out fo here if no post.
if (empty($_POST)) { die ('Invalid URL'); }

// Configuración de CORS para permitir solicitudes desde diferentes orígenes
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type");

// Verifica si el método es POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $data = json_decode($_POST['data'], true);

    // Si el método es 'sendConfirm', envía el correo de confirmación
    if ($data['method'] === 'sendConfirm') {
        // Llamar a la función que envía el correo
        //sendConfirmationEmail();
        echo json_encode(['status' => 'success', 'message' => 'Confirmation email sent.']);
        exit;
    }

    $amount = $data["monto"];
    $currency = $data["currency"];

    // API de EVO Payments URL y credenciales
    $url = 'https://evopaymentsmexico.gateway.mastercard.com/api/rest/version/83/merchant/1023085HPP/session';
    $username = 'merchant.1023085HPP';
    $password = 'fce75e216f451b7b1e4770d36e055eca';

    // Datos de la solicitud para crear la sesión
    $data = [
        "apiOperation" => "INITIATE_CHECKOUT",
        "interaction" => [
            "operation" => "PURCHASE",
            "cancelUrl" => "https://en.cargobaja.com/pago-en-lnea",
            "returnUrl" => "https://en.cargobaja.com/pago-exitoso",
            "merchant" => [
                "name" => "CARGO BAJA CIB"
            ]
        ],
        "order" => [
            "amount" => $amount,
            "currency" => $currency,
            "reference" => '1023085HPP',
            "description" => "Pago de servicio",
            "id" => uniqid("ban") // Generar un ID único para la orden
        ]
    ];

    // Inicia la solicitud cURL
    $ch = curl_init($url);

    // Configura las opciones de cURL
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Authorization: Basic ' . base64_encode("$username:$password"),
        'Content-Type: application/json'
    ]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

    // Ejecuta la solicitud y captura la respuesta
    $response = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    // Convierte la respuesta JSON en un array PHP
    $response_data = json_decode($response, true);

    if ($http_code === 201) {
        // Devuelve el sessionId si la solicitud fue exitosa
        echo json_encode(['sessionId' => $response_data['session']['id'], 'orderId' => $data['order']['id']]);
    } else {
        // Devuelve la respuesta completa si hubo un error
        http_response_code($http_code);
        echo json_encode($response_data);
    }
} else {
    // Método no permitido
    http_response_code(405);
    echo json_encode(['error' => 'Método no permitido']);
}
