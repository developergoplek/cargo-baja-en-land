<?php
/**
 * Created by PhpStorm.
 * User: developergoplek
 * Date: 12/10/17
 * Time: 14:20
 */

/**
 * Create email template.
 * @param {string} $content
 * @returns string
 */
function createTemplate($content)
{
    // Define common variables
    $SITE_URL   = "www.cargobaja.com";
    $IMAGE_LOGO = "http://cargobaja.com/design/imgs/logo.png";

    // region Template
    $template = '
        <table style="font-family: \'Arial\', sans-serif;line-height: 100%; margin: 0 auto; max-width: 520px; width: 100%;">
            <tr>
                <td>
                    <table cellpadding="10" style="    background-color: #fff; border-bottom: 2px solid #0d9041; width: 100%;">
                        <tr>
                            <td><img style="width: 110px;" src="' . sprintf("%s", $IMAGE_LOGO) . '" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="20" style="background-color: #fff; font-size: 12px; line-height: 135%; width: 100%;">
                        <tr>
                            <td>' . $content . '</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="10" style="background-color: #0D9041; font-size: 11px; width: 100%;">                
                        <tr>
                            <td style="color: #fff; text-align: center;">
                                <a href="' . $SITE_URL . '" style="color: #fff; text-decoration: none;" target="_blank">' . $SITE_URL . '</a>
                            </td>
                        </tr>                
                    </table>
                </td>
            </tr>
        </table>
    ';
    // endregion

    return $template;
}

/**
 * Send mail.
 * @param $template
 * @param string $subject
 * @param string $email
 * @param array $cc
 * @param array $cco
 * @return bool
 */
function sendMail($template, $subject = "Confirmación de Pago", $email = "cesarmejiaguzman@gmail.com", $cc = array(), $cco = array())
{
    $mail = new PHPMailer();
    $mail->SMTPDebug = 0;
    $mail->isSMTP();                                    // Set mailer to use SMTP
    $mail->CharSet = 'UTF-8';
    $mail->Host = 'mail.lagranbodega.com.mx';           // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                             // Enable SMTP authentication
    $mail->SMTPDebug = 0;
    $mail->Username = 'no-reply@lagranbodega.com.mx';   // SMTP username
    $mail->Password = 'N0-r3ply';                       // SMTP password
    $mail->SMTPSecure = 'smtp';                         // Enable encryption, 'ssl' also accepted
    $mail->Port = 587;
    $mail->From = 'no-reply@cargobaja.com';
    $mail->FromName = 'CargoBaja';
    $mail->IsHTML(true);

    $mail->Subject = $subject;
    $mail->Body = $template;
    $mail->addReplyTo('no-reply@cargobaja.com', 'CargoBaja');
    $mail->addAddress($email);
    $mail->addBCC('cesarmejiaguzman@gmail.com');
    $mail->addBCC('e.lopez@cargobaja.com');
    $mail->addBCC('bancos@cargobaja.com');

    foreach ($cc as $c)  { $mail->addCC($c); }
    foreach ($cco as $c) { $mail->addBCC($c); }

    return $mail->send();
}

/**
 * Send confirmation email.
 * @param $data
 * @return bool
 */
function sendConfirmationEmail($data)
{
    $customer = $data['customer'];
    $transaction = $data['transaction'][0];
    $order = $transaction['order'];

    $order_id = $order['id'];
    $auth_code = $transaction['transaction']['authorizationCode'];

    $customer_email     = $customer['email'];
    $customer_firstName = $customer['firstName'];
    $customer_lastName  = $customer['lastName'];
    $customer_phone     = $customer['phone'];

    $order_desc     = $order['description'];
    $order_subtotal = $order['amount'] . " " . $order['currency'];
    $order_total    = $order['amount'] . " " . $order['currency'];

    $description = $data['description'];
    $amount = $data['amount'];
    $currency = $data['currency'];
    $amount_formated = "$" . number_format($amount, 2, ".", ",");
    $lang = $currency === "MXN" ? "ES" : "EN";

    if ($lang === "ES") {
        $emailSubject = "Confirmación de Pago - " . $description;
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        $content = "
            <div>
                <div>Hola $customer_firstName,</div>
                <div>En CargoBaja agradecemos tu pago por la cantidad de $amount_formated $currency.</div>
                <div><br /></div>
                <div style='color: #000; font-style: italic; padding: 10px 20px;'>
                    <div style='border-bottom: 1px solid #000; margin-bottom: 10px; padding-bottom: 10px; text-align: center;'><b>Detalle de Pago</b></div>
                    <div style='margin-bottom: 10px;'>
                        <div><b>ID Pedido</b>: $order_id</div>
                        <div><b>Código de Autorización</b>: $auth_code</div>
                    </div>
                    <div style='margin-bottom: 10px;'>
                        <div><b>Fecha</b>:" . date('d') . '/' . $meses[date('n') - 1] . '/' . date('Y') . "</div>
                        <div><b>Nombre</b>: $customer_firstName $customer_lastName</div>
                        <div><b>Email</b>: $customer_email</div>
                        <div><b>Teléfono</b>: $customer_phone</div>
                    </div>
                    <div>
                        <div><b>Detalle</b>: $order_desc</div>
                        <div><b>Subtotal</b>: $amount_formated $order_subtotal</div>
                        <div><b>Total</b>: $amount_formated $order_total</div>
                    </div>
                </div>
                <div><br /></div>
                <div>Para cualquier duda o aclaración, con gusto estamos a sus órdenes en <a href='mailto:bancos@cargobaja.com' target='_blank'>bancos@cargobaja.com</a></div>
            </div>
        ";

    } else {
        $emailSubject = "Payment confirmation - " . $description;

        $content = "
            <div>
                <div>Hi $customer_firstName,</div>
                <div>In CargoBaja we would like to thank you for your payment of $amount_formated $currency.</div>
                <div><br /></div>
                <div style='color: #000; font-style: italic; padding: 10px 20px;'>
                    <div style='border-bottom: 1px solid #000; margin-bottom: 10px; padding-bottom: 10px; text-align: center;'><b>Payment Detail</b></div>
                    <div style='margin-bottom: 10px;'>
                        <div><b>ID Order</b>: $order_id</div>
                        <div><b>Authorization Code</b>: $auth_code</div>
                    </div>
                    <div style='margin-bottom: 10px;'>
                        <div><b>Date</b>:" . date('d') . '/' . date('F') . '/' . date('Y') . "</div>
                        <div><b>Name</b>: $customer_firstName $customer_lastName</div>
                        <div><b>Email</b>: $customer_email</div>
                        <div><b>Phone</b>: $customer_phone</div>
                    </div>
                    <div>
                        <div><b>Detail</b>: $order_desc</div>
                        <div><b>Subtotal</b>: $amount_formated $order_subtotal</div>
                        <div><b>Total</b>: $amount_formated $order_total</div>
                    </div>
                </div>
                <div><br /></div>
                <div>For any further questions please contact us at the following email address <a href='mailto:c.finanzas@cargobaja.com' target='_blank'>c.finanzas@cargobaja.com</a></div>
            </div> 
        ";

    }

    $template = createTemplate($content);

    return sendMail($template, $emailSubject, $customer_email);
}