<?php
    include_once "../globals.php";
    include_once '../util/fragment-helpers.php';

    $current_section = $_SERVER['REQUEST_URI'];
    $intern_section = ($current_section != "/") ? 'changePosition': '';

?>
<footer class="footer <?= $intern_section ?>">
    <div class="holder">
        <div class="container-fluid">
            <div class="content">
                <div class="footer-left">
                    <a href="#" class="logo-cargo-baja">
                        <img src="<?= IMGS_PATH ?>cargo-baja-footer-logo.svg" alt="">
                    </a>
                    <div class="copyright">
                        <span>CargoBaja 2019 </span><br>
                        <span>Todos los derechos reservados</span>
                    </div>
                </div>
                <div class="footer-right">
                    <div class="privacity">
                        <span>Aviso de privacidad</span>
                    </div>
                    <a href="http://www.goplek.com" target="_blank" class="logo-goplek">
                        <img src="<?= IMGS_PATH ?>goplek-logo-footer.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>