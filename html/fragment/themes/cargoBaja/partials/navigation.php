<?php
include_once "../util/fragment-helpers.php";
$listOpts = '';

if(is_home())
    $location = '';
else
    $location = 'intern';

// region List
foreach ($root_pages as $option) {
    $class = '';
    $ref = '#';
    $scroll = 'scroll-to';

    if(!(is_home()) && $option->guid !== $payment_guid ) {
        $ref = '/#';
        $scroll = 'scroll-to';
    }

    if(!(is_home()) && $option->guid === $payment_guid) {
        $ref = '/';
    }

    if((is_home()) && $option->guid === $payment_guid) {
        $ref = '/';
    }

    // Avoid sections.
    if ($option->guid === $home_guid
        || $option->guid === $specialists_guid
        || $option->guid === $footer_guid) continue;

    $listOpts .= '<li class="' . $class .'" ><a class="'.$scroll.'" href="'.$ref.$option->key.'">' . $option->title . '</a></li>';
}

$listOpts .= '<li><a class="lang" href="http://www.google.com">English</a></li>';

/**
 * Children of products
 */
?>
<nav class="navigation <?= $location ?>">
    <div class="holder navigation-holder">
        <div class="container-fluid">
            <!-- Logo -->
            <a class="logo" href="/">
                <span class="wrapper-logo-nav">
                    <img alt="Cargo Baja - logo" class="img-responsive" src="<?= IMGS_PATH ?>cargobaja-logo.svg">
                </span>
            </a>
            <!--Navigation-->
            <div class="list-wrapper">
                <ul class="list"><?= $listOpts ?></ul>
            </div>

            <!-- Toggle Button -->
            <button class="toggle-btn">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </button>
        </div>
    </div>
</nav>