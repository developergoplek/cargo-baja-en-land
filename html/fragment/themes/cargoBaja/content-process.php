<?php
include_once 'globals.php';
include_once 'util/fragment-helpers.php';

$current_parent = $page->idparent;
$current_id = $page->idpage;
$result = Page::search(array(
    'idparent' => $current_parent,
    'sortBy' => 'created ASC',
    'fragments' => array(
        'type-category',
        'subtitle-category',
        'description-category',
        'number-process1', //
        'title-process1',
        'list-process1',
        'image-category-big1',
        'image-category-small1',
        'number-process2', //
        'title-process2',
        'list-process2',
        'image-category-big2',
        'image-category-small2',
        'number-process3', //
        'title-process3',
        'list-process3',
        'image-category-big3',
        'number-process4', //
        'title-process4',
        'list-process4',
        'image-category-big4',
        'number-process5', //
        'title-process5',
        'list-process5',
        'number-process6', //
        'title-process6',
        'list-process6',
        'number-process7', //
        'title-process7',
        'list-process7',
        'image-category-big7'
    )
));

$current_results = $result['records'];

foreach ($current_results as $process) {
    $id_actually = $process->idpage;
    if ($id_actually === $current_id) {
        $category_process = $process->fragments['type-category']->value;
        $subtitle_process = $process->fragments['subtitle-category']->value;
        $inner_description_process = $process->fragments['description-category']->value;

        $n1_process = $process->fragments['number-process1']->value;
        $n1_title = $process->fragments['title-process1']->value;
        $n1_list = $process->fragments['list-process1']->value;
        $n1_image_big = $process->fragments['image-category-big1']->value;
        $n1_src_big = get_original_image($n1_image_big);
        $n1_image_small = $process->fragments['image-category-small1']->value;
        $n1_src_small = get_original_image($n1_image_small);

        $n2_process = $process->fragments['number-process2']->value;
        $n2_title = $process->fragments['title-process2']->value;
        $n2_list = $process->fragments['list-process2']->value;
        $n2_image_big = $process->fragments['image-category-big2']->value;
        $n2_src_big = get_original_image($n2_image_big);
        $n2_image_small = $process->fragments['image-category-small2']->value;
        $n2_src_small = get_original_image($n2_image_small);

        $n3_process = $process->fragments['number-process3']->value;
        $n3_title = $process->fragments['title-process3']->value;
        $n3_list = $process->fragments['list-process3']->value;
        $n3_image_big = $process->fragments['image-category-big3']->value;
        $n3_src_big = get_original_image($n3_image_big);

        $n4_process = $process->fragments['number-process4']->value;
        $n4_title = $process->fragments['title-process4']->value;
        $n4_list = $process->fragments['list-process4']->value;
        $n4_image_big = $process->fragments['image-category-big4']->value;
        $n4_src_big = get_original_image($n4_image_big);

        $n5_process = $process->fragments['number-process5']->value;
        $n5_title = $process->fragments['title-process5']->value;
        $n5_list = $process->fragments['list-process5']->value;

        $n6_process = $process->fragments['number-process6']->value;
        $n6_title = $process->fragments['title-process6']->value;
        $n6_list = $process->fragments['list-process6']->value;

        $n7_process = $process->fragments['number-process7']->value;
        $n7_title = $process->fragments['title-process7']->value;
        $n7_list = $process->fragments['list-process7']->value;
        $n7_image_big = $process->fragments['image-category-big7']->value;
        $n7_src_big = get_original_image($n7_image_big);

    }
}

$current_section = $_SERVER['REQUEST_URI'];
$intern_vh = ($current_section != "/") ? 'vh-inne': ''; 
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" itemscope itemtype="http://schema.org/Thing" lang="es-MX">
    <head>
        <?php include_once 'partials/head.php'; ?>
    </head>
    <body>
        <!-- Navigation -->
        <?php include_once 'partials/navigation.php'; ?>
        <!-- /.Navigation -->

        <!-- Outer wrapper -->
        <div class="outer-wrapper intern  <?= $intern_vh ?>">

            <div class="inner-outer-wrapper">

                <!--- Pagination --->
                <div class="wrapper-process-pagination">
                    <div class="inner-process-pagination">
                        <div class="process-information step-element"  id="step1">
                            <div class="content-name">
                                <div class="process-name">
                                    <?= $n1_title ?>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper-arrow-process">
                            <div class="inner-wrapper-arrow">
                                <div class="content-arrow">
                                    <img src="<?= IMGS_PATH ?>arrow-pagination.svg" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="process-information transparent-bk step-element" id="step2">
                            <div class="content-name">
                                <div class="process-name">
                                    <!--<?= $n2_title ?>-->
                                    Compras
                                </div>
                            </div>
                        </div>
                        <div class="wrapper-arrow-process">
                            <div class="inner-wrapper-arrow">
                                <div class="content-arrow">
                                    <img src="<?= IMGS_PATH ?>arrow-pagination.svg" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="process-information step-element" id="step3">
                            <div class="content-name">
                                <div class="process-name">
                                    <?= $n3_title ?>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper-arrow-process">
                            <div class="inner-wrapper-arrow">
                                <div class="content-arrow">
                                    <img src="<?= IMGS_PATH ?>arrow-pagination.svg" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="process-information transparent-bk step-element" id="step4-5">
                            <div class="content-name">
                                <div class="process-name">
                                    <?= $n4_title ?>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper-arrow-process">
                            <div class="inner-wrapper-arrow">
                                <div class="content-arrow">
                                    <img src="<?= IMGS_PATH ?>arrow-pagination.svg" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="process-information step-element" id="step4-5">
                            <div class="content-name">
                                <div class="process-name">
                                    <?= $n5_title ?>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper-arrow-process">
                            <div class="inner-wrapper-arrow">
                                <div class="content-arrow">
                                    <img src="<?= IMGS_PATH ?>arrow-pagination.svg" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="process-information transparent-bk step-element" id="step6-7">
                            <div class="content-name">
                                <div class="process-name">
                                    <?= $n6_title ?>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper-arrow-process">
                            <div class="inner-wrapper-arrow">
                                <div class="content-arrow">
                                    <img src="<?= IMGS_PATH ?>arrow-pagination.svg" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="process-information step-element" id="step6-7">
                            <div class="content-name">
                                <div class="process-name">
                                    <?= $n7_title ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--- /. --->

                <!--- Return --->
                <div class="wrapper-return">
                    <a href="/#conoce-nuestro-proceso" class="btn-return"><span> Regresar </span></a>
                </div>
                <!--- /. --->

                <!--- Presentation --->
                <section class="presentation">
                    <div class="wrapper-presentation" id="presentation">
                        <div class="inner-wrapper-presentation">
                            <div class="presentation-title"><?= $category_process ?></div>
                            <div class="presentation-subtitle"><?= $subtitle_process ?></div>
                            <div class="presentation-description"><?= $inner_description_process ?></div>
                        </div>
                    </div>
                </section>
                <!--- /. --->

                <!--- step1--->
                <section class=" steps step1" id="step1">
                    <div class="wrapper-step">
                        <div class="inner-step">
                            <div class="wrapper-image">
                                <div class="content-img big">
                                    <img src="<?= $n1_src_big ?>" alt="" class="img-fluid">
                                </div>
                                <div class="content-img small">
                                    <?php
                                    if ($n1_src_small)
                                        $back_small = sprintf('style="background:url(%s)  repeat center; background-size: cover;"', $n1_src_small);
                                      ?>
                                    <div class="back-img-small" <?= $back_small ?> >
                                    </div>
                                </div>
                            </div>
                            <div class="wrapper-information">
                                <div class="number-process"> <div class="inner-number"><?= $n1_process ?></div></div>
                                <div class="title-process"><?= $n1_title ?></div>
                                <div class="content-list">
                                    <ul>
                                        <?= $n1_list ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--- /. --->

                <!--- step2--->
                <section class=" steps step2" id="step2">
                    <div class="wrapper-step">
                        <div class="inner-step">
                            <div class="wrapper-image">
                                <div class="content-img big">
                                    <img src="<?= $n2_src_big ?>" alt="" class="img-fluid">
                                </div>
                                <div class="content-img small">
                                    <?php
                                    if ($n2_src_small)
                                        $back_small_n2 = sprintf('style="background:url(%s)  repeat center; background-size: cover;"', $n2_src_small);
                                      ?>
                                    <div class="back-img-small" <?= $back_small_n2 ?> >
                                    </div>
                                </div>
                            </div>
                            <div class="wrapper-information">
                                <div class="number-process"> <div class="inner-number"><?= $n2_process ?></div></div>
                                <div class="title-process"><?= $n2_title ?></div>
                                <div class="content-list">
                                    <ul>
                                        <?= $n2_list ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--- /. --->

                <!--- step3--->
                <section class=" steps step3" id="step3">
                    <div class="wrapper-step">
                        <div class="inner-step">
                            <div class="wrapper-image">
                                <div class="content-img big">
                                    <img src="<?= $n3_src_big ?>" alt="" class="img-fluid">
                                </div>
                            </div>
                            <div class="wrapper-information">
                                <div class="number-process">
                                    <div class="inner-number"><?= $n3_process ?></div>
                                </div>
                                <div class="title-process"><?= $n3_title ?></div>
                                <div class="content-list">
                                    <ul>
                                        <?= $n3_list ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--- /. --->

                <!--- step 4,5--->
                <section class=" steps step4-5" id="step4-5">
                    <div class="wrapper-step">
                        <div class="inner-step">
                            <div class="wrapper-image">
                                <div class="content-img big">
                                    <img src="<?= $n4_src_big ?>" alt="" class="img-fluid">
                                </div>
                            </div>
                            <div class="content-wrapper-information">
                                <div class="wrapper-information pro4">
                                    <div class="number-process">
                                        <div class="inner-number"><?= $n4_process ?></div>
                                    </div>
                                    <div class="title-process"><?= $n4_title ?></div>
                                    <div class="content-list">
                                        <ul>
                                            <?= $n4_list ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="wrapper-information pro5">
                                    <div class="number-process">
                                        <div class="inner-number"><?= $n5_process ?></div>
                                    </div>
                                    <div class="title-process"><?= $n5_title ?></div>
                                    <div class="content-list">
                                        <ul>
                                            <?= $n5_list ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--- /. --->

                <!--- step 6,7--->
                <section class=" steps step6-7" id="step6-7">
                    <div class="wrapper-step">
                        <div class="inner-step">
                            <div class="wrapper-image">
                                <div class="content-img big">
                                    <img src="<?= $n7_src_big ?>" alt="" class="img-fluid">
                                </div>
                            </div>

                            <div class="content-wrapper-information">

                                <div class="wrapper-information pro6">
                                    <div class="number-process">
                                        <div class="inner-number"><?= $n6_process ?></div>
                                    </div>
                                    <div class="title-process"><?= $n6_title ?></div>
                                    <div class="content-list">
                                        <ul>
                                            <?= $n6_list ?>
                                        </ul>
                                    </div>
                                </div>

                                <div class="wrapper-information pro7">
                                    <div class="number-process">
                                        <div class="inner-number"><?= $n7_process ?></div>
                                    </div>
                                    <div class="title-process"><?= $n7_title ?></div>
                                    <div class="content-list">
                                        <ul>
                                            <?= $n7_list ?>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </section>
                <!--- /. --->

            </div>
            <!-- Footer -->
            <?php include_once 'partials/footer.php'; ?>
            <!-- /.Footer -->
        </div>
        <?php include_once 'partials/scripts.php'; ?>
    </body>
</html>
