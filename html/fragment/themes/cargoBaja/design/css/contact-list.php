<?php
/**
 * List of contacts by site.
 * new: date-01/09/15
 */
return array(

    'adhara.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),
    'aepsa.mx' => array(
        'to' => 'rodolfo.cervantes@aepsa.mx, rodolfo_cervantes@aepsa.mx, daniel.delgado@aepsa.mx, concepcion.villasenor@aepsa.mx, contacto@aepsa.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'acadari.goplek.com' => array(
        'to' => 'acadariboutique@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'agbastudio.com' => array(
        'to' => 'hello@anageorgina.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'alari.com.mx' => array(
        'to' => 'marycvillarreal@hotmail.com, informes@alari.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'alkimya.mx' => array(
        'to' => 'hola@alkimya.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'agenbook.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'aguaja.com' => array(
        'to' => 'redes@aguaja.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'aige.com.mx' => array(
        'to' => 'rhumanosb@aige.com.mx',
        'cc' => '',
        'bcc' => 'emartinez@goplek.com, concentradorgoplek@gmail.com'),

    'adriancosio.com' => array(
        'to' => 'adriancosio@actioncoach.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'altec.goplek.com' => array(
        'to' => 'ventas@todogps.com.mx, info@garminpuebla.com, spmikebracas@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'amantemexicano.com' => array(
        'to' => 'moises@amantemexicano.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'anamontoto.com' => array(
        'to' => 'fotomontoto@hotmail.com, sebastian.rodz90@gmail.com', /*'ilopez@goplek.com, jmartinez@goplek.com',*/
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'en.anamontoto.com' => array(
        'to' => 'fotomontoto@hotmail.com, sebastian.rodz90@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'andreaypedro.com' => array(
        'to' => 'confirmaciones@andreaypedro.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'antaic.com' => array(
        'to' => 'contacto@antaic.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'amazingpedia.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'ammayoga.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'ammayoga.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'ampec.org.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'amway-mass.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'andy.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'annabanana.com.br' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'annabanana.goplek.com' => array(
        'to' => 'annabanana@annabanana.com.br',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'apoyofinanciero.aprecia.com.mx' => array(
        'to' => 'conexion.sucursales@fomepade.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'arcoylira.com.mx' => array(
        'to' => 'nina@arcoylira.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, ilopez@goplek.com'),

    'arkana.mx' => array(
        'to' => 'contacto@arkana.mx, gerencia@arkana.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'), 

    'arionproducciones.com' => array(
        'to' => 'info@arionproducciones.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'arivu.mx' => array(
        'to' => 'ruthrb15@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'armarioestilar.com' => array(
        'to' => ' info@armarioestilar.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'arriera.com' => array(
        'to' => 'fernando@arrierafoods.com, fernando@industriapuebla.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'arxconsultores.com' => array(
        'to' => ' gvinatier@gmail.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'atlaco128.com' => array(
        'to' => 'ventas@7cimas.mx, eugenioperezrivero@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'backup_brandsid.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''), 'babalumexico.com' => array(
        'to' => 'ventas@babalumexico.com', 'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'begolf.goplek.com' => array(
        'to' => 'contacto@begolf.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'bicicletasmdg.com' => array(
        'to' => 'ventas@bicicletasmdg.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com, mlechuga@goplek.com'),

    'bioanalisis.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'blinsafe.com.mx' => array(
        'to' => 'atlaiye@claudia.com.mx, contacto@blinsafe.com.mx',
        'cc' => 'contabilidad@xeseguridad.mx',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'blog.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'cnvs.mx' => array(
        'to' => 'howdy@cnvs.mx, alejandro@cnvs.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'bakeit.goplek.com' => array(
        'to' => 'ilopez@goplek.com',
        'cc' => '',
        'bcc' => 'ihernandez@goplek.com'),

    'bowi.com.mx' => array(
        'to' => 'claudio.s@bowi.com.mx, rafael.m@bowi.com.mx, ventas@bowi.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'brandsid-editor.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'brandsid.com' => array(
        'to' => 'claudia@brandsid.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, jmenendez@goplek.com, mlechuga@goplek.com'),

    'brandsid.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'brandsid.goplek.com' => array(
        'to' => 'claudia@brandsid.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, jmenendez@goplek.com, mlechuga@goplek.com'),

    'brandsid.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'brandsidstore.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'brandsidstore.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'mktatencion.idiomasvw.com.mx' => array(
        'to' => 'idiomasvw@amatech.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'
    ),

    'brik.co.nz' => array(
        'to' => 'eugenio@brik.co.nz',
        'cc' => '',
        'bcc' => ' alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'brik.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => 'menendezpoo+bak@gmail.com, alvaro@goplek.com'),

    'brikstudio.goplek.com' => array(
        'to' => 'menendezpoo+bak@gmail.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com'),

    'brilio.mx' => array(
        'to' => 'roscabecerra@lazzarmexico.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),
             
     'camionetasdelujo.imperialvans.com.mx' => array(
        'to' => 'gonzalo.tinoco@imperialvans.com.mx',
        'cc' => '',
        'bcc' => 'imperialvans@gmail.com,concentradorgoplek@gmail.com'),

    'canchasdepastosintetico.grassandgo.com.mx' => array(
        'to' => 'direccion@grassandgo.com.mx',
        'cc' => 'guillermo.cuadra@grassandgo.com.mx',
        'bcc' => 'concentradorgoplek@gmail.com, mlechuga@goplek.com'),

    'carmenyalvaro.com' => array(
        'to' => 'alvaro@goplek.com, karmen.hz90@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'camada.mx' => array(
        'to' => 'contacto.camada@gmail.com, info@camada.mx, valefenton@gmail.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com'),

    'certificacion-dele.idiomasvw.com.mx' => array(
        'to' => 'mlechuga@goplek.com, idiomasvw@amatech.com.mx, octavio.saucedo@ivw.com.mx',
        'cc' => 'centrosdeidiomasvw@gmail.com',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'camioneschatos.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'camioneschatos.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'casamexicanacuernavaca.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'cclandestino.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'cdpclothing.com' => array(
        'to' => 'jhachity@cdpclothing.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'cdpclothing.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'chachisabed.com' => array(
        'to' => 'contacto@chachisabed.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'chamaconandy.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'chefdussan.com' => array(
        'to' => 'dussangroup@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@goplek.com'),

    'chronoimports.com' => array(
        'to' => 'juanpablozanella@chronoimports.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'citygate.com.mx' => array(
        'to' => 'mmora@citygate.com.mx, aleventosap@gmail.com',
        'cc' => '',
        'bcc' => 'mlechuga@goplek.com, menendezpoo+bak@gmail.com, concentradorgoplek@gmail.com'),

    'clinicadediagnostico.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'coconetes.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'cocoyrafa.com' => array(
        'to' => 'rafaaja@gmail.com, cococo.hola@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@goplek.com'),

    'collective.travel' => array(
        'to' => 'info@collective.travel',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'construfacildepuebla.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'convida.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'copatinta.com' => array(
        'to' => 'santiago@copatinta.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'corpomin-es.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'corpomin.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'corporativoinlakech.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'corpustrailers.com' => array(
        'to' => 'ventas@corpustrailers.com, gerencia@corpustrailers.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'
    ),

    'cortearte.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'cosio.goplek.com' => array(
        'to' => 'ihernandez@goplek.com',
        'cc' => '',
        'bcc' => 'ihernandez@goplek.com'),

    'cube.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => 'jmenendez@goplek.com'),

    'cuadernospersonalizadosprintea.com' => array(
        'to' => 'josemiguel@printea.com.mx, mariajose@printea.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'cuatrosecretos.com' => array(
        'to' => ' info@cuatrosecretos.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'cuguz.com' => array(
        'to' => 'jamase05@yahoo.com.mx',
        'cc' => '',
        'bcc' => 'dperez@goplek.com, concentradorgoplek@gmail.com'),

    'cparada.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'dakinigraphie.com' => array(
        'to' => 'karla.reyes@dakinigraphie.com',
        'cc' => '',
        'bcc' => ''),

    'danielrodriguezangulo.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'datalatte.net' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),
             
     'comercial.desmexsolarangelopolis.com' => array(
        'to' => 'info@dsangelopolis.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'deportika.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com'),

    'dinaqua.com' => array(
        'to' => 'ventas@dinaqua.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'dintorno.com' => array(
        'to' => 'ventas@dintorno.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'dlh.mx' => array(
        'to' => 'dlh@laheredera.com.mx',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'donpacolopez.com' => array(
        'to' => 'lopezanel@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, mlechuga@goplek.com'),

    'dperez.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => 'dperez@goplek.com'),

    'dreguibar.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'drtrivia.com.mx' => array(
        'to' => 'comentarios@drtrivia.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'dynamic-waves.com' => array(
        'to' => 'adrian@dynamic-waves.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'ecodom.mx' => array(
        'to' => 'ventas@ecodom.mx, ventasecodom@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'en.ecodom.mx' => array(
        'to' => 'ventas@ecodom.mx, ventasecodom@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'ecovert.mx' => array(
        'to' => 'ventas@ecovert.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'edificacioneslr.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'elaserrin.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'elaserrin.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'elbucha.com' => array(
        'to' => 'contacto@elbucha.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'eldesafiodeoaxaca.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'eldesafiodeoaxaca.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'en-cdpclothing.goplek.com' => array(
        'to' => 'jhachity@cdpclothing.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'e-centinela.com' => array(
        'to' => 'heberto.blancarte@controlelectrico.com, julio.arvizu@controlelectrico.com, compras@controlelectrico.com, ventas07@e-centinela.com, spichardo@goplek.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'enterarte.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'en.donpacolopez.com' => array(
        'to' => 'lopezanel@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, mlechuga@goplek.com'),

    'en.lagranfama.com' => array(
        'to' => 'contacto@lagranfama.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'en.luxist.com.tr' => array(
        'to' => 'fijiwater@luxist.com.tr',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'en.mezcalvelua.com' => array(
        'to' => 'can@mezcalvelua.com, contacto@mezcalvelua.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'epicgrass.com.mx' => array(
        'to' => 'javierinfante@epicgrass.com.mx, areacomercial@epicgrass.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, operacion@epicgrass.com.mx, sistemas@epicgrass.com.mx'),

    'eportusa.com' => array(
        'to' => 'vm@sialico.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, mlechuga@goplek.com, concentradorgoplek@gmail.com'),

    'estra.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'estrelladebelem.com.mx' => array(
        'to' => 'servicios@estrelladebelem.com.mx',
        'cc' => '',
        'bcc' => 'belem@estrelladebelem.com.mx, concentradorgoplek@gmail.com'),

    'estumeta.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'estumeta.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'metraoptical.com' => array(
        'to' => 'contacto@metraoptical.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'es.dynamic-waves.com' => array(
        'to' => 'adrian@dynamic-waves.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'eurekamexico.com' => array(
        'to' => 'dpenalver.sitec@gmail.com',
        'cc' => '',
        'bcc' => 'dperez@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'eurokom.goplek.com' => array(
        'to' => 'cfremuth@ekommexico.com',
        'cc' => '',
        'bcc' => 'dperez@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'evadana.com' => array(
        'to' => 'fernanda.calvo@evadana.com, martin.cortes@evadana.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'exakta-en.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'exakta.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'excelnobleza.com.mx' => array(
        'to' => 'ventas@excelnobleza.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'en.excelnobleza.com.mx' => array(
        'to' => 'ventas@excelnobleza.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),


    'expobautizoycomunion.com' => array(
        'to' => 'georgettejm@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'fabricaselvalor.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'feedback.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'felixquintanabanquetes.com' => array(
        'to' => 'felixquintana@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'fernandarojas.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'festivalvaiven.com' => array(
        'to' => 'info@festivalvaiven.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'findenmexico.com' => array(
        'to' => 'juan.espina@findenmexico.com, atencion.clientes@findenmexico.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'first-template.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'francisco.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'fratell.com' => array(
        'to' => 'hello@fratell.com, work@fratell.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'fuerzaterritorial.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'funtappstic.com' => array(
        'to' => 'contact@funtappstic.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'frescura.mx' => array(
        'to' => 'contacto@frescura.com.mx',
        'cc' => '',
        'bcc' => 'marketingfrescura@hotmail.com, concentradorgoplek@gmail.com'),

    'frescura.goplek.com' => array(
        'to' => 'contacto@frescura.com.mx',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, marketingfrescura@hotmail.com, concentradorgoplek@gmail.com'),

    'frescura.com.mx' => array(
        'to' => ' contacto@frescura.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, marketingfrescura@hotmail.com'),

    'ftm.goplek.com' => array(
        'to' => 'ilopez@goplek.com',
        'cc' => '',
        'bcc' => ''),

    'en.ftm.goplek.com' => array(
        'to' => 'ilopez@goplek.com',
        'cc' => '',
        'bcc' => ''),

    'garminpuebla.com' => array(
        'to' => 'ventas@todogps.com.mx, info@garminpuebla.com',
        'cc' => '',
        'bcc' => ''),

    'gavinink-en.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'gavinink.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'gcb.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'geld.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'geldinteligenciafinanciera.com' => array(
        'to' => 'rh@geldinteligenciafinanciera.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'gestionautomotriz.com.mx' => array(
        'to' => 'congresovgam@vw.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, davidgtzfrc19@gmail.com, edergoplek@gmail.com, mlechuga@goplek.com'),

    'geterc.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'gisele.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'gloria.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'godshall.com.mx' => array(
        'to' => 'johngodshall@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'gommar.com.mx' => array(
        'to' => 'contacto-web@gommar.com.mx',
        'cc' => '',
        'bcc' => 'mmartinez@gommar.com.mx, concentradorgoplek@gmail.com'),

    'gommar.goplek.com' => array(
        'to' => 'contacto-web@gommar.com.mx',
        'cc' => '',
        'bcc' => 'mmartinez@gommar.com.mx'),

    'goplek.com' => array(
        'to' => 'jmenendez@goplek.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, ltorres@goplek.com'),

    'goplekhealth.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'gps-goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'grassandgo.com.mx' => array(
        'to' => 'direccion@grassandgo.com.mx',
        'cc' => 'guillermo.cuadra@grassandgo.com.mx',
        'bcc' => 'concentradorgoplek@gmail.com, mlechuga@goplek.com'),

    'grupocanfranc.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'grupovaleur.com' => array(
        'to' => 'gsolis@grupovaleur.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'gulsig.com' => array(
        'to' => 'mariana@gulsig.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'hel-sa.com' => array(
        'to' => 'contacto@hel-sa.com',
        'cc' => '',
        'bcc' => 'atencion_clientes@hel-sa.com, concentradorgoplek@gmail.com'),

    'en.hel-sa.com' => array(
        'to' => 'contacto@hel-sa.com',
        'cc' => '',
        'bcc' => 'atencion_clientes@hel-sa.com, concentradorgoplek@gmail.com'),

    'hoja.mx' => array(
        'to' => 'liza@hoja.mx, diego@hoja.mx, marco@hoja.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'claudia.com.mx' => array(
        'to' => 'servicioalcliente@claudia.com.mx, ventaspro1@claudia.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, mlechuga@goplek.com'),

    'hotel-quintasanjose.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'koaconstrucciones.com' => array(
        'to' => 'proyectos@goplek.com',
        'cc' => '',
        'bcc' => 'ilopez@goplek.com'),

    'jardinhuexotitla.com' => array(
        'to' => 'jardin@huexotitla.com',
        'cc' => '',
        'bcc' => 'info@abbondanza.com.mx, concentradorgoplek@gmail.com'),

    'joyce.goplek.com' => array(
        'to' => 'ilopez@goplek.com',
        'cc' => '',
        'bcc' => ''),

    'joyce2.goplek.com' => array(
        'to' => 'ilopez@goplek.com',
        'cc' => '',
        'bcc' => ''),

    'deportivo.productosjumbo.com' => array(
        'to' => 'jlevien@goplek.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'mobiliario.productosjumbo.com' => array(
        'to' => 'jlevien@goplek.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'recreativo.productosjumbo.com' => array(
        'to' => 'jlevien@goplek.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'htres.mx' => array(
        'to' => 'info@htres.mx',
        'cc' => '',
        'bcc' => ''),

    'heraapparel.com.mx' => array(
        'to' => 'claudia.gutierrez@hera.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'hera.goplek.com' => array(
        'to' => 'ilopez@goplek.com',
        'cc' => '',
        'bcc' => ''),

    'icum.edu.mx' => array(
        'to' => 'admisiones@icum.edu.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'inscripciones.icum.edu.mx' => array(
        // 'to' => 'admisiones@icum.edu.mx, contacto@icum.edu.mx',
        'to' => 'admisiones@icum.edu.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, mlechuga@goplek.com'),

    'ide.goplek.com' => array(
        'to' => 'alvaro@goplek.com',
        'cc' => '',
        'bcc' => ''),

    /* 'idiomasvw.com.mx' => array(
        'to' => 'contacto.vwinstituto@ivw.com.mx, idiomasvw@amatech.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, centrosdeidiomasvw@gmail.com'), */

    'idiomasvw.com.mx' => array(
        'to' => 'alopez@goplek.com',
        'cc' => '',
        'bcc' => ''),

    'imperialvans.com.mx' => array(
        'to' => 'gonzalo.tinoco@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'ingles-matutino.idiomasvw.com.mx' => array(
        'to' => 'idiomasvw@amatech.com.mx, octavio.saucedo@ivw.com.mx',
        'cc' => 'centrosdeidiomasvw@gmail.com',
        'bcc' => 'concentradorgoplek@gmail.com'
    ),

    'innoquos.com' => array(
        'to' => 'vm@sialico.com, contacto@sialico.com',
        'cc' => '',
        'bcc' => 'alavaro@goplek.com, concentradorgoplek@gmail.com'),

    'inplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'integralautomation.co.nz' => array(
        'to' => 'logan@integralautomation.co.nz',
        'cc' => '',
        'bcc' => ' alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'interdesignmexico.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'intranet.fernandarojas.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'invaders.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'conversionesdelujo.imperialvans.com.mx' => array(
        'to' => 'gonzalo.tinoco@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, mlechuga@goplek.com'),

    'isuzu.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'isuzubajio.com' => array(
        'to' => 'lduran@isuzubajio.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'landing.isuzucamiones.com' => array(
        'to' => 'jorgepla1@gmail.com',
        'cc' => '',
        'bcc' => 'mlechuga@goplek.com, concentradorgoplek@gmail.com'
    ),

    'landgrass.goplek.com' => array(
        'to' => 'jlevien@goplek.com',
        'cc' => '',
        'bcc' => ''
    ),

    'zoe.propeler.mx' => array(
        'to' => 'menendezpoo+bak@gmail.com,mb@propeler.mx,dm@propeler.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'ojodeagua.propeler.mx' => array(
        'to' => 'menendezpoo+bak@gmail.com, mb@propeler.mx, dm@propeler.mx, hola@propeler.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'ojodeagua.goplek.com' => array(
        'to' => 'alvaro@goplek.com',
        'cc' => '',
        'bcc' => ''),

    'conoce.propeler.mx' => array(
        'to' => 'menendezpoo+bak@gmail.com, mb@propeler.mx, dm@propeler.mx, hola@propeler.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'hotelpanamerican.mx' => array(
        'to' => 'h_panam_admon@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'isuzucamiones.com' => array(
        'principal-abastos' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'),

        'principal-guadalajara' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'),

        'principal-puerto-vallarta' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'),

        'principal-colima' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'),

        'productos-abastos' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'),

        'productos-guadalajara' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'),

        'productos-puerto-vallarta' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'),

        'productos-colima' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'),

        'financiamiento-abastos' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'
        ),

        'financiamiento-guadalajara' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'
        ),

        'financiamiento-puerto-vallarta' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'
        ),

        'financiamiento-colima' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'
        ),

        'servicio-abastos' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'
        ),

        'servicio-guadalajara' => array(
            'to' => 'asesorisuzu@isuzucamiones.com',
            'cc' => '',
            'bcc' => 'jorgepla1@gmail.com, mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'
        ),

        'servicio-puerto-vallarta' => array(
            'to' => 'aarellano@isuzucamiones.com',
            'cc' => '',
            'bcc' => 'jorgepla1@gmail.com, mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'
        ),

        'servicio-colima' => array(
            'to' => 'ahernandez@isuzucamiones.com',
            'cc' => '',
            'bcc' => 'jorgepla1@gmail.com, mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'
        ),

        'clinica' => array(
            'to' => 'jorgepla1@gmail.com',
            'cc' => '',
            'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com, concentradorgoplek@gmail.com'
        ),

        'to' => 'jorgepla1@gmail.com',
        'cc' => '',
        'bcc' => 'mlechuga@goplek.com, alvaro@goplek.com,concentradorgoplek@gmail.com'
    ),

    'isuzumonterrey.com.mx' => array(
        'principal-monterrey' => array(
            'to' => 'gmendoza@isuzumty.com',
            'cc' => '',
            'bcc' => 'concentradorgoplek@gmail.com, alvaro@goplek.com, mlechuga@goplek.com'
        ),
        'productos' => array(
            'to' => 'gmendoza@isuzumty.com',
            'cc' => '',
            'bcc' => 'alvaro@goplek.com, mlechuga@goplek.com'
        ),
        'financiamiento' => array(
            'to' => 'gmendoza@isuzumty.com',
            'cc' => '',
            'bcc' => 'alvaro@goplek.com, mlechuga@goplek.com'
        ),
        'servicio' => array(
            'to' => 'servicio@isuzumty.com, taller@isuzumty.com, gmendoza@isuzumty.com',
            'cc' => '',
            'bcc' => 'alvaro@goplek.com, mlechuga@goplek.com'
        ),
        'to' => 'gmendoza@isuzumty.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, mlechuga@goplek.com'),

    'isuzumex.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'isuzunet.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'jcaramel.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'jmasrarquitectos.com' => array(
        'to' => 'informes@jmasrarquitectos.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'jovenesasuncion.org' => array(
        'to' => 'aozalicia@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'jsreingenieria.com' => array(
        'to' => 'contacto@jsreingenieria.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'juan.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'junghannsmexico.com.mx' => array(
        'to' => 'atencionaclientes@junghannsmexico.com.mx, gerencia@junghannsmexico.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'juventudasuncion.goplek.com' => array(
        'to' => 'aozalicia@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'kainsa.com' => array(
        'to' => 'info@kainsa.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'kainsa.goplek.com' => array(
        'to' => 'dperez@goplek.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'kartabasoluciones.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'kikeabed-photoshots.goplek.com' => array(
        'to' => 'enriqueabed@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'kikeabed.com' => array(
        'to' => 'info@kikeabed.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com'),

    'kikeabed.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'kloeme.com' => array(
        'to' => 'ocastell@kloeme.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'kwtconsulting.goplek.com' => array(
        'to' => 'contacto.kwt@yakultpuebla.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'labienquerida.com.mx' => array(
        'to' => 'planner1@labienquerida.com.mx, planner2@labienquerida.com.mx, planner3@labienquerida.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'lagalerialazcarro.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'lagranbodega.com.mx' => array(
        'to' => 'smunoz@grupolagranbodega.com.mx, thelmarivera@grupolagranbodega.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@goplek.com'),

    'lagranfama.com' => array(
        'to' => 'contacto@lagranfama.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'laquintaesencia.mx' => array(
        'to' => 'reservaciones@laquintaesencia.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'latentestudio.com' => array(
        'to' => 'diego@latentestudio.com, hola@latentestudio.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'landingecovert.goplek.com' => array(
        'to' => 'concentradorgoplek@gmail.com',
        'cc' => '',
        'bcc' => ''),

    'latin-post.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'latin-sport.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'latinpost-infraestructura.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'latinpost.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'latinpost.mx.old' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'letreros.goplek.com' => array(
        'to' => 'letreroexpress@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'
    ),

    'licoreriasanpedrito.com.mx' => array(
        'to' => 'romeomadrid0@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'loview.mx' => array(
        'to' => 'contacto@loview.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, iarredondo@goplek.com'),

    'luxist.com.tr' => array(
        'to' => 'fijiwater@luxist.com.tr',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'macha.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'machafoods.com' => array(
        'to' => 'fernando@arrierafoods.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'mackoy.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'inmobiliariamaen.com' => array(
        'to' => 'alejandra@inmobiliariamaen.com, luzmaria@inmobiliariamaen.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'malmo.mx' => array(
        'to' => 'flopez@tallerdaviddana.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'marthagonzaleznutricion.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'martinvasquez.com.mx' => array(
        'to' => 'contacto@martinvasquez.com.mx, jbens1969@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'masterhouse.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'masterhouse.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'masmuebles.com.mx' => array(
        'to' => 'ventas@masmuebles.com.mx',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, menendezpoo+bak@gmail.com, concentradorgoplek@gmail.com'),

    'materia-prima.com.mx' => array(
        'to' => 'contacto@materia-prima.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'menendezpoo.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'mercedesrojas.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'agaves.goplek.com' => array(
        'to' => 'linda.leyva@nahmexicana.com',
        'cc' => '',
        'bcc' => 'ilopez@goplek.com'),

    'maxilofacialpuebla.com' => array(
        'to' => 'mariolagunes86@hotmail.com, mario.lagunes@correo.buap.mx',
        'cc' => '',
        'bcc' => 'spichardo@goplek.com, concentradorgoplek@gmail.com'),

    'mezcalvelua.goplek.com' => array(
        'to' => 'can@mezcalvelua.com, contacto@mezcalvelua.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'misionerascarmelitas.com' => array(
        'to' => 'carmelitas@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),   

    'mp.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'mocho30.goplek.com' => array(
        'to' => 'info@ocho30.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'morena.senado.gob.mx' => array(
        'to' => 'contacto@senadomorena.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'morninggroup.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'myufd.com' => array(
        'to' => 'contacto@myufd.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'nahresidencial.com' => array(
        'to' => 'linda.leyva@nahmexicana.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'nexpresion.com' => array(
        'to' => 'said_aba@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'new-excelnobleza.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'new-isuzu.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'new-vinilosygraficos.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'new.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => 'jmenendez@goplek.com'),

    'nhativa.com' => array(
        'to' => 'pyrigoyen@icloud.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'nhomada.com' => array(
        'to' => 'diego@nhomada.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'nsmith.com.mx' => array(
        'to' => 'nicolesmithap87@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'ngo.com.mx' => array(
        'to' => 'info@ngo.com.mx, adalberto.bedolla@ngo.com.mx, abelardo.garcia@ngo.com.mx',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'ngo.goplek.com' => array(
        'to' => 'concentradorgoplek@gmail.com',
        'cc' => '',
        'bcc' => ''),

    'nonettevillanueva.com' => array(
        'to' => 'nonettevillanuevae@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'noticiasmexicohoy.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'nossen.com.mx' => array(
        'to' => 'enrique_rivero2000@hotmail.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'nubisa.mx' => array(
        'to' => 'andrea@nubisa.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'nutriblend.com.mx' => array(
        'to' => 'fernando@arrierafoods.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'ocho30.com' => array(
        'to' => 'info@ocho30.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'odontologiapediatricapuebla.com' => array(
        'to' => 'betohachity@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'old.martinvasquez.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'onoloa.mx' => array(
        'to' => 'hola@onoloa.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'pamelazanatta.com.br' => array(
        'to' => 'pamela@pamelazanatta.com.br',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'pamelazanatta.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => 'pzanatta@goplek.com'),

    'patitosdehuleamarillo.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'paty.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'pbhwarehouse.com' => array(
        'to' => 'frank@pbhwarehouse.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'pcidepuebla.com.mx' => array(
        'to' => 'igarza@pcidepuebla.com.mx',
        'cc' => '',
        'bcc' => 'garagon@pcidepuebla.com.mx, alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'piatt.com.mx' => array(
        'to' => 'daniel_pro4@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'pixel-mas.com' => array(
        'to' => 'hola@pixel-mas.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'plazaw.goplek.com' => array(
        'to' => 'plazaw.w@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'premiumtravelservices.com.mx' => array(
        'to' => 'info@premiumtravelservices.com.mx, hhernandez@premiumtravelservices.com.mx, ameza@premiumtravelservices.com.mx, jraga@premiumtravelservices.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, jmenendez@goplek.com, mlechuga@goplek.com'),

    'productosinman.com' => array(
        'to' => 'contacto@productosinman.com, ivy@productosinman.com',
        'cc' => '',
        'bcc' => 'jlevien@goplek.com'),

    'productosragy.com' => array(
        'to' => 'ragydecoration@gmail.com, productosragy@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'promacon.com.mx' => array(
        'to' => 'aramirezj@promacon.com.mx',
        'cc' => '',
        'bcc' => 'fquintero@promacon.com.mx, concentradorgoplek@gmail.com'),

    'propamagro.com' => array(
        'to' => 'grupo_rf@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'en.propamagro.com' => array(
        'to' => 'grupo_rf@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'proyectosdevidafinanciero.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'psi.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'psipinturas.com.mx' => array(
        'to' => 'mercadotecnia@psipinturas.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'puntavictoria.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'ragon.goplek.com' => array(
        'to' => 'contacto@ragon.com.mx, ',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'residencial.desmexsolarangelopolis.com' => array(
        'to' => 'info@dsangelopolis.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'repaso.com.mx' => array(
        'to' => 'repaso@repaso.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'revista-vip.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'revistavip.es' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'revistavip.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'rintex-en.goplek.com' => array(
        'to' => 't_shirt@rintex.com.mx, ',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'rintex.goplek.com' => array(
        'to' => 't_shirt@rintex.com.mx, ',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'rosalbarojas.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'toddlies.goplek.com' => array(
        'to' => 'alvaro@goplek.com',
        'cc' => '',
        'bcc' => 'dperez@goplek.com'),

    's67487.gridserver.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'saago.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'sanjoseactipan.com' => array(
        'to' => 'contacto@sanjoseactipan.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'en.restaurantesartoria.mx' => array(
        'to' => 'carboni85@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'restaurantesartoria.mx' => array(
        'to' => 'carboni85@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'salsaclandestina.com' => array(
        'to' => 'diego@salsaclandestina.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'sanhumboldt.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'saknovias.com' => array(
        'to' => 'serna@saknovias.com, garabana@saknovias.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'segundaoportunidad.org.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'selfyt.com' => array(
        'to' => 'info@selfyt.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'shanti.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'shantimeditacion.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'sialico.com.mx' => array(
        'to' => 'contacto@sialico.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'siago.com.mx' => array(
        'to' => 'atencionclientes@siago.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'sicibsa.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'sitec.goplek.com' => array(
        'to' => 'iarredondo@goplek.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'sitecmexico.com.mx' => array(
        'to' => 'areatecnica.sitec@gmail.com, areatecnica@sitecmexico.com.mx',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'solbio.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'soluciontotal.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'soluciones.mx' => array(
        'to' => 'info@soluciones.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'sombrasvillalva.com' => array(
        'to' => 'sombrasvillalva@hotmail.com, raygonv@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'somosvip.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'soumaya.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'suargo.com.mx' => array(
        'to' => 'contacto@suargo.com.mx, ',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'suargo.goplek.com' => array(
        'to' => 'contacto@suargo.com.mx, ',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'subscriber' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'suich.com.mx' => array(
        'to' => 'pato@suich.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'tlaiye.com' => array(
        'to' => 'carlos@tliaye.com, david@tlaiye.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'tlaiye.goplek.com' => array(
        'to' => 'alvaro@goplek.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'tc-technologies.goplek.com' => array(
        'to' => 'tgarcia@tc-technologies.com, itcue@tc-technologies.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'teamventory.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'techniks.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'tecniplagas.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'tecnohielo.com.mx' => array(
        'to' => 'ventas@tecnohielo.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'tecnohielo.goplek.com' => array(
        'to' => 'ventas@tecnohielo.com.mx',
        'cc' => '',
        'bcc' => ''),

    'tecnomarmol.com.mx' => array(
        'to' => 'nieves@tecnomarmol.com.mx, ventas@tecnomarmol.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'tejidospoo.com.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'tigredemar.com' => array(
        'to' => 'hola@tigredemar.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'tmtm.com.mx' => array(
        'to' => 'contacto@tmtm.com.mx, hello@tmtm.com.mx',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'en.tmtm.com.mx' => array(
        'to' => 'contacto@tmtm.com.mx, hello@tmtm.com.mx',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, concentradorgoplek@gmail.com'),

    'tmtm2.goplek.com' => array(
        'to' => 'bandala57@hotmail.com, jp@tmtm.com.mx',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com'),

    'tenexac-en.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'tenexac.com' => array(
        'to' => 'elvalor78@gmail.com, juan_carloshaces@hotmail.com, rafaelhaces@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'tessilgraf.com' => array(
        'to' => 'a.espinosa@tessilgraf.com, info@tessilgraf.com, ndp@tessilgraf.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'tessilgraf.goplek.com' => array(
        'to' => 'iarredondo@goplek.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'texmex.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'textilestenexac.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'the-driver-magazine.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'todogps.com.mx' => array(
        'to' => 'ventas@todogps.com.mx, ',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'topracingbike.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'tragaluz.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'tragaluzestudio.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'tucarsa.com' => array(
        //'to' => 'tucarsa_puebla@hotmail.com',
        'to' => 'ventas@tucarsa.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, mlechuga@goplek.com'),

    'tugolfmagazine.com' => array(
        'to' => 'felipe0465@gmail.com, jrojas53@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'unopue.com' => array(
        'to' => 'unopue@prodigy.net.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'urbanitesuites.com' => array(
        'to' => 'maria.urrutia@urbanite.com.mx',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com, mlechuga@goplek.com, concentradorgoplek@gmail.com, rafaaja@gmail.com'),

    'vaiven.goplek.com' => array(
        'to' => 'info@festivalvaiven.com',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com'),

    'vcarquitectos.mx' => array(
        'to' => 'vcarquitectos@gmail.com, info@vcarquitectos.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'venster.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'verificentrospuebla.com.mx' => array(
        'to' => 'inf.verificentrospuebla@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'veuree.goplek.com' => array(
        'to' => 'erosas@tc-technologies.com, jtcue@tc-technologies.com, atcue@tc-technologies.com',
        'cc' => '',
        'bcc' => ''),

    'vinilosygraficos.com' => array(
        'to' => 'info@vinilosygraficos.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'vinilosygraficos.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'vinyl-rules.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'vip.latinpost.mx' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'virtus.goplek.com' => array(
        'to' => 'concentradorgoplek@gmail.com',
        'cc' => '',
        'bcc' => ''),

    'watchesandgold.com.mx' => array(
        'to' => 'redes@chronoimports.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'volkswageninstituto.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => 'alvaro@goplek.com'),

    'vwi.com.mx' => array(
        'to' => 'contacto.vwinstituto@ivw.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'vwidiomas.goplek.com' => array(
        'to' => 'contacto.vwinstituto@ivw.com.mx',
        'cc' => '',
        'bcc' => 'jmenendez@goplek.com, alvaro@goplek.com, mlechuga@goplek.com'),

    'vwinstituto.goplek.com' => array(
        'to' => 'contacto.vwinstituto@ivw.com.mx',
        'cc' => 'dperez@goplek.com',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'vwpedidos.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'wiki.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'xibu.com.mx' => array(
        'to' => 'contacto@xibu.com.mx, dimexibu@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'
    ),

    'uplink.idiomasvw.com.mx' => array(
        'to' => 'centrosdeidiomasvw@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, idiomasvw@amatech.com.mx, inscripciones.zavaleta@idiomasvw.com.mx'
    ),

    'idiomas40.idiomasvw.com.mx' => array(
        'to' => 'centrosdeidiomasvw@gmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, idiomasvw@amatech.com.mx, inscripciones.zavaleta@idiomasvw.com.mx'
    ),

    'upaep.idiomasvw.com.mx' => array(
        'to' => 'inscripciones.zavaleta@idiomasvw.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, idiomasvw@amatech.com.mx'
    ),

    'ylcom.mx' => array(
        'to' => 'norma@ylcom.mx, mariana@ylcom.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'ylcom.goplek.com' => array(
        'to' => 'iarredondo@goplek.com',
        'cc' => '',
        'bcc' => ''),

    'zoryziga.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'zomato.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'zoryziga.goplek.com' => array(
        'to' => '',
        'cc' => '',
        'bcc' => ''),

    'skylift-services.com.mx' => array(
        'to' => 'contacto@skylift-services.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'venta.skylift-services.com.mx' => array(
        'to' => 'contacto@skylift-services.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, mlechuga@goplek.com'),

    'renta.skylift-services.com.mx' => array(
        'to' => 'contacto@skylift-services.com.mx',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com, mlechuga@goplek.com'),

    'terrenos.farasi.com.mx' => array(
        'to' => 'fgma.91@gmail.com, mercaji@hotmail.com, rosario@mercaji.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'ventas.farasi.com.mx' => array(
        'to' => 'fgma.91@gmail.com, contacto@mercaji.com, mercaji@hotmail.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'ventas.misionlosagaves.com' => array(
        'to' => 'ventas@nahmexicana.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'residencias.misionlosagaves.com' => array(
        'to' => 'ventas@nahmexicana.com',
        'cc' => '',
        'bcc' => 'concentradorgoplek@gmail.com'),

    'TODO_HOST' => array(
        'to' => 'menendezpoo+bak@gmail.com',
        'cc' => '',
        'bcc' => '')
);
