<?php
/**
 * Created by PhpStorm.
 * User: cesarmejia
 * Date: 31/07/17
 * Time: 18:35
 */

// ---------------------------------------------------------------------
// | Global Variables                                                   |
// ---------------------------------------------------------------------

/*
 * Root Id
 */
$root_id = 0;

/*
 * Home GUID
 */
$home_guid = 'home';

/*
 * Proceso GUID
 */
$process_guid = '_kZqMNh5QS';

/*
 * Por que Nosotros  GUID
 */
$why_guid = 'Gj-MYYCBNQ';

/*
 * Especialistas  GUID
 */
$specialists_guid = 'WZFB0RgX6m';

/*
 * Galería  GUID
 */
$gallery_guid = 'LYm_z96FJI';

/*
 * Contacto  GUID
 */
$contact_guid = 'XuX9_bzwOk';

/*
 * Pago  GUID
 */
$payment_guid = '8I6jn0lh6X';

/*
 * Footer  GUID
 */
$footer_guid = 'C38HI0Cjvy';

/**
 * Contact GUID
 */
$contact_guid = 'XuX9_bzwOk';
$contact_id = 481;

// ---------------------------------------------------------------------
// | Fragment Helpers                                                   |
// ---------------------------------------------------------------------

/**
 * Find page by guid.
 * @param {string} $guid
 * @param {Array<Page>} $pages
 * @return Page
 */
function find_page_by_guid($guid, $pages) {
    foreach ($pages as $page) {
        if ($guid === $page->guid)
            return $page;
    }

    return null;
}


/**
 * Find fragments by name.
 * @param $name
 * @param $fragments
 * @return Fragment
 */
function find_fragment_by_name($name, $fragments) {
    foreach ($fragments as $fragment) {
        if ($name === $fragment->name)
            return $fragment;
    }

    return null;
}


/**
 * Find fragments by name.
 * @param $key
 * @param $fragments
 * @return Fragment
 */
function find_fragment_by_key($key, $fragments) {
    foreach ($fragments as $fragment) {
        if ($key === $fragment->key)
            return $fragment;
    }

    return null;
}


/**
 * Find setting by name.
 * @param $name
 * @param $settings
 * @return Setting
 */
function find_setting_by_name($name, $settings) {
    foreach ($settings as $setting) {
        if ($name === $setting->name)
            return $setting;
    }

    return null;
}



/**
 * Get an image tag in string state.
 * @param $image
 * @return string
 */
function get_original_image($image) {
    if ($image) {
        $attrs = Fragment::elementAttributes($image);
        return $attrs['src'];
    }

    return "";
}


/**
 * Determine if current page is home.
 * @throws Exception
 * @return boolean
 */
function is_home() {
    global $page, $home_guid;

    if (!($page instanceof Page)) {
        throw new Exception('$page variable is not in the scope. Maybe you\'re in a file that is not a template.');
    }

    return $page->guid === $home_guid;
}


// ---------------------------------------------------------------------
// | Common pages used                                                  |
// ---------------------------------------------------------------------


// Retrieve root pages.
$result = Page::search(array(
    'idparent' => $root_id,
    'sortBy' => 'created ASC',
    'fragments' => array(
        'title-supply',
        'description-supply',
        'subtitle-supply',
        'phrase-supply',
        'gallery',
        'text-contact',
        'office-number-contact1',
        'office-number-contact2',
        'personal-number-contact1',
        'personal-number-contact2',
        'shortcut-image-1', 'shortcut-image-2','shortcut-image-3', 'shortcut-image-4','shortcut-image-5', 'shortcut-image-6','shortcut-image-7',
        'title', 'desc', 'image-1', 'image-2', 'image-1-2', 'image-2-2'
    )
));

$root_pages = $result['records'];
