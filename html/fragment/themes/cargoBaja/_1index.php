<?php
include_once 'globals.php';
include_once 'util/fragment-helpers.php';
/**
 * specialists section
 */
$process_page = find_page_by_guid($process_guid, $root_pages);
$process_id = $process_page->idpage;
$process_title = $process_page->fragments['title-supply']->value;
$process_description = $process_page->fragments['description-supply']->value;
$process_subtitle = $process_page->fragments['subtitle-supply']->value;
$process_phrase = $process_page->fragments['phrase-supply']->value;

/**
 * Contact Section
 */
$contact_page = find_page_by_guid($contact_guid, $root_pages);
$contact_text = $contact_page->fragments['text-contact']->value;
$office_number1 = $contact_page->fragments['office-number-contact1']->value;
$office_number2 = $contact_page->fragments['office-number-contact2']->value;
$personal_number1 = $contact_page->fragments['personal-number-contact1']->value;
$personal_number2 = $contact_page->fragments['personal-number-contact2']->value;

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" itemscope itemtype="http://schema.org/Thing" lang="es-MX">
    <head>
        <?php include_once 'partials/head.php'; ?>
    </head>
    <body>

        <!-- Navigation -->
        <?php include_once 'partials/navigation.php'; ?>
        <!-- /.Navigation -->

        <!-- Outer wrapper -->
        <div class="outer-wrapper">
            <!-- Block Cover -->
            <?php
                $cover = find_page_by_guid($home_guid, $root_pages);
            ?>
            <section class="block cover" id="home">
                <div class="holder">
                    <div class="slider loading"><?= $cover->fragments['gallery']->value ?></div>
                </div>
                <span class="decoration">
                    <svg width="171px" height="160px" viewBox="0 0 171 160" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.174274554"> <g id="Group-6" transform="translate(1.000000, 2.000000)" stroke="#C1C1C1" stroke-width="2"> <polygon id="Polygon-Copy-19" points="64.5 0 129 39 129 117 64.5 156 8.95117314e-14 117 4.83363349e-14 39"></polygon> <polygon id="Polygon-Copy-20" points="144 83 169 98 169 128 144 143 119 128 119 98"></polygon> </g> </g></svg>
                </span>
            </section>
            <!-- /.Cover -->

            <!-- Block Suministro -->
            <section class="block suministro" id="">
                <div class="text-back-supply d-block d-sm-none">
                    <?= $process_phrase ?>
                </div>
                <div class="holder">
                    <div class="container-fluid">
                        <div class="content">
                            <div class="wrapper-supply-info">
                                <div class="inner-supply">
                                    <div class="header">
                                        <div class="title"><?= $process_title ?></div>
                                    </div>
                                    <div class="description-supply">
                                        <?= $process_description ?>
                                    </div>
                                    <div class="subtitle-supply">
                                        <?= $process_subtitle ?>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapper-supply-carrousel">
                                <?php
                                $result = Page::search(array(
                                    'idparent' => $process_id,
                                    'sortBy' => 'created ASC',
                                    'fragments' => array(
                                        'title-category',
                                        'image-category'
                                    )
                                ));
                                $supply_values = $result['records'];
                                ?>
                                <div class="inner-carrousel">
                                    <div class="util-carrousel">
                                        <?php
                                        foreach ($supply_values as $thumb) {
                                            $supply_title = $thumb->fragments['title-category']->value;
                                            $supply_image = $thumb->fragments['image-category']->value;
                                            $supply_src = get_original_image($supply_image);
                                            ?>
                                            <a class="thumb" href="<?= $thumb->key ?>">
                                                <div class="wrapper-supply-thumb">
                                                    <div class="content-img">
                                                        <div class="inner-content-img">
                                                            <img src="<?= $supply_src ?>" alt="" class="img-fluid">
                                                        </div>
                                                    </div>
                                                    <div class="content-category">
                                                        <div class="inner-content-category">
                                                            <?= $supply_title ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.Suministro -->

            <!-- Block Por qué nosotros -->
            <?php
                $why_us = find_page_by_guid($why_guid, $root_pages);
                $wu_title = $why_us -> title;
                $whyus_result = Page::search(array(
                   'idparent' => $why_us->idpage,
                   'fragments' => array('icon', 'desc'),
                    'sortBy' => 'created ASC'
                ));

                $whyus_records = $whyus_result['records'];
            ?>
            <section class="block why-us" id="por-que-nosotros">
                <div class="holder">
                    <div class="container-fluid">
                        <div class="header"></div>
                        <div class="header">
                            <div class="title"><?= $wu_title ?></div>
                        </div>
                        <div class="content">
                            <div class="row">
                                <?php
                                    foreach ($whyus_records as $wur) {
                                        $wur_title = $wur -> title;
                                        $wur_icon = $wur -> fragments['icon'] -> value;
                                        $wur_desc = $wur -> fragments['desc'] -> value;
                                ?>
                                <div class="col-12 col-sm-6 col-md-3 whyus-item">
                                    <div class="feature">
                                        <div class="icon partial"><?= $wur_icon ?></div>
                                        <h3 class="title partial"><?= $wur_title ?></h3>
                                        <div class="desc partial"><p><?= $wur_desc ?></p></div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.Por qué nosotros -->

            <!-- Block especialistas -->
            <div class="specialists-wrapper">
                <?php
                    $specialists = find_page_by_guid($specialists_guid, $root_pages);
                    $specialists_title = $specialists -> fragments['title'] -> value;
                    $specialists_desc = $specialists -> fragments['desc'] -> value;
                    $first_image = $specialists -> fragments['image-1'] -> value;
                    $second_image = $specialists -> fragments['image-2'] -> value;

                    $image_horizontal1_attrs= Fragment::elementAttributes($specialists -> fragments['image-2'] -> value);
                    $image_horizontal2_attrs= Fragment::elementAttributes($specialists -> fragments['image-2-2'] -> value);
                    $image_vertical1_attrs = Fragment::elementAttributes($specialists -> fragments['image-1'] -> value);
                    $image_vertical2_attrs = Fragment::elementAttributes($specialists -> fragments['image-1-2'] -> value);
                ?>
                <section class="block specialists" id="especialistas">
                    <div class="holder">
                        <div class="container-fluid">
                            <div class="content">
                                <div class="left-side">
                                    <div class="img1-vertical" style="background: url(<?= $image_vertical1_attrs['src'] ?>); background-repeat: no-repeat; background-size: auto; background-position: bottom;"></div>
                                    <div class="img2-vertical" style="background: url(<?= $image_vertical2_attrs['src'] ?>); background-repeat: no-repeat; background-size: auto; background-position: bottom;"></div>
                                </div>
                                <div class="right-side">
                                    <div class="image">
                                        <div class="img1-horizontal" style="background: url(<?= $image_horizontal1_attrs['src'] ?>); background-repeat: no-repeat; background-position: left center; background-size: auto;"></div>
                                        <div class="img2-horizontal" style="background: url(<?= $image_horizontal2_attrs['src'] ?>); background-repeat: no-repeat; background-position: left center; background-size: auto;"></div>
                                    </div>
                                    <div class="section-desc">
                                        <div class="wrapper-desc">
                                            <div class="header">
                                                <div class="title"><?= $specialists_title ?></div>
                                            </div>
                                            <div class="desc"><?= $specialists_desc ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!-- /.especialistas -->

            <!-- Block Galeria -->
            <?php
            $gallery = find_page_by_guid($gallery_guid, $root_pages);
            $g_title = $gallery -> title;
            $result_gallery = Page::search(array(
                'idparent' => $gallery->idpage,
                'fragments' => array('vertical-image', 'horizontal-image', 'little-image-1', 'little-image-2')
            ));

            $gallery_records = $result_gallery['records'];
            ?>
            <section class="block gallery" id="galeria">
                <div class="holder">
                    <div class="container-fluid">
                        <div class="header">
                            <div class="title">
                                <?= $g_title ?>
                            </div>
                        </div>
                        <div class="content">
                            <div class="wrapper-gallery">
                                <div class="inner-carrousel">
                                    <div class="util-carrousel">
                                        <?php
                                            foreach ($gallery_records as $gr){
                                                $vertical_img = $gr -> fragments['vertical-image'] -> value;
                                                $horizontal_img = $gr -> fragments['horizontal-image'] -> value;
                                                $little_image_1 = $gr -> fragments['little-image-1'] -> value;
                                                $little_image_2 = $gr -> fragments['little-image-2'] -> value;
                                        ?>
                                            <div class="thumb thumb-project">
                                                <div class="single-slide btn-open-mobil-modal-index">
                                                    <div class="left-side img-wrapper"><?= $vertical_img ?></div>
                                                    <div class="right-side">
                                                        <div class="horizontal-image img-wrapper"><?= $horizontal_img ?></div>
                                                        <div class="little-images">
                                                            <div class="little-image-1 img-wrapper"><?= $little_image_1 ?></div>
                                                            <div class="little-image-2 img-wrapper"><?= $little_image_2 ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Modal -->
            <div class="d-none">
                <div class="wrapper-mobil-carrousel">
                    <div class="inner-mobil-carrousel-tracing">
                        <div class="util-carrousel">
                            <?php
                                foreach ($gallery_records as $gr_popup) {
                                    $vertical_img_popup = $gr_popup -> fragments['vertical-image'] -> value;
                                    $horizontal_img_popup = $gr_popup -> fragments['horizontal-image'] -> value;
                                    $little_image_1_popup = $gr_popup -> fragments['little-image-1'] -> value;
                                    $little_image_2_popup = $gr_popup -> fragments['little-image-2'] -> value;
                                    ?>
                                    <div class="thumb">
                                        <div class="wrapper-thumb-content">
                                            <div class="thumb-img">
                                                <?= $vertical_img_popup ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="thumb">
                                        <div class="wrapper-thumb-content">
                                            <div class="thumb-img">
                                                <?= $horizontal_img_popup ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="thumb">
                                        <div class="wrapper-thumb-content">
                                            <div class="thumb-img">
                                                <?= $little_image_1_popup ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="thumb">
                                        <div class="wrapper-thumb-content">
                                            <div class="thumb-img">
                                                <?= $little_image_2_popup ?>
                                            </div>
                                        </div>
                                    </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.Modal -->
            <!-- /.Galeria -->

            <!-- Block contacto -->
            <section class="block contact" id="contacto">
                <div class="element-title-back">
                    <?= $contact_page->title ?>
                </div>
                <div class="holder">
                    <div class="container-fluid">
                        <div class="header"></div>
                        <div class="content">
                            <!-- Contact Info -->
                            <div class="contact-info">
                                <div class="inner-contact-info">
                                    <div class="header">
                                        <h2 class="title"><?= $contact_page->title ?></h2>
                                    </div>
                                    <div class="info-message"><?= $contact_text ?></div>
                                    <a href="tel:<?= $office_number2 ?>" class="office-phone">
                                        <span class="icon"><img src="<?= IMGS_PATH ?>telephone.svg" alt=""
                                                                class="img-fluid"></span>
                                        <span class="text"><?= $office_number1 ?></span>
                                    </a>
                                    <a href="tel:<?= $personal_number2 ?>" class="mobil-phone">
                                        <span class="icon"><img src="<?= IMGS_PATH ?>smartphone.svg" alt=""
                                                                class="img-fluid"></span>
                                        <span class="text"><?= $personal_number1 ?></span>
                                    </a>
                                </div>
                            </div>

                            <!-- Contact Form -->
                            <div class="wrapper-contact-form">
                                <div class="inner-wrapper-form">
                                    <div class="inner-wrapper-unbend">
                                        <form class="contact-form">
                                            <div class="status-message d-none"></div>
                                            <div class="wrapper-content-form">
                                                <input autocomplete="off" data-clue="Ingresa un Nombre"
                                                       data-validate="notEmpty"
                                                       name="Nombre" placeholder="Nombre" data-placeholder="Nombre"
                                                       type="text">
                                                <input autocomplete="off"
                                                       data-clue="Ingrese número a diez (10) dígitos:"
                                                       data-validate="phone" name="Telefono" placeholder="Teléfono"
                                                       data-placeholder="Teléfono" type="text">
                                                <input autocomplete="off" data-validate="email"
                                                       data-clue="Ingresa un correo válido"
                                                       name="Email" data-placeholder="E-mail:" placeholder="E-mail"
                                                       type="text">
                                                <textarea data-validate="notEmpty" data-clue="Ingresa un Mensaje"
                                                          name="Mensaje"
                                                          placeholder="Mensaje" data-placeholder="Mensaje"></textarea>
                                                <button class="submit" type="submit">
                                                    <span class="inner">Contacto</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <!-- /.contacto -->
            <!-- Footer -->
            <?php include_once 'partials/footer.php'; ?>
            <!-- /.Footer -->
        </div>
        <?php include_once 'partials/scripts.php'; ?>
    </body>
</html>
