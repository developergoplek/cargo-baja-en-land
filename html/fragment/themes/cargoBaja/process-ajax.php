<?php

// Simulate time that server takes to send a message.
sleep(3);

// Simulate response from server
// 1 = Success
// 0 = Error
echo 1;